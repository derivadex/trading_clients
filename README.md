# DerivaDEX Trading Client Tooling

This repository is under active development and currently includes:

- An auditor client
- Sample strategies

Please use these materials in conjunction with the [API documentation](https://apidocs.derivadex.io/).
