"""
CancelAllIntent module
"""


class CancelAllIntent:
    """
    Defines an CancelAllIntent
    """

    def __init__(
        self,
        strategy,
        nonce,
        signature="",
    ):
        self.strategy = strategy
        self.nonce = nonce
        self.signature = signature

    def __str__(self):
        return f"CancelAllIntent: strategy = {self.strategy}; nonce = {self.nonce}; signature = {self.signature}"
