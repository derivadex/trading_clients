"""
WithdrawIntent module
"""


class WithdrawIntent:
    """
    Defines a WithdrawIntent
    """

    def __init__(
        self,
        recipient_address,
        strategy_id,
        currency,
        amount,
        nonce,
        signature="",
    ):
        self.recipient_address = recipient_address
        self.strategy_id = strategy_id
        self.currency = currency
        self.amount = amount
        self.nonce = nonce
        self.signature = signature

    def __str__(self):
        return f"WithdrawIntent: recipient_address = {self.recipient_address}; strategy_id = {self.strategy_id}; currency = {self.currency}; amount = {self.amount}; nonce = {self.nonce}; signature = {self.signature}"
