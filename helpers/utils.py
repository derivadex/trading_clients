"""
Utils module
"""

from ddx_python.decimal import Decimal
import requests


def round_to_unit(val: Decimal, tick_size: int) -> Decimal:
    return val.quantize(tick_size)


def make_snapshot_url(webserver_url: str) -> str:
    return f"{webserver_url.replace('https','http',1)}/snapshot/addresses"


def get_contract_deployment_info(
    webserver_url: str, contract_deployment: str, snapshot_url: str = None
):
    if snapshot_url:
        r = requests.get(
            snapshot_url, params={"contractDeployment": contract_deployment}
        )
    else:
        r = requests.get(
            make_snapshot_url(webserver_url),
            params={"contractDeployment": contract_deployment},
        )
    return r.json()


def exchange_is_up(webserver_url: str, contract_deployment: str) -> bool:
    session = retrying_session()

    snapshot_is_up = session.get(
        make_snapshot_url(webserver_url),
        params={"contractDeployment": contract_deployment},
    ).ok

    return snapshot_is_up and operator_is_up(webserver_url)


def auditor_is_up(auditor_host: str) -> bool:
    session = retrying_session()
    # Q: http or https?
    return session.get(f"http://{auditor_host}:8766/trader").ok


def operator_is_up(webserver_url: str) -> bool:
    session = retrying_session()
    return session.get(f"{webserver_url}/v2/status").ok


def retrying_session(total=10, backoff_factor=1):
    session = requests.Session()
    retries = requests.adapters.Retry(
        total=total,
        backoff_factor=backoff_factor,
        status_forcelist=[404, 429, 500, 502, 503, 504],
    )
    adapter = requests.adapters.HTTPAdapter(max_retries=retries)
    session.mount("http://", adapter)
    session.mount("https://", adapter)

    return session
