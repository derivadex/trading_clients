"""
OrderIntent module
"""


class OrderIntent:
    """
    Defines an OrderIntent
    """

    def __init__(
        self,
        symbol,
        strategy,
        side,
        order_type,
        nonce,
        amount,
        price,
        stop_price,
        signature="",
        remaining_amount=None,
    ):
        self.symbol = symbol
        self.strategy = strategy
        self.side = side
        self.order_type = order_type
        self.nonce = nonce
        self.amount = amount
        self.price = price
        self.stop_price = stop_price
        self.signature = signature
        self.remaining_amount = (
            remaining_amount if remaining_amount is not None else amount
        )

    def __str__(self):
        return f"OrderIntent: symbol = {self.symbol}; strategy = {self.strategy}; side = {self.side}; order_type = {self.order_type}; nonce = {self.nonce}; amount = {self.amount}; price = {self.price}; stop_price = {self.stop_price}; signature = {self.signature}; remaining_amount = {self.remaining_amount}"
