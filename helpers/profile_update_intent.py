"""
ProfileUpdateIntent module
"""


class ProfileUpdateIntent:
    """
    Defines a ProfileUpdateIntent
    """

    def __init__(
        self,
        pay_fees_in_ddx,
        nonce,
        signature="",
    ):
        self.pay_fees_in_ddx = pay_fees_in_ddx
        self.nonce = nonce
        self.signature = signature

    def __str__(self):
        return f"ProfileUpdateIntent: pay_fees_in_ddx = {self.pay_fees_in_ddx}; nonce = {self.nonce}; signature = {self.signature}"
