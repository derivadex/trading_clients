"""
CancelIntent module
"""


class CancelIntent:
    """
    Defines a CancelIntent
    """

    def __init__(
        self,
        symbol: str,
        order_hash: str,
        nonce: str,
        signature: str = "",
    ):
        self.symbol = symbol
        self.order_hash = order_hash
        self.nonce = nonce
        self.signature = signature

    def __str__(self):
        return f"CancelIntent: symbol = {self.symbol}; order_hash = {self.order_hash}; nonce = {self.nonce}; signature = {self.signature};"
