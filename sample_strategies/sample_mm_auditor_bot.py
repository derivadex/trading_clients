"""
Sample DerivaDEX market making and Auditor bot
"""

import asyncio
import logging
from os import environ
import random
from typing import List, Optional, Dict
import simplejson as json
import jsonschema

import configargparse
from ddx_python.decimal import Decimal

from ddx_client.auditor.state.empty import Empty
from ddx_client.auditor.state.identifiers.book_order_identifier import (
    BookOrderIdentifier,
)
from ddx_client.auditor.state.identifiers.position_identifier import PositionIdentifier
from ddx_client.auditor.state.identifiers.strategy_identifier import StrategyIdentifier
from ddx_client.auditor.state.identifiers.trader_identifier import TraderIdentifier
from ddx_client.auditor.transactions.identifiers.fill_identifier import FillIdentifier
from ddx_client.auditor.transactions.identifiers.price_checkpoint_identifier import (
    PriceCheckpointIdentifier,
)
from ddx_client.auditor.transactions.price_checkpoint import PriceCheckpoint
from ddx_client.helpers.cancel_all_intent import CancelAllIntent
from ddx_client.helpers.cancel_intent import CancelIntent
from ddx_client.client.derivadex_client import DerivaDEXClient
from ddx_client.helpers.order_intent import OrderIntent
from ddx_client.helpers.utils import (
    round_to_unit,
    get_contract_deployment_info,
    auditor_is_up,
)

fmt_str = "[%(asctime)s] %(levelname)s @ line %(lineno)d: %(message)s"
log_level = environ.get("PYTHON_LOG").upper() if "PYTHON_LOG" in environ else 100
logging.basicConfig(level=log_level, format=fmt_str)
logger = logging.getLogger(__name__)


def create_maker_limit_order_intent(
    derivadex_client: DerivaDEXClient,
    symbol: str,
    base_px: Decimal,
    side: str,
    price_offset_index: int,
    price_offset: Decimal,
    quantity_per_level: Decimal,
) -> OrderIntent:
    """
    Create a maker limit order

    Parameters
    ----------
    derivadex_client : DerivaDEXClient
        DerivaDEX client wrapper
    symbol : str
        Market symbol
    base_px : Decimal
        Base price to adjust with offsets
    side : str
        Order side - "Bid" or "Ask"
    price_offset_index : int
        Level offset to determine price adjustment to base price
    price_offset : Decimal
        Specifies price between quoted levels
    quantity_per_level : Decimal
        Size to post for each quoted level

    Returns
    -------
    OrderIntent
        Signed order intent to be placed
    """

    price = round_to_unit(
        base_px * (Decimal("1") - price_offset * (price_offset_index + Decimal("1")))
        if side == "Bid"
        else base_px
        * (Decimal("1") + price_offset * (price_offset_index + Decimal("1"))),
        1 if symbol == "ETHPERP" else 0,
    )

    quantity = round_to_unit(
        Decimal(
            str(
                random.uniform(
                    float(quantity_per_level - quantity_per_level / Decimal("2")),
                    float(quantity_per_level + quantity_per_level / Decimal("2")),
                )
            )
        ),
        1,
    )

    return OrderIntent(
        symbol,
        "main",
        side,
        "Limit",
        f"0x{derivadex_client.get_encoded_nonce()}",
        quantity,
        price,
        Decimal("0"),
    )


def should_revisit_orders(
    price_checkpoint: PriceCheckpoint,
    ref_px_deviation_to_replace_orders: Decimal,
    deployment: str,
    ref_mark_px: Optional[Decimal] = None,
) -> bool:
    """
    Checks whether bot should revisit/refresh orders

    Parameters
    ----------
    price_checkpoint : Decimal
        Latest mark price for market
    ref_px_deviation_to_replace_orders : Decimal
        Specifies how much the reference price must change to
        refresh orders
    deployment : str
        Deployment name
    ref_mark_px : Decimal
        Reference mark price for market

    Returns
    -------
    bool
        Whether bot should refresh orders
    """

    mark_price = price_checkpoint.mark_price
    if (
        deployment == "geth"
        or ref_mark_px is None
        or abs((mark_price - ref_mark_px) / ref_mark_px)
        > ref_px_deviation_to_replace_orders
    ):
        logging.info(
            f"REFRESH: mark_price ({mark_price}) drifted from ref_mark_px ({ref_mark_px})"
        )
        return True
    return False


async def cancel_orders(derivadex_client: DerivaDEXClient, symbol_to_cancel: str):
    """
    Cancel active open orders

    Parameters
    ----------
    derivadex_client : DerivaDEXClient
        DerivaDEX client wrapper
    symbol_to_cancel: str
        Symbol to cancel
    """

    logging.info(f"CANCEL_ALL: canceling all")
    cancel_all_intent = CancelAllIntent(
        "main", f"0x{derivadex_client.get_encoded_nonce()}"
    )
    await derivadex_client.cancel_all_orders(cancel_all_intent)


async def place_orders(
    derivadex_client: DerivaDEXClient,
    symbol: str,
    base_px: Decimal,
    levels_to_quote: int,
    price_offset: Decimal,
    quantity_per_level: Decimal,
):
    """
    Place new orders around a base price

    Parameters
    ----------
    derivadex_client : DerivaDEXClient
        DerivaDEX client wrapper
    symbol : str
        Market symbol
    base_px : Decimal
        Base price to adjust with offsets
    levels_to_quote : int
        Number of levels on either side to quote
    price_offset : Decimal
        Specifies price between quoted levels
    quantity_per_level : Decimal
        Size to post for each quoted level
    """

    order_intents = []

    # Iterate through the levels to quote
    for level in range(levels_to_quote):
        # Create a new bid order intent for level
        bid_order_intent = create_maker_limit_order_intent(
            derivadex_client,
            symbol,
            base_px,
            "Bid",
            level,
            price_offset,
            quantity_per_level,
        )
        logging.info(
            f"POST: posting ({bid_order_intent.side}) order for ({bid_order_intent.amount}) @ ({bid_order_intent.price})"
        )
        order_intents.append(bid_order_intent)

        # Create a new ask order intent for level
        ask_order_intent = create_maker_limit_order_intent(
            derivadex_client,
            symbol,
            base_px,
            "Ask",
            level,
            price_offset,
            quantity_per_level,
        )
        logging.info(
            f"POST: posting ({ask_order_intent.side}) order for ({ask_order_intent.amount}) @ ({ask_order_intent.price})"
        )
        order_intents.append(ask_order_intent)

    # Submit created orders to the Auditor API for placement
    await derivadex_client.create_orders(order_intents)


async def deposit_if_necessary(
    derivadex_client: DerivaDEXClient,
    collateral_address: str,
    ddx_address: str,
    deposit_minimum: Decimal,
    deposit_amount: Decimal,
    faucet_private_key: str,
):
    strategy_leaf = derivadex_client.strategy_tracker.strategies[
        (
            f"0x00{derivadex_client.web3_account.address.lower()[2:]}",
            f"{StrategyIdentifier.generate_strategy_id_hash('main').hex()}",
        )
    ]

    deposited = False
    while (
        isinstance(strategy_leaf, Empty)
        or collateral_address not in strategy_leaf.free_collateral
        or strategy_leaf.free_collateral[collateral_address] < deposit_minimum
    ):
        if not deposited:
            nonce = derivadex_client.w3.eth.getTransactionCount(
                derivadex_client.web3_account.address
            )
            if faucet_private_key is not None:
                await derivadex_client.mint(
                    ddx_address, faucet_private_key, Decimal("1_000_000")
                )
                await derivadex_client.approve_ddx(
                    ddx_address, Decimal("1_000_000"), nonce=nonce
                )
                await derivadex_client.deposit_ddx(
                    Decimal("1_000_000"), nonce=nonce + 1
                )
                nonce += 2
            await derivadex_client.approve(
                collateral_address, deposit_amount, nonce=nonce
            )
            await derivadex_client.deposit(
                collateral_address, deposit_amount, nonce=nonce + 1
            )
            deposited = True

        logging.info("DEPOSIT: must deposit sufficient capital")
        await asyncio.sleep(5)
        strategy_leaf = derivadex_client.strategy_tracker.strategies[
            (
                f"0x00{derivadex_client.web3_account.address.lower()[2:]}",
                f"{StrategyIdentifier.generate_strategy_id_hash('main').hex()}",
            )
        ]
    if deposited:
        logging.info("DEPOSIT: sufficient collateral has been deposited")


async def main(
    config_json: Dict,
):
    """
    Main entry point for sample market making strategy

    Parameters
    ----------
    config_json : Dict
        Market maker config
    """

    if not auditor_is_up(config_json["auditor_host"]):
        raise RuntimeError(f"auditor at host {config_json['auditor_host']} is not up")

    deployment_info = get_contract_deployment_info(
        config_json["webserver_url"], config_json["contract_deployment"]
    )
    chain_id = deployment_info["chainId"]
    verifying_contract = deployment_info["addresses"]["derivaDEXAddress"]
    collateral_address = deployment_info["addresses"]["usdcAddress"]
    ddx_address = deployment_info["addresses"]["ddxAddress"]
    faucet_private_key = (
        config_json["faucet_private_key"]
        if "faucet_private_key" in config_json
        else None
    )

    symbols = [market["symbol"] for market in config_json["markets"]]

    price_checkpoint_topics = [
        f"TX_LOG/PRICE_CHECKPOINT/{symbol}/" for symbol in symbols
    ]
    book_order_topics = [
        f"STATE/BOOK_ORDER/{symbol}/{config_json['bot_address']}/0x2576ebd1/"
        for symbol in symbols
    ]
    fill_topics = [
        f"TX_LOG/FILL/{symbol}/{config_json['bot_address']}/0x2576ebd1/"
        for symbol in symbols
    ]
    position_topics = [
        f"STATE/POSITION/{symbol}/{config_json['bot_address']}/0x2576ebd1/"
        for symbol in symbols
    ]
    strategy_topics = [f"STATE/STRATEGY/{config_json['bot_address']}/0x2576ebd1/"]
    trader_topics = [f"STATE/TRADER/{config_json['bot_address']}/"]

    trader_identifiers = [
        TraderIdentifier.decode_topic_string_into_cls(trader_topic)
        for trader_topic in trader_topics
    ]
    strategy_identifiers = [
        StrategyIdentifier.decode_topic_string_into_cls(strategy_topic)
        for strategy_topic in strategy_topics
    ]
    position_identifiers = [
        PositionIdentifier.decode_topic_string_into_cls(position_topic)
        for position_topic in position_topics
    ]
    book_order_identifiers = [
        BookOrderIdentifier.decode_topic_string_into_cls(book_order_topic)
        for book_order_topic in book_order_topics
    ]
    price_checkpoint_identifiers = [
        PriceCheckpointIdentifier.decode_topic_string_into_cls(price_checkpoint_topic)
        for price_checkpoint_topic in price_checkpoint_topics
    ]
    fill_identifiers = [
        FillIdentifier.decode_topic_string_into_cls(fill_topic)
        for fill_topic in fill_topics
    ]

    derivadex_client = DerivaDEXClient(
        config_json["webserver_url"],
        config_json["auditor_host"],
        config_json["rpc_url"],
        config_json["contract_deployment"],
        chain_id,
        verifying_contract,
        symbols,
        trader_identifiers,
        strategy_identifiers,
        position_identifiers,
        book_order_identifiers,
        price_checkpoint_identifiers,
        fill_identifiers,
        config_json["private_key"],
        None,
    )

    await derivadex_client.start_network()
    await asyncio.sleep(5.0)

    ref_mark_pxs = {symbol: None for symbol in symbols}

    # # Sample WS data feeds
    # # print("OrderBooks", derivadex_client.order_book_tracker.order_books)
    # # print("Traders", derivadex_client.trader_tracker.traders)
    # # print("Strategies", derivadex_client.strategy_tracker.strategies)
    # # print("Positions", derivadex_client.position_tracker.positions)
    # # print("BookOrders", derivadex_client.book_order_tracker.book_orders)
    # # print("PriceCheckpoints", derivadex_client.price_checkpoint_tracker.latest_price_checkpoints)
    #
    while True:
        await deposit_if_necessary(
            derivadex_client,
            collateral_address,
            ddx_address,
            config_json["deposit_minimum"],
            config_json["deposit_amount"],
            faucet_private_key,
        )

        market = random.choice(config_json["markets"])
        latest_price_checkpoint = (
            derivadex_client.price_checkpoint_tracker.latest_price_checkpoints[
                market["symbol"]
            ]
        )
        if latest_price_checkpoint and should_revisit_orders(
            latest_price_checkpoint,
            market["ref_px_deviation_to_replace_orders"],
            config_json["contract_deployment"],
            ref_mark_pxs[market["symbol"]],
        ):
            ref_mark_pxs[market["symbol"]] = latest_price_checkpoint.mark_price

            await cancel_orders(derivadex_client, market["symbol"])

            # Random 20pt offset in either direction to trigger matches
            if market["trading_strategy"] == "MAKER":
                base_px = latest_price_checkpoint.index_price
            elif market["trading_strategy"] == "TAKER":
                base_px = Decimal(
                    str(
                        random.uniform(
                            float(latest_price_checkpoint.index_price - Decimal("20")),
                            float(latest_price_checkpoint.index_price + Decimal("20")),
                        )
                    )
                )
            else:
                base_px = Decimal(
                    str(
                        random.uniform(
                            float(
                                latest_price_checkpoint.index_price * Decimal("0.95")
                            ),
                            float(
                                latest_price_checkpoint.index_price * Decimal("1.05")
                            ),
                        )
                    )
                )

            # Black out period, bot stops providing liquidity
            if market["trading_strategy"] == "FUZZED" and random.random() < 0.01:
                logging.info(f"BLACK_OUT PERIOD...")
                await asyncio.sleep(30.0)

            if base_px > Decimal("0"):
                await place_orders(
                    derivadex_client,
                    market["symbol"],
                    base_px,
                    market["levels_to_quote"],
                    market["price_offset"],
                    market["quantity_per_level"],
                )

        await asyncio.sleep(config_json["sleep_rate"])


if __name__ == "__main__":
    p = configargparse.ArgParser()
    p.add_argument(
        "-c",
        "--config",
        required=False,
        help="config file path",
    )

    options = p.parse_args()

    print(options)
    print("----------")
    print(p.format_help())
    print("----------")
    print(p.format_values())

    if environ.get("CONFIG"):
        config_json = json.loads(environ.get("CONFIG"))
    else:
        with open(options.config, "r") as fp:
            config_json = json.load(fp)

    mm_schema = {
        "type": "object",
        "properties": {
            "webserver_url": {"type": "string"},
            "contract_deployment": {"type": "string"},
            "rpc_url": {"type": "string"},
            "auditor_host": {"type": "string"},
            "deposit_minimum": {"type": "number"},
            "deposit_amount": {"type": "number"},
            "sleep_rate": {"type": "number"},
            "bot_address": {"type": "string"},
            "private_key": {"type": "string"},
            "markets": {
                "type": "array",
                "items": {
                    "type": "object",
                    "properties": {
                        "symbol": {"type": "string"},
                        "levels_to_quote": {"type": "number"},
                        "price_offset": {"type": "number"},
                        "quantity_per_level": {"type": "number"},
                        "ref_px_deviation_to_replace_orders": {"type": "number"},
                        "trading_strategy": {"type": "string"},
                    },
                    "minProperties": 6,
                },
                "minItems": 1,
                "uniqueItems": True,
            },
        },
        "minProperties": 10,
    }

    jsonschema.validate(config_json, mm_schema)

    asyncio.run(main(config_json))
