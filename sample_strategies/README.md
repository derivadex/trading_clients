# Setup

## From source

To run the market maker, follow these steps:

1. If you don't already have it, we recommend setting up Anaconda/Python(>3) on your machine

2. Initialize and activate a Conda environment (located at the root level of the repo) from which you will run the market maker: `conda env create -f environment.yml && conda activate derivadex`

3. Set your PYTHONPATH: `export PYTHONPATH="${PYTHONPATH}:/your/full/path/upto/but/not/including/ddx_client"`

4. Navigate to the `sample_strategies` subdirectory and create an `.marketmaker.conf.json` file using the template: `cp .marketmaker.conf.json.template .marketmaker.conf.json`

5. Run the Auditor with: `PYTHON_LOG=verbose python sample_strategies/sample_mm_auditor_bot.py --config ".marketmaker.conf.json"`

You will immediately see logging messages (should be lots of green) with state initialized and transactions streaming through successfully.
