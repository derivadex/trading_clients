"""
DerivaDEX signature utils
"""

from eth_abi import encode_single
from eth_abi.utils.padding import zpad32_right
from eth_utils.crypto import keccak
from ddx_python.decimal import Decimal

from ddx_client.helpers.cancel_all_intent import CancelAllIntent
from ddx_client.helpers.cancel_intent import CancelIntent
from ddx_client.helpers.order_intent import OrderIntent
from ddx_client.helpers.profile_update_intent import ProfileUpdateIntent
from ddx_client.helpers.withdraw_intent import WithdrawIntent

eip191_header = b"\x19\x01"


def compute_eip712_domain_struct_hash(chain_id: int, verifying_contract: str):
    return keccak(
        keccak(
            b"EIP712Domain("
            + b"string name,"
            + b"string version,"
            + b"uint256 chainId,"
            + b"address verifyingContract"
            + b")"
        )
        + keccak(b"DerivaDEX")
        + keccak(b"1")
        + encode_single("uint256", chain_id)
        + encode_single("address", verifying_contract)
    )


def compute_eip712_create_order_hash(
    chain_id: int, verifying_contract: str, order_intent: OrderIntent
) -> str:
    # keccak-256 hash of the encoded schema for the place order command
    eip712_order_params_schema_hash = keccak(
        b"OrderParams("
        + b"bytes32 symbol,"
        + b"bytes32 strategy,"
        + b"uint256 side,"
        + b"uint256 orderType,"
        + b"bytes32 nonce,"
        + b"uint256 amount,"
        + b"uint256 price,"
        + b"uint256 stopPrice"
        + b")"
    )

    # Ensure decimal value has no more than 6 decimals of precision
    def round_to_unit(val: Decimal) -> Decimal:
        return val.quantize(6)

    # Scale up to DDX grains format (i.e. multiply by 1e18)
    def to_base_unit_amount(val: Decimal, decimals: int) -> int:
        return int(round_to_unit(val) * 10 ** decimals)

    # Convert order side string to int representation
    def order_side_to_int(order_side: str) -> int:
        if order_side == "Bid":
            return 0
        elif order_side == "Ask":
            return 1
        return 2

    # Convert order type string to int representation
    def order_type_to_int(order_type: str) -> int:
        if order_type == "Limit":
            return 0
        elif order_type == "Market":
            return 1
        return 2

    eip712_message_struct_hash = keccak(
        eip712_order_params_schema_hash
        + zpad32_right(
            len(order_intent.symbol).to_bytes(1, byteorder="little")
            + order_intent.symbol.encode("utf8")
        )
        + zpad32_right(
            len(order_intent.strategy).to_bytes(1, byteorder="little")
            + order_intent.strategy.encode("utf8")
        )
        + encode_single("uint256", order_side_to_int(order_intent.side))
        + encode_single("uint256", order_type_to_int(order_intent.order_type))
        + encode_single("bytes32", bytes.fromhex(order_intent.nonce[2:]))
        + encode_single("uint256", to_base_unit_amount(order_intent.amount, 6))
        + encode_single("uint256", to_base_unit_amount(order_intent.price, 6))
        + encode_single("uint256", to_base_unit_amount(order_intent.stop_price, 6))
    )

    # Converting bytes result to a hexadecimal string
    return keccak(
        eip191_header
        + compute_eip712_domain_struct_hash(chain_id, verifying_contract)
        + eip712_message_struct_hash
    ).hex()


def compute_eip712_cancel_order_hash(
    chain_id: int, verifying_contract: str, cancel_intent: CancelIntent
) -> str:
    # keccak-256 hash of the encoded schema for the place order command
    eip712_cancel_params_schema_hash = keccak(
        b"CancelOrderParams("
        + b"bytes32 symbol,"
        + b"bytes32 orderHash,"
        + b"bytes32 nonce"
        + b")"
    )

    eip712_message_struct_hash = keccak(
        eip712_cancel_params_schema_hash
        + zpad32_right(
            len(cancel_intent.symbol).to_bytes(1, byteorder="little")
            + cancel_intent.symbol.encode("utf8")
        )
        + encode_single("bytes32", bytes.fromhex(cancel_intent.order_hash[2:]))
        + encode_single("bytes32", bytes.fromhex(cancel_intent.nonce[2:]))
    )

    # Converting bytes result to a hexadecimal string
    return keccak(
        eip191_header
        + compute_eip712_domain_struct_hash(chain_id, verifying_contract)
        + eip712_message_struct_hash
    ).hex()


def compute_eip712_cancel_all_hash(
    chain_id: int, verifying_contract: str, cancel_all_intent: CancelAllIntent
) -> str:
    # keccak-256 hash of the encoded schema for the cancel all command
    eip712_cancel_all_schema_hash = keccak(
        b"CancelAllParams(" + b"bytes32 strategy," + b"bytes32 nonce" + b")"
    )

    eip712_message_struct_hash = keccak(
        eip712_cancel_all_schema_hash
        + zpad32_right(
            len(cancel_all_intent.strategy).to_bytes(1, byteorder="little")
            + cancel_all_intent.strategy.encode("utf8")
        )
        + encode_single("bytes32", bytes.fromhex(cancel_all_intent.nonce[2:]))
    )

    # Converting bytes result to a hexadecimal string
    return keccak(
        eip191_header
        + compute_eip712_domain_struct_hash(chain_id, verifying_contract)
        + eip712_message_struct_hash
    ).hex()


def compute_eip712_withdraw_hash(
    chain_id: int, verifying_contract: str, withdraw_intent: WithdrawIntent
) -> str:
    # keccak-256 hash of the encoded schema for the withdraw command
    eip712_withdraw_schema_hash = keccak(
        b"WithdrawParams("
        + b"address recipientAddress,"
        + b"bytes32 strategyId,"
        + b"address currency,"
        + b"uint128 amount,"
        + b"bytes32 nonce"
        + b")"
    )

    # Ensure decimal value has no more than 6 decimals of precision
    def round_to_unit(val: Decimal) -> Decimal:
        return val.quantize(6)

    # Scale up to DDX grains format (i.e. multiply by 1e18)
    def to_base_unit_amount(val: Decimal, decimals: int) -> int:
        return int(round_to_unit(val) * 10 ** decimals)

    eip712_message_struct_hash = keccak(
        eip712_withdraw_schema_hash
        + encode_single("address", withdraw_intent.recipient_address)
        + zpad32_right(
            len(withdraw_intent.strategy_id).to_bytes(1, byteorder="little")
            + withdraw_intent.strategy_id.encode("utf8")
        )
        + encode_single("address", withdraw_intent.currency)
        + encode_single("uint128", to_base_unit_amount(withdraw_intent.amount, 6))
        + encode_single("bytes32", bytes.fromhex(withdraw_intent.nonce[2:]))
    )

    # Converting bytes result to a hexadecimal string
    return keccak(
        eip191_header
        + compute_eip712_domain_struct_hash(chain_id, verifying_contract)
        + eip712_message_struct_hash
    ).hex()


def compute_eip712_profile_update_hash(
    chain_id: int, verifying_contract: str, profile_update_intent: ProfileUpdateIntent
) -> str:
    # keccak-256 hash of the encoded schema for the profile update command
    eip712_withdraw_schema_hash = keccak(
        b"UpdateProfileParams(" + b"bool payFeesInDdx," + b"bytes32 nonce" + b")"
    )

    eip712_message_struct_hash = keccak(
        eip712_withdraw_schema_hash
        + encode_single("bool", profile_update_intent.pay_fees_in_ddx)
        + encode_single("bytes32", bytes.fromhex(profile_update_intent.nonce[2:]))
    )

    # Converting bytes result to a hexadecimal string
    return keccak(
        eip191_header
        + compute_eip712_domain_struct_hash(chain_id, verifying_contract)
        + eip712_message_struct_hash
    ).hex()
