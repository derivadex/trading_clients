"""
PositionTracker module
"""

import asyncio
from collections import defaultdict
from typing import DefaultDict, Optional, List, Dict
import websockets
import simplejson as json

from ddx_client.auditor.state.empty import Empty
from ddx_client.auditor.state.identifiers.position_identifier import (
    PositionIdentifier,
)
from ddx_client.auditor.state.item_message import ItemMessage
from ddx_client.auditor.state.position import Position
from ddx_client.auditor.utils import ComplexOutputEncoder
from ddx_client.client.asyncio_utils import empty_queue
from ddx_client.client.trackers.tracker import Tracker
from ddx_client.client.websocket_message import (
    WebsocketMessage,
    WebsocketMessageType,
    WebsocketEventType,
)


class PositionTracker(Tracker):
    """
    Defines a PositionTracker
    """

    def __init__(
        self,
        position_identifiers: Optional[List[PositionIdentifier]] = None,
        ws_uri: str = None,
    ):
        """
        Initialize a PositionTracker. A PositionTracker is an async task
        that can send PriceCheckpoint-related data, or receive real-time
        Positions a user has subscribed to.

        Parameters
        ----------
        position_identifiers: Optional[List[PositionIdentifier]]
            List of PositionIdentifiers user is subscribing to
        ws_uri : str
           WebSocket URI
        """

        self.position_identifiers = position_identifiers
        self.ws_uri = ws_uri

        self.positions: DefaultDict = defaultdict(lambda: Empty())
        self.position_stream_queue: asyncio.Queue[ItemMessage] = asyncio.Queue()

    def _reset(self):
        self.positions.clear()
        empty_queue(self.position_stream_queue)

    async def _consumer_handler(self):
        """
        Consume data from the Auditor after subscribing to the various
        Identifiers.
        """

        while True:
            try:
                async with websockets.connect(self.ws_uri, max_size=2 ** 32) as ws:
                    ws: websockets.WebSocketClientProtocol = ws
                    for position_identifier in self.position_identifiers:
                        subscribe_request = WebsocketMessage(
                            WebsocketMessageType.SUBSCRIBE,
                            position_identifier.topic_string,
                        )
                        subscribe_request_json = ComplexOutputEncoder().encode(
                            subscribe_request
                        )
                        await ws.send(subscribe_request_json)
                    async for raw_msg in self._inner_messages(ws):
                        message = json.loads(raw_msg)
                        position_leaf_event_message = ItemMessage.decode_value_into_cls(
                            message
                        )
                        self.position_stream_queue.put_nowait(
                            position_leaf_event_message
                        )

            except asyncio.CancelledError:
                raise
            except Exception:
                print(
                    "Unexpected error with WebSocket connection. Retrying after 30 seconds...",
                )
                await asyncio.sleep(30.0)
            finally:
                self._reset()

    async def _process_item_data_entry(self, item_data_entry: Dict):
        """
        Process individual state leaf update message from the Auditor.

        Parameters
        ----------
        item_data_entry: dict
            An individual state leaf update message entry
        """

        # Derive PositionIdentifier based on the message topic
        identifier = PositionIdentifier.decode_topic_string_into_cls(
            item_data_entry["t"]
        )

        # Save Position leaf in live-updating dict
        self.positions[
            (
                identifier.symbol,
                identifier.trader_address,
                identifier.abbrev_strategy_id_hash,
            )
        ] = Position.decode_value_into_cls(item_data_entry["c"])

    async def _producer_handler(self):
        """
        Produce data to be saved and/or sent to the Trader based on
        what has already been consumed earlier.
        """

        while True:
            message = await self.position_stream_queue.get()
            if message.message_event == WebsocketEventType.PARTIAL:
                for item_data_entry in message.message_content["item_data"]:
                    await self._process_item_data_entry(item_data_entry)
            else:
                await self._process_item_data_entry(
                    message.message_content["item_data"]
                )
