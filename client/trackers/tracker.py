"""
Tracker module
"""

import asyncio
from typing import AsyncIterable
import websockets
from ddx_client.client.asyncio_utils import safe_ensure_future, safe_gather


class Tracker:
    """
    Defines a Tracker
    """

    async def start(self):
        """
        Set up consumer and producer tasks
        """

        consumer_task = safe_ensure_future(self._consumer_handler())
        producer_task = safe_ensure_future(self._producer_handler())
        await safe_gather(consumer_task, producer_task)

    async def _inner_messages(
        self, ws: websockets.WebSocketClientProtocol
    ) -> AsyncIterable[str]:
        try:
            while True:
                try:
                    msg: str = await asyncio.wait_for(ws.recv(), timeout=30.0)
                    yield msg
                except asyncio.TimeoutError:
                    try:
                        pong_waiter = await ws.ping()
                        await asyncio.wait_for(pong_waiter, timeout=30.0)
                    except asyncio.TimeoutError:
                        raise
        except asyncio.TimeoutError:
            print("WebSocket ping timed out. Going to reconnect...")
            return
        except websockets.ConnectionClosed:
            return
        finally:
            await ws.close()

    def reset(self):
        raise NotImplementedError

    async def _consumer_handler(self):
        raise NotImplementedError

    async def _producer_handler(self):
        raise NotImplementedError
