"""
CommandTracker module
"""

import asyncio
from collections import defaultdict
from typing import DefaultDict
import websockets
import simplejson as json

from ddx_client.auditor.state.empty import Empty
from ddx_client.auditor.utils import ComplexOutputEncoder
from ddx_client.client.asyncio_utils import empty_queue
from ddx_client.client.trackers.tracker import Tracker
from ddx_client.client.websocket_message import (
    WebsocketMessage,
    WebsocketMessageType,
)


class CommandTracker(Tracker):
    """
    Defines a CommandTracker
    """

    def __init__(self, ws_uri: str = None):
        """
        Initialize a CommandTracker. A CommandTracker is an async task
        that can send Command-related data, or receive real-time
        Command-related responses from the Auditor. This allows Traders
        to send messages such as posting new orders, cancellations, or
        withdrawal commands, and receive sequenced / erroneous messages
        from the Trader API back.

        Parameters
        ----------
        ws_uri : str
           WebSocket URI
        """

        self.ws_uri = ws_uri

        self.command_input_queue: asyncio.Queue[WebsocketMessage] = asyncio.Queue()
        self.command_stream_queue: asyncio.Queue[WebsocketMessage] = asyncio.Queue()

    def _reset(self):
        empty_queue(self.command_input_queue)
        empty_queue(self.command_stream_queue)

    async def _consumer_handler(self):
        """
        Consume data from the Auditor after subscribing to the various
        Identifiers.
        """

        while True:
            try:
                async with websockets.connect(self.ws_uri, max_size=2 ** 32) as ws:
                    self.ws: websockets.WebSocketClientProtocol = ws

                    # Subscribe to the "COMMAND_RECEIPT" channel, which
                    # will emit Sequenced | Info | SafetyFailure
                    # messages
                    subscribe_request = WebsocketMessage(
                        WebsocketMessageType.SUBSCRIBE,
                        "COMMAND_RECEIPT",
                    )
                    subscribe_request_json = ComplexOutputEncoder().encode(
                        subscribe_request
                    )
                    await self.ws.send(subscribe_request_json)
                    async for raw_msg in self._inner_messages(ws):
                        message = json.loads(raw_msg)
                        command_event_message = WebsocketMessage.decode_value_into_cls(
                            message
                        )
                        self.command_stream_queue.put_nowait(command_event_message)

            except asyncio.CancelledError:
                raise
            except Exception:
                print(
                    "Unexpected error with WebSocket connection. Retrying after 30 seconds...",
                )
                await asyncio.sleep(30.0)
            finally:
                self._reset()

    async def _producer_handler(self):
        """
        Produce data to be saved and/or sent to the Trader based on
        what has already been consumed earlier.
        """

        while True:
            message = await self.command_input_queue.get()
            try:
                await self.ws.send(ComplexOutputEncoder().encode(message))
            except Exception as e:
                print("err", e)
                pass
