"""
BookOrderTracker module
"""

import asyncio
from collections import defaultdict
from typing import DefaultDict, Optional, List, Dict
import websockets
import simplejson as json

from ddx_client.auditor.state.empty import Empty
from ddx_client.auditor.state.identifiers.book_order_identifier import (
    BookOrderIdentifier,
)
from ddx_client.auditor.state.item_message import ItemMessage
from ddx_client.auditor.state.book_order import BookOrder
from ddx_client.auditor.utils import ComplexOutputEncoder
from ddx_client.client.asyncio_utils import empty_queue
from ddx_client.client.trackers.tracker import Tracker
from ddx_client.client.websocket_message import (
    WebsocketMessage,
    WebsocketMessageType,
    WebsocketEventType,
)


class BookOrderTracker(Tracker):
    """
    Defines a BookOrderTracker
    """

    def __init__(
        self,
        book_order_identifiers: Optional[List[BookOrderIdentifier]] = None,
        ws_uri: str = None,
    ):
        """
        Initialize a BookOrderTracker. A BookOrderTracker is an async
        task that can send BookOrder-related data, or receive real-time
        BookOrders a user has subscribed to.

        Parameters
        ----------
        book_order_identifiers: Optional[List[BookOrderIdentifier]]
            List of BookOrderIdentifiers user is subscribing to
        ws_uri : str
           WebSocket URI
        """

        self.book_order_identifiers = book_order_identifiers
        self.ws_uri = ws_uri

        self.book_orders: DefaultDict = defaultdict(lambda: Empty())
        self.book_order_stream_queue: asyncio.Queue[ItemMessage] = asyncio.Queue()

    def _reset(self):
        self.book_orders.clear()
        empty_queue(self.book_order_stream_queue)

    async def _consumer_handler(self):
        """
        Consume data from the Auditor after subscribing to the various
        Identifiers.
        """

        while True:
            try:
                async with websockets.connect(self.ws_uri, max_size=2 ** 32) as ws:
                    ws: websockets.WebSocketClientProtocol = ws
                    for book_order_identifier in self.book_order_identifiers:
                        subscribe_request = WebsocketMessage(
                            WebsocketMessageType.SUBSCRIBE,
                            book_order_identifier.topic_string,
                        )
                        subscribe_request_json = ComplexOutputEncoder().encode(
                            subscribe_request
                        )
                        await ws.send(subscribe_request_json)
                    async for raw_msg in self._inner_messages(ws):
                        message = json.loads(raw_msg)
                        book_order_leaf_event_message = (
                            ItemMessage.decode_value_into_cls(message)
                        )
                        self.book_order_stream_queue.put_nowait(
                            book_order_leaf_event_message
                        )

            except asyncio.CancelledError:
                raise
            except Exception:
                print(
                    "Unexpected error with WebSocket connection. Retrying after 30 seconds...",
                )
                await asyncio.sleep(30.0)
            finally:
                self._reset()

    async def _process_item_data_entry(self, item_data_entry: Dict):
        """
        Process individual state leaf update message from the Auditor.

        Parameters
        ----------
        item_data_entry: dict
            An individual state leaf update message entry
        """

        # Construct BookOrderIdentifier given the message topic
        identifier = BookOrderIdentifier.decode_topic_string_into_cls(
            item_data_entry["t"]
        )

        # Obtain BookOrder leaf that has been updated
        book_order = BookOrder.decode_value_into_cls(item_data_entry["c"])

        if isinstance(book_order, Empty):
            # If BookOrder is now empty, we remove from the
            # live-updating dict
            del self.book_orders[
                (
                    identifier.symbol,
                    identifier.trader_address,
                    identifier.abbrev_strategy_id_hash,
                    identifier.order_hash,
                )
            ]
            return

        # Update the live-updating dict with the latest BookOrder
        self.book_orders[
            (
                identifier.symbol,
                identifier.trader_address,
                identifier.abbrev_strategy_id_hash,
                identifier.order_hash,
            )
        ] = book_order

    async def _producer_handler(self):
        """
        Produce data to be saved and/or sent to the Trader based on
        what has already been consumed earlier.
        """

        while True:
            message = await self.book_order_stream_queue.get()
            if message.message_event == WebsocketEventType.PARTIAL:
                # Process list of BookOrder leaves obtained in the
                # Partial response
                for item_data_entry in message.message_content["item_data"]:
                    await self._process_item_data_entry(item_data_entry)
            else:
                # Process individual BookOrder leaf obtained in the
                # Update response
                await self._process_item_data_entry(
                    message.message_content["item_data"]
                )
