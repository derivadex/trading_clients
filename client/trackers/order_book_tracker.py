"""
OrderBookTracker module
"""

import asyncio
from collections import defaultdict
from typing import DefaultDict, Optional
import websockets
import simplejson as json

from ddx_client.auditor.state.identifiers.book_order_identifier import (
    BookOrderIdentifier,
)
from ddx_client.auditor.state.item_message import ItemMessage
from ddx_client.auditor.utils import ComplexOutputEncoder
from ddx_client.client.asyncio_utils import empty_queue
from ddx_client.client.trackers.order_book import OrderBook
from ddx_client.client.trackers.tracker import Tracker
from ddx_client.client.websocket_message import (
    WebsocketMessage,
    WebsocketMessageType,
    WebsocketEventType,
)


class OrderBookTracker(Tracker):
    """
    Defines a OrderBookTracker
    """

    def __init__(self, symbols: Optional[str] = None, ws_uri: str = None):
        """
        Initialize an OrderBookTracker. A OrderBookTracker is an async
        task that can send BookOrder-related data, or receive real-time
        BookOrders a user has subscribed to to populate a local order
        book.

        Parameters
        ----------
        symbols: Optional[str]
            List of market symbols user is subscribing to
        ws_uri : str
           WebSocket URI
        """

        self.book_order_identifiers = [
            BookOrderIdentifier(symbol, None, None, None) for symbol in symbols
        ]
        self.ws_uri = ws_uri

        self.order_books: DefaultDict[str, OrderBook] = defaultdict(lambda: OrderBook())
        self.book_order_stream_queue: asyncio.Queue[ItemMessage] = asyncio.Queue()

    def _reset(self):
        self.order_books.clear()
        empty_queue(self.book_order_stream_queue)

    async def _consumer_handler(self):
        """
        Consume data from the Auditor after subscribing to the various
        Identifiers.
        """

        while True:
            try:
                async with websockets.connect(self.ws_uri, max_size=2 ** 32) as ws:
                    ws: websockets.WebSocketClientProtocol = ws
                    for book_order_identifier in self.book_order_identifiers:
                        subscribe_request = WebsocketMessage(
                            WebsocketMessageType.SUBSCRIBE,
                            book_order_identifier.topic_string,
                        )
                        subscribe_request_json = ComplexOutputEncoder().encode(
                            subscribe_request
                        )
                        await ws.send(subscribe_request_json)
                    async for raw_msg in self._inner_messages(ws):
                        message = json.loads(raw_msg)
                        book_order_leaf_event_message = (
                            ItemMessage.decode_value_into_cls(message)
                        )
                        self.book_order_stream_queue.put_nowait(
                            book_order_leaf_event_message
                        )

            except asyncio.CancelledError:
                raise
            except Exception:
                print(
                    "Unexpected error with WebSocket connection. Retrying after 30 seconds...",
                )
                await asyncio.sleep(30.0)
            finally:
                self._reset()

    async def _producer_handler(self):
        """
        Produce data to be saved and/or sent to the Trader based on
        what has already been consumed earlier.
        """

        while True:
            message = await self.book_order_stream_queue.get()

            # Construct BookOrderIdentifier based on message topic
            book_order_identifier = BookOrderIdentifier.decode_topic_string_into_cls(
                message.message_type
            )

            # Handle either a Partial or Update response
            if message.message_event == WebsocketEventType.PARTIAL:
                # If message is a Partial, we must process the snapshot
                # of the whole order book at this time
                await self.order_books[
                    book_order_identifier.symbol
                ].process_book_order_partial(message)
            else:
                # If message is an Update, we must process the
                # individual updated message
                await self.order_books[
                    book_order_identifier.symbol
                ].process_book_order_update(message)
