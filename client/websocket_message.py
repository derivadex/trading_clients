"""
WebsocketMessage module
"""

from enum import Enum
from typing import Dict


class WebsocketMessageType(str, Enum):
    GET = "Get"
    SUBSCRIBE = "Subscribe"
    REQUEST = "Request"
    INFO = "Info"
    SEQUENCED = "Sequenced"
    SAFETY_FAILURE = "SafetyFailure"


class WebsocketEventType(str, Enum):
    PARTIAL = "Partial"
    UPDATE = "Update"
    SNAPSHOT = "Snapshot"
    HEAD = "Head"
    TAIL = "Tail"


class WebsocketMessage:
    def __init__(self, message_type: [WebsocketMessageType, str], message_content):
        self.message_type = message_type
        self.message_content = message_content

    @classmethod
    def decode_value_into_cls(cls, raw_websocket_message: Dict):
        """
        Decode a raw websocket message into class

        Parameters
        ----------
        raw_websocket_message : Dict
            Raw websocket message
        """

        return cls(
            raw_websocket_message["t"],
            raw_websocket_message["c"],
        )

    def repr_json(self):
        return {"t": self.message_type, "c": self.message_content}
