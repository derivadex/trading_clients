"""Generated wrapper for Trader Solidity contract."""

# pylint: disable=too-many-arguments

import json
from typing import (  # pylint: disable=unused-import
    Any,
    List,
    Optional,
    Tuple,
    Union,
)

from eth_utils import to_checksum_address
from mypy_extensions import TypedDict  # pylint: disable=unused-import
from hexbytes import HexBytes
from web3 import Web3
from web3.contract import ContractFunction
from web3.datastructures import AttributeDict
from web3.providers.base import BaseProvider

from zero_ex.contract_wrappers.bases import ContractMethod, Validator
from zero_ex.contract_wrappers.tx_params import TxParams


# Try to import a custom validator class definition; if there isn't one,
# declare one that we can instantiate for the default argument to the
# constructor for Trader below.
try:
    # both mypy and pylint complain about what we're doing here, but this
    # works just fine, so their messages have been disabled here.
    from . import (  # type: ignore # pylint: disable=import-self
        TraderValidator,
    )
except ImportError:

    class TraderValidator(Validator):  # type: ignore
        """No-op input validator."""


try:
    from .middleware import MIDDLEWARE  # type: ignore
except ImportError:
    pass


class SharedDefsWithdrawalData(TypedDict):
    """Python representation of a tuple or struct.

    Solidity compiler output does not include the names of structs that appear
    in method definitions.  A tuple found in an ABI may have been written in
    Solidity as a literal, anonymous tuple, or it may have been written as a
    named `struct`:code:, but there is no way to tell from the compiler
    output.  This class represents a tuple that appeared in a method
    definition.  Its name is derived from a hash of that tuple's field names,
    and every method whose ABI refers to a tuple with that same list of field
    names will have a generated wrapper method that refers to this class.

    Any members of type `bytes`:code: should be encoded as UTF-8, which can be
    accomplished via `str.encode("utf_8")`:code:
    """

    tokens: List[str]

    amounts: List[int]


class TraderDefsStrategyData(TypedDict):
    """Python representation of a tuple or struct.

    Solidity compiler output does not include the names of structs that appear
    in method definitions.  A tuple found in an ABI may have been written in
    Solidity as a literal, anonymous tuple, or it may have been written as a
    named `struct`:code:, but there is no way to tell from the compiler
    output.  This class represents a tuple that appeared in a method
    definition.  Its name is derived from a hash of that tuple's field names,
    and every method whose ABI refers to a tuple with that same list of field
    names will have a generated wrapper method that refers to this class.

    Any members of type `bytes`:code: should be encoded as UTF-8, which can be
    accomplished via `str.encode("utf_8")`:code:
    """

    strategyId: Union[bytes, str]

    freeCollateral: SharedDefsWithdrawalData

    frozenCollateral: SharedDefsWithdrawalData

    maxLeverage: int

    frozen: bool


class TraderDefsExchangeCollateral(TypedDict):
    """Python representation of a tuple or struct.

    Solidity compiler output does not include the names of structs that appear
    in method definitions.  A tuple found in an ABI may have been written in
    Solidity as a literal, anonymous tuple, or it may have been written as a
    named `struct`:code:, but there is no way to tell from the compiler
    output.  This class represents a tuple that appeared in a method
    definition.  Its name is derived from a hash of that tuple's field names,
    and every method whose ABI refers to a tuple with that same list of field
    names will have a generated wrapper method that refers to this class.

    Any members of type `bytes`:code: should be encoded as UTF-8, which can be
    accomplished via `str.encode("utf_8")`:code:
    """

    underlyingToken: str

    flavor: int

    isListed: bool


class TraderDefsTrader(TypedDict):
    """Python representation of a tuple or struct.

    Solidity compiler output does not include the names of structs that appear
    in method definitions.  A tuple found in an ABI may have been written in
    Solidity as a literal, anonymous tuple, or it may have been written as a
    named `struct`:code:, but there is no way to tell from the compiler
    output.  This class represents a tuple that appeared in a method
    definition.  Its name is derived from a hash of that tuple's field names,
    and every method whose ABI refers to a tuple with that same list of field
    names will have a generated wrapper method that refers to this class.

    Any members of type `bytes`:code: should be encoded as UTF-8, which can be
    accomplished via `str.encode("utf_8")`:code:
    """

    ddxBalance: int

    ddxWalletContract: str


class TraderDefsTraderData(TypedDict):
    """Python representation of a tuple or struct.

    Solidity compiler output does not include the names of structs that appear
    in method definitions.  A tuple found in an ABI may have been written in
    Solidity as a literal, anonymous tuple, or it may have been written as a
    named `struct`:code:, but there is no way to tell from the compiler
    output.  This class represents a tuple that appeared in a method
    definition.  Its name is derived from a hash of that tuple's field names,
    and every method whose ABI refers to a tuple with that same list of field
    names will have a generated wrapper method that refers to this class.

    Any members of type `bytes`:code: should be encoded as UTF-8, which can be
    accomplished via `str.encode("utf_8")`:code:
    """

    freeDDXBalance: int

    frozenDDXBalance: int

    referralAddress: str


class AddExchangeCollateralMethod(ContractMethod):  # pylint: disable=invalid-name
    """Various interfaces to the addExchangeCollateral method."""

    def __init__(
        self,
        web3_or_provider: Union[Web3, BaseProvider],
        contract_address: str,
        contract_function: ContractFunction,
        validator: Validator = None,
    ):
        """Persist instance data."""
        super().__init__(web3_or_provider, contract_address, validator)
        self._underlying_method = contract_function

    def validate_and_normalize_inputs(self, collateral_token: str):
        """Validate the inputs to the addExchangeCollateral method."""
        self.validator.assert_valid(
            method_name="addExchangeCollateral",
            parameter_name="_collateralToken",
            argument_value=collateral_token,
        )
        collateral_token = self.validate_and_checksum_address(collateral_token)
        return collateral_token

    def call(self, collateral_token: str, tx_params: Optional[TxParams] = None) -> None:
        """Execute underlying contract method via eth_call.

        This function purposefully prevents governance from adding collateral
        flavors that are not Vanilla. Claiming Compound or Aave rewards has not
        been implemented yet, so adding these tokens as collateral will
        encourage users to waste their rewards.

        :param _collateralToken: The collateral token to add.
        :param tx_params: transaction parameters
        :returns: the return value of the underlying method.
        """
        (collateral_token) = self.validate_and_normalize_inputs(collateral_token)
        tx_params = super().normalize_tx_params(tx_params)
        self._underlying_method(collateral_token).call(tx_params.as_dict())

    def send_transaction(
        self, collateral_token: str, tx_params: Optional[TxParams] = None
    ) -> Union[HexBytes, bytes]:
        """Execute underlying contract method via eth_sendTransaction.

        This function purposefully prevents governance from adding collateral
        flavors that are not Vanilla. Claiming Compound or Aave rewards has not
        been implemented yet, so adding these tokens as collateral will
        encourage users to waste their rewards.

        :param _collateralToken: The collateral token to add.
        :param tx_params: transaction parameters
        """
        (collateral_token) = self.validate_and_normalize_inputs(collateral_token)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(collateral_token).transact(tx_params.as_dict())

    def build_transaction(
        self, collateral_token: str, tx_params: Optional[TxParams] = None
    ) -> dict:
        """Construct calldata to be used as input to the method."""
        (collateral_token) = self.validate_and_normalize_inputs(collateral_token)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(collateral_token).buildTransaction(
            tx_params.as_dict()
        )

    def estimate_gas(
        self, collateral_token: str, tx_params: Optional[TxParams] = None
    ) -> int:
        """Estimate gas consumption of method call."""
        (collateral_token) = self.validate_and_normalize_inputs(collateral_token)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(collateral_token).estimateGas(
            tx_params.as_dict()
        )


class DepositMethod(ContractMethod):  # pylint: disable=invalid-name
    """Various interfaces to the deposit method."""

    def __init__(
        self,
        web3_or_provider: Union[Web3, BaseProvider],
        contract_address: str,
        contract_function: ContractFunction,
        validator: Validator = None,
    ):
        """Persist instance data."""
        super().__init__(web3_or_provider, contract_address, validator)
        self._underlying_method = contract_function

    def validate_and_normalize_inputs(
        self,
        collateral_address: str,
        strategy_id: Union[bytes, str],
        amount: int,
    ):
        """Validate the inputs to the deposit method."""
        self.validator.assert_valid(
            method_name="deposit",
            parameter_name="_collateralAddress",
            argument_value=collateral_address,
        )
        collateral_address = self.validate_and_checksum_address(collateral_address)
        self.validator.assert_valid(
            method_name="deposit",
            parameter_name="_strategyId",
            argument_value=strategy_id,
        )
        self.validator.assert_valid(
            method_name="deposit",
            parameter_name="_amount",
            argument_value=amount,
        )
        return (collateral_address, strategy_id, amount)

    def call(
        self,
        collateral_address: str,
        strategy_id: Union[bytes, str],
        amount: int,
        tx_params: Optional[TxParams] = None,
    ) -> None:
        """Execute underlying contract method via eth_call.

        :param _amount: The amount to depost.
        :param _collateralAddress: The address of the collateral that should be
            deposited.
        :param _strategyId: The ID of the strategy to deposit into.
        :param tx_params: transaction parameters
        :returns: the return value of the underlying method.
        """
        (
            collateral_address,
            strategy_id,
            amount,
        ) = self.validate_and_normalize_inputs(collateral_address, strategy_id, amount)
        tx_params = super().normalize_tx_params(tx_params)
        self._underlying_method(collateral_address, strategy_id, amount).call(
            tx_params.as_dict()
        )

    def send_transaction(
        self,
        collateral_address: str,
        strategy_id: Union[bytes, str],
        amount: int,
        tx_params: Optional[TxParams] = None,
    ) -> Union[HexBytes, bytes]:
        """Execute underlying contract method via eth_sendTransaction.

        :param _amount: The amount to depost.
        :param _collateralAddress: The address of the collateral that should be
            deposited.
        :param _strategyId: The ID of the strategy to deposit into.
        :param tx_params: transaction parameters
        """
        (
            collateral_address,
            strategy_id,
            amount,
        ) = self.validate_and_normalize_inputs(collateral_address, strategy_id, amount)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(
            collateral_address, strategy_id, amount
        ).transact(tx_params.as_dict())

    def build_transaction(
        self,
        collateral_address: str,
        strategy_id: Union[bytes, str],
        amount: int,
        tx_params: Optional[TxParams] = None,
    ) -> dict:
        """Construct calldata to be used as input to the method."""
        (
            collateral_address,
            strategy_id,
            amount,
        ) = self.validate_and_normalize_inputs(collateral_address, strategy_id, amount)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(
            collateral_address, strategy_id, amount
        ).buildTransaction(tx_params.as_dict())

    def estimate_gas(
        self,
        collateral_address: str,
        strategy_id: Union[bytes, str],
        amount: int,
        tx_params: Optional[TxParams] = None,
    ) -> int:
        """Estimate gas consumption of method call."""
        (
            collateral_address,
            strategy_id,
            amount,
        ) = self.validate_and_normalize_inputs(collateral_address, strategy_id, amount)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(
            collateral_address, strategy_id, amount
        ).estimateGas(tx_params.as_dict())


class DepositDdxMethod(ContractMethod):  # pylint: disable=invalid-name
    """Various interfaces to the depositDDX method."""

    def __init__(
        self,
        web3_or_provider: Union[Web3, BaseProvider],
        contract_address: str,
        contract_function: ContractFunction,
        validator: Validator = None,
    ):
        """Persist instance data."""
        super().__init__(web3_or_provider, contract_address, validator)
        self._underlying_method = contract_function

    def validate_and_normalize_inputs(self, amount: int):
        """Validate the inputs to the depositDDX method."""
        self.validator.assert_valid(
            method_name="depositDDX",
            parameter_name="_amount",
            argument_value=amount,
        )
        return amount

    def call(self, amount: int, tx_params: Optional[TxParams] = None) -> None:
        """Execute underlying contract method via eth_call.

        :param _amount: The amount to deposit.
        :param tx_params: transaction parameters
        :returns: the return value of the underlying method.
        """
        (amount) = self.validate_and_normalize_inputs(amount)
        tx_params = super().normalize_tx_params(tx_params)
        self._underlying_method(amount).call(tx_params.as_dict())

    def send_transaction(
        self, amount: int, tx_params: Optional[TxParams] = None
    ) -> Union[HexBytes, bytes]:
        """Execute underlying contract method via eth_sendTransaction.

        :param _amount: The amount to deposit.
        :param tx_params: transaction parameters
        """
        (amount) = self.validate_and_normalize_inputs(amount)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(amount).transact(tx_params.as_dict())

    def build_transaction(
        self, amount: int, tx_params: Optional[TxParams] = None
    ) -> dict:
        """Construct calldata to be used as input to the method."""
        (amount) = self.validate_and_normalize_inputs(amount)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(amount).buildTransaction(tx_params.as_dict())

    def estimate_gas(self, amount: int, tx_params: Optional[TxParams] = None) -> int:
        """Estimate gas consumption of method call."""
        (amount) = self.validate_and_normalize_inputs(amount)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(amount).estimateGas(tx_params.as_dict())


class GetExchangeCollateralInfoMethod(ContractMethod):  # pylint: disable=invalid-name
    """Various interfaces to the getExchangeCollateralInfo method."""

    def __init__(
        self,
        web3_or_provider: Union[Web3, BaseProvider],
        contract_address: str,
        contract_function: ContractFunction,
        validator: Validator = None,
    ):
        """Persist instance data."""
        super().__init__(web3_or_provider, contract_address, validator)
        self._underlying_method = contract_function

    def validate_and_normalize_inputs(self, collateral_token: str):
        """Validate the inputs to the getExchangeCollateralInfo method."""
        self.validator.assert_valid(
            method_name="getExchangeCollateralInfo",
            parameter_name="_collateralToken",
            argument_value=collateral_token,
        )
        collateral_token = self.validate_and_checksum_address(collateral_token)
        return collateral_token

    def call(
        self, collateral_token: str, tx_params: Optional[TxParams] = None
    ) -> TraderDefsExchangeCollateral:
        """Execute underlying contract method via eth_call.

        :param _collateralToken: The collateral token of the collateral to
            query.
        :param tx_params: transaction parameters
        :returns: the return value of the underlying method.
        """
        (collateral_token) = self.validate_and_normalize_inputs(collateral_token)
        tx_params = super().normalize_tx_params(tx_params)
        returned = self._underlying_method(collateral_token).call(tx_params.as_dict())
        return TraderDefsExchangeCollateral(
            underlyingToken=returned[0],
            flavor=returned[1],
            isListed=returned[2],
        )

    def send_transaction(
        self, collateral_token: str, tx_params: Optional[TxParams] = None
    ) -> Union[HexBytes, bytes]:
        """Execute underlying contract method via eth_sendTransaction.

        :param _collateralToken: The collateral token of the collateral to
            query.
        :param tx_params: transaction parameters
        """
        (collateral_token) = self.validate_and_normalize_inputs(collateral_token)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(collateral_token).transact(tx_params.as_dict())

    def build_transaction(
        self, collateral_token: str, tx_params: Optional[TxParams] = None
    ) -> dict:
        """Construct calldata to be used as input to the method."""
        (collateral_token) = self.validate_and_normalize_inputs(collateral_token)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(collateral_token).buildTransaction(
            tx_params.as_dict()
        )

    def estimate_gas(
        self, collateral_token: str, tx_params: Optional[TxParams] = None
    ) -> int:
        """Estimate gas consumption of method call."""
        (collateral_token) = self.validate_and_normalize_inputs(collateral_token)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(collateral_token).estimateGas(
            tx_params.as_dict()
        )


class GetMaxDdxCapMethod(ContractMethod):  # pylint: disable=invalid-name
    """Various interfaces to the getMaxDDXCap method."""

    def __init__(
        self,
        web3_or_provider: Union[Web3, BaseProvider],
        contract_address: str,
        contract_function: ContractFunction,
    ):
        """Persist instance data."""
        super().__init__(web3_or_provider, contract_address)
        self._underlying_method = contract_function

    def call(self, tx_params: Optional[TxParams] = None) -> int:
        """Execute underlying contract method via eth_call.

        :param tx_params: transaction parameters
        :returns: the return value of the underlying method.
        """
        tx_params = super().normalize_tx_params(tx_params)
        returned = self._underlying_method().call(tx_params.as_dict())
        return int(returned)

    def send_transaction(
        self, tx_params: Optional[TxParams] = None
    ) -> Union[HexBytes, bytes]:
        """Execute underlying contract method via eth_sendTransaction.

        :param tx_params: transaction parameters
        """
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method().transact(tx_params.as_dict())

    def build_transaction(self, tx_params: Optional[TxParams] = None) -> dict:
        """Construct calldata to be used as input to the method."""
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method().buildTransaction(tx_params.as_dict())

    def estimate_gas(self, tx_params: Optional[TxParams] = None) -> int:
        """Estimate gas consumption of method call."""
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method().estimateGas(tx_params.as_dict())


class GetTraderMethod(ContractMethod):  # pylint: disable=invalid-name
    """Various interfaces to the getTrader method."""

    def __init__(
        self,
        web3_or_provider: Union[Web3, BaseProvider],
        contract_address: str,
        contract_function: ContractFunction,
        validator: Validator = None,
    ):
        """Persist instance data."""
        super().__init__(web3_or_provider, contract_address, validator)
        self._underlying_method = contract_function

    def validate_and_normalize_inputs(self, trader: str):
        """Validate the inputs to the getTrader method."""
        self.validator.assert_valid(
            method_name="getTrader",
            parameter_name="_trader",
            argument_value=trader,
        )
        trader = self.validate_and_checksum_address(trader)
        return trader

    def call(
        self, trader: str, tx_params: Optional[TxParams] = None
    ) -> TraderDefsTrader:
        """Execute underlying contract method via eth_call.

        :param _trader: Trader address.
        :param tx_params: transaction parameters
        :returns: the return value of the underlying method.
        """
        (trader) = self.validate_and_normalize_inputs(trader)
        tx_params = super().normalize_tx_params(tx_params)
        returned = self._underlying_method(trader).call(tx_params.as_dict())
        return TraderDefsTrader(
            ddxBalance=returned[0],
            ddxWalletContract=returned[1],
        )

    def send_transaction(
        self, trader: str, tx_params: Optional[TxParams] = None
    ) -> Union[HexBytes, bytes]:
        """Execute underlying contract method via eth_sendTransaction.

        :param _trader: Trader address.
        :param tx_params: transaction parameters
        """
        (trader) = self.validate_and_normalize_inputs(trader)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(trader).transact(tx_params.as_dict())

    def build_transaction(
        self, trader: str, tx_params: Optional[TxParams] = None
    ) -> dict:
        """Construct calldata to be used as input to the method."""
        (trader) = self.validate_and_normalize_inputs(trader)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(trader).buildTransaction(tx_params.as_dict())

    def estimate_gas(self, trader: str, tx_params: Optional[TxParams] = None) -> int:
        """Estimate gas consumption of method call."""
        (trader) = self.validate_and_normalize_inputs(trader)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(trader).estimateGas(tx_params.as_dict())


class GetWithdrawalsInEpochMethod(ContractMethod):  # pylint: disable=invalid-name
    """Various interfaces to the getWithdrawalsInEpoch method."""

    def __init__(
        self,
        web3_or_provider: Union[Web3, BaseProvider],
        contract_address: str,
        contract_function: ContractFunction,
        validator: Validator = None,
    ):
        """Persist instance data."""
        super().__init__(web3_or_provider, contract_address, validator)
        self._underlying_method = contract_function

    def validate_and_normalize_inputs(
        self, trader_address: str, epoch_id: int, collateral_address: str
    ):
        """Validate the inputs to the getWithdrawalsInEpoch method."""
        self.validator.assert_valid(
            method_name="getWithdrawalsInEpoch",
            parameter_name="_traderAddress",
            argument_value=trader_address,
        )
        trader_address = self.validate_and_checksum_address(trader_address)
        self.validator.assert_valid(
            method_name="getWithdrawalsInEpoch",
            parameter_name="_epochId",
            argument_value=epoch_id,
        )
        # safeguard against fractional inputs
        epoch_id = int(epoch_id)
        self.validator.assert_valid(
            method_name="getWithdrawalsInEpoch",
            parameter_name="_collateralAddress",
            argument_value=collateral_address,
        )
        collateral_address = self.validate_and_checksum_address(collateral_address)
        return (trader_address, epoch_id, collateral_address)

    def call(
        self,
        trader_address: str,
        epoch_id: int,
        collateral_address: str,
        tx_params: Optional[TxParams] = None,
    ) -> int:
        """Execute underlying contract method via eth_call.

        :param _collateralAddress: The address of the collateral that was
            withdrawn.
        :param _epochId: The epoch id to look up.
        :param _traderAddress: The address to look up.
        :param tx_params: transaction parameters
        :returns: the return value of the underlying method.
        """
        (
            trader_address,
            epoch_id,
            collateral_address,
        ) = self.validate_and_normalize_inputs(
            trader_address, epoch_id, collateral_address
        )
        tx_params = super().normalize_tx_params(tx_params)
        returned = self._underlying_method(
            trader_address, epoch_id, collateral_address
        ).call(tx_params.as_dict())
        return int(returned)

    def send_transaction(
        self,
        trader_address: str,
        epoch_id: int,
        collateral_address: str,
        tx_params: Optional[TxParams] = None,
    ) -> Union[HexBytes, bytes]:
        """Execute underlying contract method via eth_sendTransaction.

        :param _collateralAddress: The address of the collateral that was
            withdrawn.
        :param _epochId: The epoch id to look up.
        :param _traderAddress: The address to look up.
        :param tx_params: transaction parameters
        """
        (
            trader_address,
            epoch_id,
            collateral_address,
        ) = self.validate_and_normalize_inputs(
            trader_address, epoch_id, collateral_address
        )
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(
            trader_address, epoch_id, collateral_address
        ).transact(tx_params.as_dict())

    def build_transaction(
        self,
        trader_address: str,
        epoch_id: int,
        collateral_address: str,
        tx_params: Optional[TxParams] = None,
    ) -> dict:
        """Construct calldata to be used as input to the method."""
        (
            trader_address,
            epoch_id,
            collateral_address,
        ) = self.validate_and_normalize_inputs(
            trader_address, epoch_id, collateral_address
        )
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(
            trader_address, epoch_id, collateral_address
        ).buildTransaction(tx_params.as_dict())

    def estimate_gas(
        self,
        trader_address: str,
        epoch_id: int,
        collateral_address: str,
        tx_params: Optional[TxParams] = None,
    ) -> int:
        """Estimate gas consumption of method call."""
        (
            trader_address,
            epoch_id,
            collateral_address,
        ) = self.validate_and_normalize_inputs(
            trader_address, epoch_id, collateral_address
        )
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(
            trader_address, epoch_id, collateral_address
        ).estimateGas(tx_params.as_dict())


class InitializeMethod(ContractMethod):  # pylint: disable=invalid-name
    """Various interfaces to the initialize method."""

    def __init__(
        self,
        web3_or_provider: Union[Web3, BaseProvider],
        contract_address: str,
        contract_function: ContractFunction,
        validator: Validator = None,
    ):
        """Persist instance data."""
        super().__init__(web3_or_provider, contract_address, validator)
        self._underlying_method = contract_function

    def validate_and_normalize_inputs(self, ddx_wallet_cloneable: str):
        """Validate the inputs to the initialize method."""
        self.validator.assert_valid(
            method_name="initialize",
            parameter_name="_ddxWalletCloneable",
            argument_value=ddx_wallet_cloneable,
        )
        ddx_wallet_cloneable = self.validate_and_checksum_address(ddx_wallet_cloneable)
        return ddx_wallet_cloneable

    def call(
        self, ddx_wallet_cloneable: str, tx_params: Optional[TxParams] = None
    ) -> None:
        """Execute underlying contract method via eth_call.

        This function is best called as a parameter to the diamond cut
        function. This is removed prior to the selectors being added to the
        diamond, meaning it cannot be called again.

        :param _ddxWalletCloneable: The address of the on-chain cloneable DDX
            wallet.
        :param tx_params: transaction parameters
        :returns: the return value of the underlying method.
        """
        (ddx_wallet_cloneable) = self.validate_and_normalize_inputs(
            ddx_wallet_cloneable
        )
        tx_params = super().normalize_tx_params(tx_params)
        self._underlying_method(ddx_wallet_cloneable).call(tx_params.as_dict())

    def send_transaction(
        self, ddx_wallet_cloneable: str, tx_params: Optional[TxParams] = None
    ) -> Union[HexBytes, bytes]:
        """Execute underlying contract method via eth_sendTransaction.

        This function is best called as a parameter to the diamond cut
        function. This is removed prior to the selectors being added to the
        diamond, meaning it cannot be called again.

        :param _ddxWalletCloneable: The address of the on-chain cloneable DDX
            wallet.
        :param tx_params: transaction parameters
        """
        (ddx_wallet_cloneable) = self.validate_and_normalize_inputs(
            ddx_wallet_cloneable
        )
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(ddx_wallet_cloneable).transact(
            tx_params.as_dict()
        )

    def build_transaction(
        self, ddx_wallet_cloneable: str, tx_params: Optional[TxParams] = None
    ) -> dict:
        """Construct calldata to be used as input to the method."""
        (ddx_wallet_cloneable) = self.validate_and_normalize_inputs(
            ddx_wallet_cloneable
        )
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(ddx_wallet_cloneable).buildTransaction(
            tx_params.as_dict()
        )

    def estimate_gas(
        self, ddx_wallet_cloneable: str, tx_params: Optional[TxParams] = None
    ) -> int:
        """Estimate gas consumption of method call."""
        (ddx_wallet_cloneable) = self.validate_and_normalize_inputs(
            ddx_wallet_cloneable
        )
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(ddx_wallet_cloneable).estimateGas(
            tx_params.as_dict()
        )


class IssueDdxRewardMethod(ContractMethod):  # pylint: disable=invalid-name
    """Various interfaces to the issueDDXReward method."""

    def __init__(
        self,
        web3_or_provider: Union[Web3, BaseProvider],
        contract_address: str,
        contract_function: ContractFunction,
        validator: Validator = None,
    ):
        """Persist instance data."""
        super().__init__(web3_or_provider, contract_address, validator)
        self._underlying_method = contract_function

    def validate_and_normalize_inputs(self, amount: int, trader: str):
        """Validate the inputs to the issueDDXReward method."""
        self.validator.assert_valid(
            method_name="issueDDXReward",
            parameter_name="_amount",
            argument_value=amount,
        )
        self.validator.assert_valid(
            method_name="issueDDXReward",
            parameter_name="_trader",
            argument_value=trader,
        )
        trader = self.validate_and_checksum_address(trader)
        return (amount, trader)

    def call(
        self, amount: int, trader: str, tx_params: Optional[TxParams] = None
    ) -> None:
        """Execute underlying contract method via eth_call.

        :param _amount: DDX tokens to be rewarded.
        :param _trader: Trader recipient address.
        :param tx_params: transaction parameters
        :returns: the return value of the underlying method.
        """
        (amount, trader) = self.validate_and_normalize_inputs(amount, trader)
        tx_params = super().normalize_tx_params(tx_params)
        self._underlying_method(amount, trader).call(tx_params.as_dict())

    def send_transaction(
        self, amount: int, trader: str, tx_params: Optional[TxParams] = None
    ) -> Union[HexBytes, bytes]:
        """Execute underlying contract method via eth_sendTransaction.

        :param _amount: DDX tokens to be rewarded.
        :param _trader: Trader recipient address.
        :param tx_params: transaction parameters
        """
        (amount, trader) = self.validate_and_normalize_inputs(amount, trader)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(amount, trader).transact(tx_params.as_dict())

    def build_transaction(
        self, amount: int, trader: str, tx_params: Optional[TxParams] = None
    ) -> dict:
        """Construct calldata to be used as input to the method."""
        (amount, trader) = self.validate_and_normalize_inputs(amount, trader)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(amount, trader).buildTransaction(
            tx_params.as_dict()
        )

    def estimate_gas(
        self, amount: int, trader: str, tx_params: Optional[TxParams] = None
    ) -> int:
        """Estimate gas consumption of method call."""
        (amount, trader) = self.validate_and_normalize_inputs(amount, trader)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(amount, trader).estimateGas(tx_params.as_dict())


class IssueDdxToRecipientMethod(ContractMethod):  # pylint: disable=invalid-name
    """Various interfaces to the issueDDXToRecipient method."""

    def __init__(
        self,
        web3_or_provider: Union[Web3, BaseProvider],
        contract_address: str,
        contract_function: ContractFunction,
        validator: Validator = None,
    ):
        """Persist instance data."""
        super().__init__(web3_or_provider, contract_address, validator)
        self._underlying_method = contract_function

    def validate_and_normalize_inputs(self, amount: int, recipient: str):
        """Validate the inputs to the issueDDXToRecipient method."""
        self.validator.assert_valid(
            method_name="issueDDXToRecipient",
            parameter_name="_amount",
            argument_value=amount,
        )
        self.validator.assert_valid(
            method_name="issueDDXToRecipient",
            parameter_name="_recipient",
            argument_value=recipient,
        )
        recipient = self.validate_and_checksum_address(recipient)
        return (amount, recipient)

    def call(
        self, amount: int, recipient: str, tx_params: Optional[TxParams] = None
    ) -> None:
        """Execute underlying contract method via eth_call.

        :param _amount: DDX tokens to be rewarded.
        :param _recipient: External recipient address.
        :param tx_params: transaction parameters
        :returns: the return value of the underlying method.
        """
        (amount, recipient) = self.validate_and_normalize_inputs(amount, recipient)
        tx_params = super().normalize_tx_params(tx_params)
        self._underlying_method(amount, recipient).call(tx_params.as_dict())

    def send_transaction(
        self, amount: int, recipient: str, tx_params: Optional[TxParams] = None
    ) -> Union[HexBytes, bytes]:
        """Execute underlying contract method via eth_sendTransaction.

        :param _amount: DDX tokens to be rewarded.
        :param _recipient: External recipient address.
        :param tx_params: transaction parameters
        """
        (amount, recipient) = self.validate_and_normalize_inputs(amount, recipient)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(amount, recipient).transact(tx_params.as_dict())

    def build_transaction(
        self, amount: int, recipient: str, tx_params: Optional[TxParams] = None
    ) -> dict:
        """Construct calldata to be used as input to the method."""
        (amount, recipient) = self.validate_and_normalize_inputs(amount, recipient)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(amount, recipient).buildTransaction(
            tx_params.as_dict()
        )

    def estimate_gas(
        self, amount: int, recipient: str, tx_params: Optional[TxParams] = None
    ) -> int:
        """Estimate gas consumption of method call."""
        (amount, recipient) = self.validate_and_normalize_inputs(amount, recipient)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(amount, recipient).estimateGas(
            tx_params.as_dict()
        )


class RemoveExchangeCollateralMethod(ContractMethod):  # pylint: disable=invalid-name
    """Various interfaces to the removeExchangeCollateral method."""

    def __init__(
        self,
        web3_or_provider: Union[Web3, BaseProvider],
        contract_address: str,
        contract_function: ContractFunction,
        validator: Validator = None,
    ):
        """Persist instance data."""
        super().__init__(web3_or_provider, contract_address, validator)
        self._underlying_method = contract_function

    def validate_and_normalize_inputs(self, collateral_token: str):
        """Validate the inputs to the removeExchangeCollateral method."""
        self.validator.assert_valid(
            method_name="removeExchangeCollateral",
            parameter_name="_collateralToken",
            argument_value=collateral_token,
        )
        collateral_token = self.validate_and_checksum_address(collateral_token)
        return collateral_token

    def call(self, collateral_token: str, tx_params: Optional[TxParams] = None) -> None:
        """Execute underlying contract method via eth_call.

        :param _collateralToken: The collateral token to remove.
        :param tx_params: transaction parameters
        :returns: the return value of the underlying method.
        """
        (collateral_token) = self.validate_and_normalize_inputs(collateral_token)
        tx_params = super().normalize_tx_params(tx_params)
        self._underlying_method(collateral_token).call(tx_params.as_dict())

    def send_transaction(
        self, collateral_token: str, tx_params: Optional[TxParams] = None
    ) -> Union[HexBytes, bytes]:
        """Execute underlying contract method via eth_sendTransaction.

        :param _collateralToken: The collateral token to remove.
        :param tx_params: transaction parameters
        """
        (collateral_token) = self.validate_and_normalize_inputs(collateral_token)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(collateral_token).transact(tx_params.as_dict())

    def build_transaction(
        self, collateral_token: str, tx_params: Optional[TxParams] = None
    ) -> dict:
        """Construct calldata to be used as input to the method."""
        (collateral_token) = self.validate_and_normalize_inputs(collateral_token)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(collateral_token).buildTransaction(
            tx_params.as_dict()
        )

    def estimate_gas(
        self, collateral_token: str, tx_params: Optional[TxParams] = None
    ) -> int:
        """Estimate gas consumption of method call."""
        (collateral_token) = self.validate_and_normalize_inputs(collateral_token)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(collateral_token).estimateGas(
            tx_params.as_dict()
        )


class SendDdxFromTraderToTraderWalletMethod(
    ContractMethod
):  # pylint: disable=invalid-name
    """Various interfaces to the sendDDXFromTraderToTraderWallet method."""

    def __init__(
        self,
        web3_or_provider: Union[Web3, BaseProvider],
        contract_address: str,
        contract_function: ContractFunction,
        validator: Validator = None,
    ):
        """Persist instance data."""
        super().__init__(web3_or_provider, contract_address, validator)
        self._underlying_method = contract_function

    def validate_and_normalize_inputs(self, trader: str, amount: int):
        """Validate the inputs to the sendDDXFromTraderToTraderWallet method."""
        self.validator.assert_valid(
            method_name="sendDDXFromTraderToTraderWallet",
            parameter_name="_trader",
            argument_value=trader,
        )
        trader = self.validate_and_checksum_address(trader)
        self.validator.assert_valid(
            method_name="sendDDXFromTraderToTraderWallet",
            parameter_name="_amount",
            argument_value=amount,
        )
        return (trader, amount)

    def call(
        self, trader: str, amount: int, tx_params: Optional[TxParams] = None
    ) -> None:
        """Execute underlying contract method via eth_call.

        :param _amount: The DDX tokens to be staked.
        :param _trader: Trader address to receive DDX (inside their
            wallet, which will be created if it does not already        exist).
        :param tx_params: transaction parameters
        :returns: the return value of the underlying method.
        """
        (trader, amount) = self.validate_and_normalize_inputs(trader, amount)
        tx_params = super().normalize_tx_params(tx_params)
        self._underlying_method(trader, amount).call(tx_params.as_dict())

    def send_transaction(
        self, trader: str, amount: int, tx_params: Optional[TxParams] = None
    ) -> Union[HexBytes, bytes]:
        """Execute underlying contract method via eth_sendTransaction.

        :param _amount: The DDX tokens to be staked.
        :param _trader: Trader address to receive DDX (inside their
            wallet, which will be created if it does not already        exist).
        :param tx_params: transaction parameters
        """
        (trader, amount) = self.validate_and_normalize_inputs(trader, amount)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(trader, amount).transact(tx_params.as_dict())

    def build_transaction(
        self, trader: str, amount: int, tx_params: Optional[TxParams] = None
    ) -> dict:
        """Construct calldata to be used as input to the method."""
        (trader, amount) = self.validate_and_normalize_inputs(trader, amount)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(trader, amount).buildTransaction(
            tx_params.as_dict()
        )

    def estimate_gas(
        self, trader: str, amount: int, tx_params: Optional[TxParams] = None
    ) -> int:
        """Estimate gas consumption of method call."""
        (trader, amount) = self.validate_and_normalize_inputs(trader, amount)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(trader, amount).estimateGas(tx_params.as_dict())


class SetMaxDdxCapMethod(ContractMethod):  # pylint: disable=invalid-name
    """Various interfaces to the setMaxDDXCap method."""

    def __init__(
        self,
        web3_or_provider: Union[Web3, BaseProvider],
        contract_address: str,
        contract_function: ContractFunction,
        validator: Validator = None,
    ):
        """Persist instance data."""
        super().__init__(web3_or_provider, contract_address, validator)
        self._underlying_method = contract_function

    def validate_and_normalize_inputs(self, max_ddx_cap: int):
        """Validate the inputs to the setMaxDDXCap method."""
        self.validator.assert_valid(
            method_name="setMaxDDXCap",
            parameter_name="_maxDDXCap",
            argument_value=max_ddx_cap,
        )
        # safeguard against fractional inputs
        max_ddx_cap = int(max_ddx_cap)
        return max_ddx_cap

    def call(self, max_ddx_cap: int, tx_params: Optional[TxParams] = None) -> None:
        """Execute underlying contract method via eth_call.

        We intentionally avoid checking to see that the max DDX cap is greater
        than the current DDX balance as this check could be front-run in order
        to keep the cap at the elevated level. Instead, we simply set the cap
        to the chosen value which prevents further deposits from taking place.
        The operation is still susceptible to front-running, but the ability to
        keep the cap arbitrarily elevated is removed.

        :param _maxDDXCap: The maximum amount of DDX that can be held within
            the        DerivaDEX SMT.
        :param tx_params: transaction parameters
        :returns: the return value of the underlying method.
        """
        (max_ddx_cap) = self.validate_and_normalize_inputs(max_ddx_cap)
        tx_params = super().normalize_tx_params(tx_params)
        self._underlying_method(max_ddx_cap).call(tx_params.as_dict())

    def send_transaction(
        self, max_ddx_cap: int, tx_params: Optional[TxParams] = None
    ) -> Union[HexBytes, bytes]:
        """Execute underlying contract method via eth_sendTransaction.

        We intentionally avoid checking to see that the max DDX cap is greater
        than the current DDX balance as this check could be front-run in order
        to keep the cap at the elevated level. Instead, we simply set the cap
        to the chosen value which prevents further deposits from taking place.
        The operation is still susceptible to front-running, but the ability to
        keep the cap arbitrarily elevated is removed.

        :param _maxDDXCap: The maximum amount of DDX that can be held within
            the        DerivaDEX SMT.
        :param tx_params: transaction parameters
        """
        (max_ddx_cap) = self.validate_and_normalize_inputs(max_ddx_cap)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(max_ddx_cap).transact(tx_params.as_dict())

    def build_transaction(
        self, max_ddx_cap: int, tx_params: Optional[TxParams] = None
    ) -> dict:
        """Construct calldata to be used as input to the method."""
        (max_ddx_cap) = self.validate_and_normalize_inputs(max_ddx_cap)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(max_ddx_cap).buildTransaction(
            tx_params.as_dict()
        )

    def estimate_gas(
        self, max_ddx_cap: int, tx_params: Optional[TxParams] = None
    ) -> int:
        """Estimate gas consumption of method call."""
        (max_ddx_cap) = self.validate_and_normalize_inputs(max_ddx_cap)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(max_ddx_cap).estimateGas(tx_params.as_dict())


class SetRewardCliffMethod(ContractMethod):  # pylint: disable=invalid-name
    """Various interfaces to the setRewardCliff method."""

    def __init__(
        self,
        web3_or_provider: Union[Web3, BaseProvider],
        contract_address: str,
        contract_function: ContractFunction,
        validator: Validator = None,
    ):
        """Persist instance data."""
        super().__init__(web3_or_provider, contract_address, validator)
        self._underlying_method = contract_function

    def validate_and_normalize_inputs(self, reward_cliff: bool):
        """Validate the inputs to the setRewardCliff method."""
        self.validator.assert_valid(
            method_name="setRewardCliff",
            parameter_name="_rewardCliff",
            argument_value=reward_cliff,
        )
        return reward_cliff

    def call(self, reward_cliff: bool, tx_params: Optional[TxParams] = None) -> None:
        """Execute underlying contract method via eth_call.

        :param _rewardCliff: Reward cliff.
        :param tx_params: transaction parameters
        :returns: the return value of the underlying method.
        """
        (reward_cliff) = self.validate_and_normalize_inputs(reward_cliff)
        tx_params = super().normalize_tx_params(tx_params)
        self._underlying_method(reward_cliff).call(tx_params.as_dict())

    def send_transaction(
        self, reward_cliff: bool, tx_params: Optional[TxParams] = None
    ) -> Union[HexBytes, bytes]:
        """Execute underlying contract method via eth_sendTransaction.

        :param _rewardCliff: Reward cliff.
        :param tx_params: transaction parameters
        """
        (reward_cliff) = self.validate_and_normalize_inputs(reward_cliff)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(reward_cliff).transact(tx_params.as_dict())

    def build_transaction(
        self, reward_cliff: bool, tx_params: Optional[TxParams] = None
    ) -> dict:
        """Construct calldata to be used as input to the method."""
        (reward_cliff) = self.validate_and_normalize_inputs(reward_cliff)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(reward_cliff).buildTransaction(
            tx_params.as_dict()
        )

    def estimate_gas(
        self, reward_cliff: bool, tx_params: Optional[TxParams] = None
    ) -> int:
        """Estimate gas consumption of method call."""
        (reward_cliff) = self.validate_and_normalize_inputs(reward_cliff)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(reward_cliff).estimateGas(tx_params.as_dict())


class StakeDdxFromTraderMethod(ContractMethod):  # pylint: disable=invalid-name
    """Various interfaces to the stakeDDXFromTrader method."""

    def __init__(
        self,
        web3_or_provider: Union[Web3, BaseProvider],
        contract_address: str,
        contract_function: ContractFunction,
        validator: Validator = None,
    ):
        """Persist instance data."""
        super().__init__(web3_or_provider, contract_address, validator)
        self._underlying_method = contract_function

    def validate_and_normalize_inputs(self, amount: int):
        """Validate the inputs to the stakeDDXFromTrader method."""
        self.validator.assert_valid(
            method_name="stakeDDXFromTrader",
            parameter_name="_amount",
            argument_value=amount,
        )
        return amount

    def call(self, amount: int, tx_params: Optional[TxParams] = None) -> None:
        """Execute underlying contract method via eth_call.

        :param _amount: The DDX tokens to be staked.
        :param tx_params: transaction parameters
        :returns: the return value of the underlying method.
        """
        (amount) = self.validate_and_normalize_inputs(amount)
        tx_params = super().normalize_tx_params(tx_params)
        self._underlying_method(amount).call(tx_params.as_dict())

    def send_transaction(
        self, amount: int, tx_params: Optional[TxParams] = None
    ) -> Union[HexBytes, bytes]:
        """Execute underlying contract method via eth_sendTransaction.

        :param _amount: The DDX tokens to be staked.
        :param tx_params: transaction parameters
        """
        (amount) = self.validate_and_normalize_inputs(amount)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(amount).transact(tx_params.as_dict())

    def build_transaction(
        self, amount: int, tx_params: Optional[TxParams] = None
    ) -> dict:
        """Construct calldata to be used as input to the method."""
        (amount) = self.validate_and_normalize_inputs(amount)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(amount).buildTransaction(tx_params.as_dict())

    def estimate_gas(self, amount: int, tx_params: Optional[TxParams] = None) -> int:
        """Estimate gas consumption of method call."""
        (amount) = self.validate_and_normalize_inputs(amount)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(amount).estimateGas(tx_params.as_dict())


class WithdrawMethod(ContractMethod):  # pylint: disable=invalid-name
    """Various interfaces to the withdraw method."""

    def __init__(
        self,
        web3_or_provider: Union[Web3, BaseProvider],
        contract_address: str,
        contract_function: ContractFunction,
        validator: Validator = None,
    ):
        """Persist instance data."""
        super().__init__(web3_or_provider, contract_address, validator)
        self._underlying_method = contract_function

    def validate_and_normalize_inputs(
        self,
        strategy_id: Union[bytes, str],
        withdrawal_data: SharedDefsWithdrawalData,
        strategy: TraderDefsStrategyData,
        proof: Union[bytes, str],
    ):
        """Validate the inputs to the withdraw method."""
        self.validator.assert_valid(
            method_name="withdraw",
            parameter_name="_strategyId",
            argument_value=strategy_id,
        )
        self.validator.assert_valid(
            method_name="withdraw",
            parameter_name="_withdrawalData",
            argument_value=withdrawal_data,
        )
        self.validator.assert_valid(
            method_name="withdraw",
            parameter_name="_strategy",
            argument_value=strategy,
        )
        self.validator.assert_valid(
            method_name="withdraw",
            parameter_name="_proof",
            argument_value=proof,
        )
        return (strategy_id, withdrawal_data, strategy, proof)

    def call(
        self,
        strategy_id: Union[bytes, str],
        withdrawal_data: SharedDefsWithdrawalData,
        strategy: TraderDefsStrategyData,
        proof: Union[bytes, str],
        tx_params: Optional[TxParams] = None,
    ) -> None:
        """Execute underlying contract method via eth_call.

        :param _proof: A merkle proof that proves that the included strategy is
            in        the most recent state root.
        :param _strategy: The data that is contained within the strategy.
        :param _strategyId: The id of the strategy to withdraw from.
        :param _withdrawalData: Data that specifies the tokens and amounts to
                 withdraw. The withdraw data that is provided to this function
            must        be given in the same order as the
            `_strategy.frozenCollateral`        field. Misordering this
            parameter will cause the function to revert.
        :param tx_params: transaction parameters
        :returns: the return value of the underlying method.
        """
        (
            strategy_id,
            withdrawal_data,
            strategy,
            proof,
        ) = self.validate_and_normalize_inputs(
            strategy_id, withdrawal_data, strategy, proof
        )
        tx_params = super().normalize_tx_params(tx_params)
        self._underlying_method(strategy_id, withdrawal_data, strategy, proof).call(
            tx_params.as_dict()
        )

    def send_transaction(
        self,
        strategy_id: Union[bytes, str],
        withdrawal_data: SharedDefsWithdrawalData,
        strategy: TraderDefsStrategyData,
        proof: Union[bytes, str],
        tx_params: Optional[TxParams] = None,
    ) -> Union[HexBytes, bytes]:
        """Execute underlying contract method via eth_sendTransaction.

        :param _proof: A merkle proof that proves that the included strategy is
            in        the most recent state root.
        :param _strategy: The data that is contained within the strategy.
        :param _strategyId: The id of the strategy to withdraw from.
        :param _withdrawalData: Data that specifies the tokens and amounts to
                 withdraw. The withdraw data that is provided to this function
            must        be given in the same order as the
            `_strategy.frozenCollateral`        field. Misordering this
            parameter will cause the function to revert.
        :param tx_params: transaction parameters
        """
        (
            strategy_id,
            withdrawal_data,
            strategy,
            proof,
        ) = self.validate_and_normalize_inputs(
            strategy_id, withdrawal_data, strategy, proof
        )
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(
            strategy_id, withdrawal_data, strategy, proof
        ).transact(tx_params.as_dict())

    def build_transaction(
        self,
        strategy_id: Union[bytes, str],
        withdrawal_data: SharedDefsWithdrawalData,
        strategy: TraderDefsStrategyData,
        proof: Union[bytes, str],
        tx_params: Optional[TxParams] = None,
    ) -> dict:
        """Construct calldata to be used as input to the method."""
        (
            strategy_id,
            withdrawal_data,
            strategy,
            proof,
        ) = self.validate_and_normalize_inputs(
            strategy_id, withdrawal_data, strategy, proof
        )
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(
            strategy_id, withdrawal_data, strategy, proof
        ).buildTransaction(tx_params.as_dict())

    def estimate_gas(
        self,
        strategy_id: Union[bytes, str],
        withdrawal_data: SharedDefsWithdrawalData,
        strategy: TraderDefsStrategyData,
        proof: Union[bytes, str],
        tx_params: Optional[TxParams] = None,
    ) -> int:
        """Estimate gas consumption of method call."""
        (
            strategy_id,
            withdrawal_data,
            strategy,
            proof,
        ) = self.validate_and_normalize_inputs(
            strategy_id, withdrawal_data, strategy, proof
        )
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(
            strategy_id, withdrawal_data, strategy, proof
        ).estimateGas(tx_params.as_dict())


class WithdrawDdxMethod(ContractMethod):  # pylint: disable=invalid-name
    """Various interfaces to the withdrawDDX method."""

    def __init__(
        self,
        web3_or_provider: Union[Web3, BaseProvider],
        contract_address: str,
        contract_function: ContractFunction,
        validator: Validator = None,
    ):
        """Persist instance data."""
        super().__init__(web3_or_provider, contract_address, validator)
        self._underlying_method = contract_function

    def validate_and_normalize_inputs(
        self,
        amount: int,
        trader: TraderDefsTraderData,
        proof: Union[bytes, str],
    ):
        """Validate the inputs to the withdrawDDX method."""
        self.validator.assert_valid(
            method_name="withdrawDDX",
            parameter_name="_amount",
            argument_value=amount,
        )
        self.validator.assert_valid(
            method_name="withdrawDDX",
            parameter_name="_trader",
            argument_value=trader,
        )
        self.validator.assert_valid(
            method_name="withdrawDDX",
            parameter_name="_proof",
            argument_value=proof,
        )
        return (amount, trader, proof)

    def call(
        self,
        amount: int,
        trader: TraderDefsTraderData,
        proof: Union[bytes, str],
        tx_params: Optional[TxParams] = None,
    ) -> None:
        """Execute underlying contract method via eth_call.

        :param _amount: The amount of DDX to withdraw.
        :param _proof: The merkle proof that will be used to verify whether or
            not        the trader can withdraw the requested amount.
        :param _trader: The Trader leaf that should represent the sender's
            trader        account within the DDX state root.
        :param tx_params: transaction parameters
        :returns: the return value of the underlying method.
        """
        (amount, trader, proof) = self.validate_and_normalize_inputs(
            amount, trader, proof
        )
        tx_params = super().normalize_tx_params(tx_params)
        self._underlying_method(amount, trader, proof).call(tx_params.as_dict())

    def send_transaction(
        self,
        amount: int,
        trader: TraderDefsTraderData,
        proof: Union[bytes, str],
        tx_params: Optional[TxParams] = None,
    ) -> Union[HexBytes, bytes]:
        """Execute underlying contract method via eth_sendTransaction.

        :param _amount: The amount of DDX to withdraw.
        :param _proof: The merkle proof that will be used to verify whether or
            not        the trader can withdraw the requested amount.
        :param _trader: The Trader leaf that should represent the sender's
            trader        account within the DDX state root.
        :param tx_params: transaction parameters
        """
        (amount, trader, proof) = self.validate_and_normalize_inputs(
            amount, trader, proof
        )
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(amount, trader, proof).transact(
            tx_params.as_dict()
        )

    def build_transaction(
        self,
        amount: int,
        trader: TraderDefsTraderData,
        proof: Union[bytes, str],
        tx_params: Optional[TxParams] = None,
    ) -> dict:
        """Construct calldata to be used as input to the method."""
        (amount, trader, proof) = self.validate_and_normalize_inputs(
            amount, trader, proof
        )
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(amount, trader, proof).buildTransaction(
            tx_params.as_dict()
        )

    def estimate_gas(
        self,
        amount: int,
        trader: TraderDefsTraderData,
        proof: Union[bytes, str],
        tx_params: Optional[TxParams] = None,
    ) -> int:
        """Estimate gas consumption of method call."""
        (amount, trader, proof) = self.validate_and_normalize_inputs(
            amount, trader, proof
        )
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(amount, trader, proof).estimateGas(
            tx_params.as_dict()
        )


class WithdrawDdxToTraderMethod(ContractMethod):  # pylint: disable=invalid-name
    """Various interfaces to the withdrawDDXToTrader method."""

    def __init__(
        self,
        web3_or_provider: Union[Web3, BaseProvider],
        contract_address: str,
        contract_function: ContractFunction,
        validator: Validator = None,
    ):
        """Persist instance data."""
        super().__init__(web3_or_provider, contract_address, validator)
        self._underlying_method = contract_function

    def validate_and_normalize_inputs(self, amount: int):
        """Validate the inputs to the withdrawDDXToTrader method."""
        self.validator.assert_valid(
            method_name="withdrawDDXToTrader",
            parameter_name="_amount",
            argument_value=amount,
        )
        return amount

    def call(self, amount: int, tx_params: Optional[TxParams] = None) -> None:
        """Execute underlying contract method via eth_call.

        :param _amount: The DDX tokens to be withdrawn.
        :param tx_params: transaction parameters
        :returns: the return value of the underlying method.
        """
        (amount) = self.validate_and_normalize_inputs(amount)
        tx_params = super().normalize_tx_params(tx_params)
        self._underlying_method(amount).call(tx_params.as_dict())

    def send_transaction(
        self, amount: int, tx_params: Optional[TxParams] = None
    ) -> Union[HexBytes, bytes]:
        """Execute underlying contract method via eth_sendTransaction.

        :param _amount: The DDX tokens to be withdrawn.
        :param tx_params: transaction parameters
        """
        (amount) = self.validate_and_normalize_inputs(amount)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(amount).transact(tx_params.as_dict())

    def build_transaction(
        self, amount: int, tx_params: Optional[TxParams] = None
    ) -> dict:
        """Construct calldata to be used as input to the method."""
        (amount) = self.validate_and_normalize_inputs(amount)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(amount).buildTransaction(tx_params.as_dict())

    def estimate_gas(self, amount: int, tx_params: Optional[TxParams] = None) -> int:
        """Estimate gas consumption of method call."""
        (amount) = self.validate_and_normalize_inputs(amount)
        tx_params = super().normalize_tx_params(tx_params)
        return self._underlying_method(amount).estimateGas(tx_params.as_dict())


# pylint: disable=too-many-public-methods,too-many-instance-attributes
class Trader:
    """Wrapper class for Trader Solidity contract.

    All method parameters of type `bytes`:code: should be encoded as UTF-8,
    which can be accomplished via `str.encode("utf_8")`:code:.
    """

    add_exchange_collateral: AddExchangeCollateralMethod
    """Constructor-initialized instance of
    :class:`AddExchangeCollateralMethod`.
    """

    deposit: DepositMethod
    """Constructor-initialized instance of
    :class:`DepositMethod`.
    """

    deposit_ddx: DepositDdxMethod
    """Constructor-initialized instance of
    :class:`DepositDdxMethod`.
    """

    get_exchange_collateral_info: GetExchangeCollateralInfoMethod
    """Constructor-initialized instance of
    :class:`GetExchangeCollateralInfoMethod`.
    """

    get_max_ddx_cap: GetMaxDdxCapMethod
    """Constructor-initialized instance of
    :class:`GetMaxDdxCapMethod`.
    """

    get_trader: GetTraderMethod
    """Constructor-initialized instance of
    :class:`GetTraderMethod`.
    """

    get_withdrawals_in_epoch: GetWithdrawalsInEpochMethod
    """Constructor-initialized instance of
    :class:`GetWithdrawalsInEpochMethod`.
    """

    initialize: InitializeMethod
    """Constructor-initialized instance of
    :class:`InitializeMethod`.
    """

    issue_ddx_reward: IssueDdxRewardMethod
    """Constructor-initialized instance of
    :class:`IssueDdxRewardMethod`.
    """

    issue_ddx_to_recipient: IssueDdxToRecipientMethod
    """Constructor-initialized instance of
    :class:`IssueDdxToRecipientMethod`.
    """

    remove_exchange_collateral: RemoveExchangeCollateralMethod
    """Constructor-initialized instance of
    :class:`RemoveExchangeCollateralMethod`.
    """

    send_ddx_from_trader_to_trader_wallet: SendDdxFromTraderToTraderWalletMethod
    """Constructor-initialized instance of
    :class:`SendDdxFromTraderToTraderWalletMethod`.
    """

    set_max_ddx_cap: SetMaxDdxCapMethod
    """Constructor-initialized instance of
    :class:`SetMaxDdxCapMethod`.
    """

    set_reward_cliff: SetRewardCliffMethod
    """Constructor-initialized instance of
    :class:`SetRewardCliffMethod`.
    """

    stake_ddx_from_trader: StakeDdxFromTraderMethod
    """Constructor-initialized instance of
    :class:`StakeDdxFromTraderMethod`.
    """

    withdraw: WithdrawMethod
    """Constructor-initialized instance of
    :class:`WithdrawMethod`.
    """

    withdraw_ddx: WithdrawDdxMethod
    """Constructor-initialized instance of
    :class:`WithdrawDdxMethod`.
    """

    withdraw_ddx_to_trader: WithdrawDdxToTraderMethod
    """Constructor-initialized instance of
    :class:`WithdrawDdxToTraderMethod`.
    """

    def __init__(
        self,
        web3_or_provider: Union[Web3, BaseProvider],
        contract_address: str,
        validator: TraderValidator = None,
    ):
        """Get an instance of wrapper for smart contract.

        :param web3_or_provider: Either an instance of `web3.Web3`:code: or
            `web3.providers.base.BaseProvider`:code:
        :param contract_address: where the contract has been deployed
        :param validator: for validation of method inputs.
        """
        # pylint: disable=too-many-statements

        self.contract_address = contract_address

        if not validator:
            validator = TraderValidator(web3_or_provider, contract_address)

        web3 = None
        if isinstance(web3_or_provider, BaseProvider):
            web3 = Web3(web3_or_provider)
        elif isinstance(web3_or_provider, Web3):
            web3 = web3_or_provider
        else:
            raise TypeError(
                "Expected parameter 'web3_or_provider' to be an instance of either"
                + " Web3 or BaseProvider"
            )

        # if any middleware was imported, inject it
        try:
            MIDDLEWARE
        except NameError:
            pass
        else:
            try:
                for middleware in MIDDLEWARE:
                    web3.middleware_onion.inject(
                        middleware["function"],
                        layer=middleware["layer"],
                    )
            except ValueError as value_error:
                if value_error.args == (
                    "You can't add the same un-named instance twice",
                ):
                    pass

        self._web3_eth = web3.eth

        functions = self._web3_eth.contract(
            address=to_checksum_address(contract_address), abi=Trader.abi()
        ).functions

        self.add_exchange_collateral = AddExchangeCollateralMethod(
            web3_or_provider,
            contract_address,
            functions.addExchangeCollateral,
            validator,
        )

        self.deposit = DepositMethod(
            web3_or_provider, contract_address, functions.deposit, validator
        )

        self.deposit_ddx = DepositDdxMethod(
            web3_or_provider, contract_address, functions.depositDDX, validator
        )

        self.get_exchange_collateral_info = GetExchangeCollateralInfoMethod(
            web3_or_provider,
            contract_address,
            functions.getExchangeCollateralInfo,
            validator,
        )

        self.get_max_ddx_cap = GetMaxDdxCapMethod(
            web3_or_provider, contract_address, functions.getMaxDDXCap
        )

        self.get_trader = GetTraderMethod(
            web3_or_provider, contract_address, functions.getTrader, validator
        )

        self.get_withdrawals_in_epoch = GetWithdrawalsInEpochMethod(
            web3_or_provider,
            contract_address,
            functions.getWithdrawalsInEpoch,
            validator,
        )

        self.initialize = InitializeMethod(
            web3_or_provider, contract_address, functions.initialize, validator
        )

        self.issue_ddx_reward = IssueDdxRewardMethod(
            web3_or_provider,
            contract_address,
            functions.issueDDXReward,
            validator,
        )

        self.issue_ddx_to_recipient = IssueDdxToRecipientMethod(
            web3_or_provider,
            contract_address,
            functions.issueDDXToRecipient,
            validator,
        )

        self.remove_exchange_collateral = RemoveExchangeCollateralMethod(
            web3_or_provider,
            contract_address,
            functions.removeExchangeCollateral,
            validator,
        )

        self.send_ddx_from_trader_to_trader_wallet = (
            SendDdxFromTraderToTraderWalletMethod(
                web3_or_provider,
                contract_address,
                functions.sendDDXFromTraderToTraderWallet,
                validator,
            )
        )

        self.set_max_ddx_cap = SetMaxDdxCapMethod(
            web3_or_provider,
            contract_address,
            functions.setMaxDDXCap,
            validator,
        )

        self.set_reward_cliff = SetRewardCliffMethod(
            web3_or_provider,
            contract_address,
            functions.setRewardCliff,
            validator,
        )

        self.stake_ddx_from_trader = StakeDdxFromTraderMethod(
            web3_or_provider,
            contract_address,
            functions.stakeDDXFromTrader,
            validator,
        )

        self.withdraw = WithdrawMethod(
            web3_or_provider, contract_address, functions.withdraw, validator
        )

        self.withdraw_ddx = WithdrawDdxMethod(
            web3_or_provider,
            contract_address,
            functions.withdrawDDX,
            validator,
        )

        self.withdraw_ddx_to_trader = WithdrawDdxToTraderMethod(
            web3_or_provider,
            contract_address,
            functions.withdrawDDXToTrader,
            validator,
        )

    def get_ddx_reward_issued_event(
        self, tx_hash: Union[HexBytes, bytes]
    ) -> Tuple[AttributeDict]:
        """Get log entry for DDXRewardIssued event.

        :param tx_hash: hash of transaction emitting DDXRewardIssued event
        """
        tx_receipt = self._web3_eth.getTransactionReceipt(tx_hash)
        return (
            self._web3_eth.contract(
                address=to_checksum_address(self.contract_address),
                abi=Trader.abi(),
            )
            .events.DDXRewardIssued()
            .processReceipt(tx_receipt)
        )

    def get_exchange_collateral_added_event(
        self, tx_hash: Union[HexBytes, bytes]
    ) -> Tuple[AttributeDict]:
        """Get log entry for ExchangeCollateralAdded event.

        :param tx_hash: hash of transaction emitting ExchangeCollateralAdded
            event
        """
        tx_receipt = self._web3_eth.getTransactionReceipt(tx_hash)
        return (
            self._web3_eth.contract(
                address=to_checksum_address(self.contract_address),
                abi=Trader.abi(),
            )
            .events.ExchangeCollateralAdded()
            .processReceipt(tx_receipt)
        )

    def get_exchange_collateral_removed_event(
        self, tx_hash: Union[HexBytes, bytes]
    ) -> Tuple[AttributeDict]:
        """Get log entry for ExchangeCollateralRemoved event.

        :param tx_hash: hash of transaction emitting ExchangeCollateralRemoved
            event
        """
        tx_receipt = self._web3_eth.getTransactionReceipt(tx_hash)
        return (
            self._web3_eth.contract(
                address=to_checksum_address(self.contract_address),
                abi=Trader.abi(),
            )
            .events.ExchangeCollateralRemoved()
            .processReceipt(tx_receipt)
        )

    def get_max_ddx_cap_set_event(
        self, tx_hash: Union[HexBytes, bytes]
    ) -> Tuple[AttributeDict]:
        """Get log entry for MaxDDXCapSet event.

        :param tx_hash: hash of transaction emitting MaxDDXCapSet event
        """
        tx_receipt = self._web3_eth.getTransactionReceipt(tx_hash)
        return (
            self._web3_eth.contract(
                address=to_checksum_address(self.contract_address),
                abi=Trader.abi(),
            )
            .events.MaxDDXCapSet()
            .processReceipt(tx_receipt)
        )

    def get_reward_cliff_set_event(
        self, tx_hash: Union[HexBytes, bytes]
    ) -> Tuple[AttributeDict]:
        """Get log entry for RewardCliffSet event.

        :param tx_hash: hash of transaction emitting RewardCliffSet event
        """
        tx_receipt = self._web3_eth.getTransactionReceipt(tx_hash)
        return (
            self._web3_eth.contract(
                address=to_checksum_address(self.contract_address),
                abi=Trader.abi(),
            )
            .events.RewardCliffSet()
            .processReceipt(tx_receipt)
        )

    def get_strategy_updated_event(
        self, tx_hash: Union[HexBytes, bytes]
    ) -> Tuple[AttributeDict]:
        """Get log entry for StrategyUpdated event.

        :param tx_hash: hash of transaction emitting StrategyUpdated event
        """
        tx_receipt = self._web3_eth.getTransactionReceipt(tx_hash)
        return (
            self._web3_eth.contract(
                address=to_checksum_address(self.contract_address),
                abi=Trader.abi(),
            )
            .events.StrategyUpdated()
            .processReceipt(tx_receipt)
        )

    def get_trader_updated_event(
        self, tx_hash: Union[HexBytes, bytes]
    ) -> Tuple[AttributeDict]:
        """Get log entry for TraderUpdated event.

        :param tx_hash: hash of transaction emitting TraderUpdated event
        """
        tx_receipt = self._web3_eth.getTransactionReceipt(tx_hash)
        return (
            self._web3_eth.contract(
                address=to_checksum_address(self.contract_address),
                abi=Trader.abi(),
            )
            .events.TraderUpdated()
            .processReceipt(tx_receipt)
        )

    @staticmethod
    def abi():
        """Return the ABI to the underlying contract."""
        return json.loads(
            '[{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"trader","type":"address"},{"indexed":false,"internalType":"uint96","name":"amount","type":"uint96"}],"name":"DDXRewardIssued","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"collateralToken","type":"address"},{"indexed":false,"internalType":"address","name":"underlyingToken","type":"address"},{"indexed":false,"internalType":"enum SharedDefs.Flavor","name":"flavor","type":"uint8"}],"name":"ExchangeCollateralAdded","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"collateralToken","type":"address"}],"name":"ExchangeCollateralRemoved","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"oldMaxCap","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"newMaxCap","type":"uint256"}],"name":"MaxDDXCapSet","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"bool","name":"rewardCliffSet","type":"bool"}],"name":"RewardCliffSet","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"trader","type":"address"},{"indexed":false,"internalType":"address","name":"collateralAddress","type":"address"},{"indexed":false,"internalType":"bytes32","name":"strategyId","type":"bytes32"},{"indexed":false,"internalType":"uint128","name":"amount","type":"uint128"},{"indexed":false,"internalType":"enum TraderDefs.StrategyUpdateType","name":"updateType","type":"uint8"}],"name":"StrategyUpdated","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"trader","type":"address"},{"indexed":false,"internalType":"uint128","name":"amount","type":"uint128"},{"indexed":false,"internalType":"enum TraderDefs.TraderUpdateType","name":"updateType","type":"uint8"}],"name":"TraderUpdated","type":"event"},{"inputs":[{"internalType":"address","name":"_collateralToken","type":"address"}],"name":"addExchangeCollateral","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_collateralAddress","type":"address"},{"internalType":"bytes32","name":"_strategyId","type":"bytes32"},{"internalType":"uint128","name":"_amount","type":"uint128"}],"name":"deposit","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint128","name":"_amount","type":"uint128"}],"name":"depositDDX","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_collateralToken","type":"address"}],"name":"getExchangeCollateralInfo","outputs":[{"components":[{"internalType":"address","name":"underlyingToken","type":"address"},{"internalType":"enum SharedDefs.Flavor","name":"flavor","type":"uint8"},{"internalType":"bool","name":"isListed","type":"bool"}],"internalType":"struct TraderDefs.ExchangeCollateral","name":"","type":"tuple"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getMaxDDXCap","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_trader","type":"address"}],"name":"getTrader","outputs":[{"components":[{"internalType":"uint96","name":"ddxBalance","type":"uint96"},{"internalType":"address","name":"ddxWalletContract","type":"address"}],"internalType":"struct TraderDefs.Trader","name":"","type":"tuple"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_traderAddress","type":"address"},{"internalType":"uint256","name":"_epochId","type":"uint256"},{"internalType":"address","name":"_collateralAddress","type":"address"}],"name":"getWithdrawalsInEpoch","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"contract IDDXWalletCloneable","name":"_ddxWalletCloneable","type":"address"}],"name":"initialize","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint96","name":"_amount","type":"uint96"},{"internalType":"address","name":"_trader","type":"address"}],"name":"issueDDXReward","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint96","name":"_amount","type":"uint96"},{"internalType":"address","name":"_recipient","type":"address"}],"name":"issueDDXToRecipient","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_collateralToken","type":"address"}],"name":"removeExchangeCollateral","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_trader","type":"address"},{"internalType":"uint96","name":"_amount","type":"uint96"}],"name":"sendDDXFromTraderToTraderWallet","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_maxDDXCap","type":"uint256"}],"name":"setMaxDDXCap","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bool","name":"_rewardCliff","type":"bool"}],"name":"setRewardCliff","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint96","name":"_amount","type":"uint96"}],"name":"stakeDDXFromTrader","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes32","name":"_strategyId","type":"bytes32"},{"components":[{"internalType":"address[]","name":"tokens","type":"address[]"},{"internalType":"uint128[]","name":"amounts","type":"uint128[]"}],"internalType":"struct SharedDefs.WithdrawalData","name":"_withdrawalData","type":"tuple"},{"components":[{"internalType":"bytes32","name":"strategyId","type":"bytes32"},{"components":[{"internalType":"address[]","name":"tokens","type":"address[]"},{"internalType":"uint128[]","name":"amounts","type":"uint128[]"}],"internalType":"struct SharedDefs.WithdrawalData","name":"freeCollateral","type":"tuple"},{"components":[{"internalType":"address[]","name":"tokens","type":"address[]"},{"internalType":"uint128[]","name":"amounts","type":"uint128[]"}],"internalType":"struct SharedDefs.WithdrawalData","name":"frozenCollateral","type":"tuple"},{"internalType":"uint64","name":"maxLeverage","type":"uint64"},{"internalType":"bool","name":"frozen","type":"bool"}],"internalType":"struct TraderDefs.StrategyData","name":"_strategy","type":"tuple"},{"internalType":"bytes","name":"_proof","type":"bytes"}],"name":"withdraw","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint128","name":"_amount","type":"uint128"},{"components":[{"internalType":"uint128","name":"freeDDXBalance","type":"uint128"},{"internalType":"uint128","name":"frozenDDXBalance","type":"uint128"},{"internalType":"address","name":"referralAddress","type":"address"}],"internalType":"struct TraderDefs.TraderData","name":"_trader","type":"tuple"},{"internalType":"bytes","name":"_proof","type":"bytes"}],"name":"withdrawDDX","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint96","name":"_amount","type":"uint96"}],"name":"withdrawDDXToTrader","outputs":[],"stateMutability":"nonpayable","type":"function"}]'  # noqa: E501 (line-too-long)
        )


# pylint: disable=too-many-lines
