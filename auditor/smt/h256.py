"""
H256 module
"""


class H256:
    BYTE_SIZE = 8
    MAX = 256

    def __init__(self, bytes_array):
        self.bytes_array = bytes_array

    def __eq__(self, other):
        if not isinstance(other, H256):
            # don't attempt to compare against unrelated types
            return NotImplemented

        return self.bytes_array == other.bytes_array

    def get_bit_string(self):
        return format(int.from_bytes(self.bytes_array, "little"), "0256b")

    @staticmethod
    def zero():
        return H256(int(0).to_bytes(32, "little"))

    def is_zero(self):
        return self.bytes_array == self.zero().bytes_array

    def get_bit(self, bit_index):
        byte_pos = bit_index // H256.BYTE_SIZE
        bit_pos_in_byte = bit_index % H256.BYTE_SIZE
        return (self.bytes_array[byte_pos] >> bit_pos_in_byte) & 1

    def set_bit(self, bit_index):
        byte_pos = bit_index // H256.BYTE_SIZE
        bit_pos_in_byte = bit_index % H256.BYTE_SIZE
        bytes_array_temp = bytearray(self.bytes_array)
        bytes_array_temp[byte_pos] = bytes_array_temp[byte_pos] | (1 << bit_pos_in_byte)
        self.bytes_array = bytes(bytes_array_temp)

    def clear_bit(self, bit_index):
        byte_pos = bit_index // H256.BYTE_SIZE
        bit_pos_in_byte = bit_index % H256.BYTE_SIZE
        bytes_array_temp = bytearray(self.bytes_array)
        bytes_array_temp[byte_pos] = bytes_array_temp[byte_pos] & ~(
            1 << bit_pos_in_byte
        )
        self.bytes_array = bytes(bytes_array_temp)

    def fork_height(self, key):
        for h in range(2 ** H256.BYTE_SIZE - 1, -1, -1):
            if self.get_bit(h) != key.get_bit(h):
                return h
        return 0

    def parent_path(self, height):
        checked_height = height + 1
        if checked_height > H256.MAX:
            return H256.zero()
        copied_bits = self.copy_bits(checked_height)
        if copied_bits is not None:
            return copied_bits
        return H256.zero()

    def copy_bits(self, checked_height):
        target = H256.zero()
        start = checked_height
        end = H256.MAX

        remain = 1 if start & H256.BYTE_SIZE != 0 else 0
        start_byte = (start // H256.BYTE_SIZE) + remain

        end_byte = end // H256.BYTE_SIZE

        if (start_byte < len(self.bytes_array)) and start_byte <= end_byte:
            bytes_array_target = bytearray(target.bytes_array)
            bytes_array_self = bytearray(self.bytes_array)
            bytes_array_target[start_byte:end_byte] = bytes_array_self[
                start_byte:end_byte
            ]

        for i in list(range(start, min(start_byte * H256.BYTE_SIZE, end))) + list(
            range(max(end_byte * H256.BYTE_SIZE, end))
        ):
            if self.get_bit(i):
                target.set_bit(i)
        return target
