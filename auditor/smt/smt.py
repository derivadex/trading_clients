#!/usr/bin/env python
# coding: utf-8

from pathlib import Path

from ddx_client.auditor.utils import h256_to_bytes_array

Path.ls = lambda x: list(x.files())
from BTrees.OOBTree import OOBTree
from Crypto.Hash import keccak
from collections import deque

from ddx_client.auditor.state.item import Item
from ddx_client.auditor.smt.h256 import H256


class Error(Exception):
    """Base class for exceptions in this module."""

    pass


class EmptyKeysError(Error):
    """Exception raised for errors in the input.

    Attributes:
        expression -- input expression in which the error occurred
        message -- explanation of the error
    """

    def __init__(self):
        pass


class IncorrectNumberOfLeavesError(Error):
    """Exception raised for errors in the input.

    Attributes:
        expression -- input expression in which the error occurred
        message -- explanation of the error
    """

    def __init__(self, expected, actual):
        self.expected = expected
        self.actual = actual


class DefaultStore:
    def __init__(self):
        self._branches_map = {}
        self._leaves_map = {}

    def branches_map(self):
        return self._branches_map

    def leaves_map(self):
        return self._leaves_map

    def clear(self):
        self._branches_map = {}
        self._leaves_map = {}


class Store(DefaultStore):
    def __init__(self):
        super().__init__()

    def get_branch(self, node):
        return self._branches_map.get(node.bytes_array)

    def get_leaf(self, leaf):
        return self._leaves_map.get(leaf.bytes_array)

    def insert_branch(self, node, branch):
        self._branches_map[node.bytes_array] = branch

    def insert_leaf(self, leaf_hash, leaf):
        self._leaves_map[leaf_hash.bytes_array] = leaf

    def remove_branch(self, node):
        del self._branches_map[node.bytes_array]

    def remove_leaf(self, leaf_hash):
        del self._leaves_map[leaf_hash.bytes_array]


class BranchNode:
    def __init__(self, fork_height, key, node_type):
        self.fork_height = fork_height
        self.key = key
        self.node_type = node_type

    def node_at(self, height: int):
        if type(self.node_type) is tuple:
            node, sibling = self.node_type
            if self.key.get_bit(height):
                return sibling, node
            return node, sibling
        return self.node_type


class LeafNode:
    def __init__(self, key, value):
        self.key = key
        self.value = value


class Hasher:
    def __init__(self):
        self.keccak_256 = keccak.new(digest_bits=256)

    def write_h256(self, h):
        self.keccak_256.update(h.bytes_array)

    def finish(self):
        return H256(self.keccak_256.digest())


def hash_leaf(key, value):
    if value.is_zero():
        return H256.zero()
    hasher = Hasher()
    hasher.write_h256(key)
    hasher.write_h256(value)
    return hasher.finish()


def merge(lhs, rhs):
    if lhs.is_zero():
        return rhs
    elif rhs.is_zero():
        return lhs
    hasher = Hasher()
    hasher.write_h256(lhs)
    hasher.write_h256(rhs)
    return hasher.finish()


class MerkleProof:
    def __init__(self, leaves_path=[], proof=[]):
        self.leaves_path = leaves_path
        self.proof = proof

    def take(self):
        return (self.leaves_path.copy(), self.proof.copy())

    def leaves_count(self):
        return len(self.leaves_path)

    def leaves_path(self):
        return self.leaves_path

    def proof(self):
        return self.proof

    def compute_root(self, leaves):
        if not leaves:
            return
        elif len(leaves) != self.leaves_count():
            raise IncorrectNumberOfLeavesError(self.leaves_count(), len(leaves))

        (leaves_path, proof) = self.take()
        leaves_path = [deque(lp) for lp in leaves_path]
        proof = deque(proof)

        leaves.sort(key=lambda x: x[0])

        tree_buf = OOBTree(
            [((0, k), (i, hash_leaf(k, v))) for (i, (k, v)) in enumerate(leaves)]
        )

        while True:
            try:
                ((height, key), (leaf_index, node)) = next(tree_buf.iteritems())
                tree_buf.pop((height, key))

                if not proof and not tree_buf:
                    return node
                elif height == SparseMerkleTree.TREE_HEIGHT:
                    if proof:
                        return
                    return node

                sibling_key = key.parent_path(height)
                if not key.get_bit(height):
                    sibling_key.set_bit(height)
                if (height, sibling_key) == next(tree_buf.iterkeys()):
                    (_leaf_index, sibling) = tree_buf.remove((height, sibling_key))
                    (sibling, sibling_height) = (sibling, height)
                else:
                    merge_height = leaves_path[leaf_index][-1]
                    if merge_height is None:
                        merge_height = height
                    if height != merge_height:
                        parent_key = key.copy_bits(merge_height)
                        tree_buf.insert((merge_height, parent_key), (leaf_index, node))
                        continue
                    (node, height) = proof.pop()
                    (sibling, sibling_height) = (node, height)
                if height < sibling_height:
                    height = sibling_height
                parent_key = key.parent_path(height)

                parent = (
                    merge(sibling, node)
                    if key.get_bit(height)
                    else merge(node, sibling)
                )

                leaves_path[leaf_index].pop()
                tree_buf.insert((height + 1, parent_key), (leaf_index, parent))
            except IndexError:
                print("index error")
                break


class SparseMerkleTree:
    UINT8_MAX = 255
    TREE_HEIGHT = 256

    def __init__(self, store=None, root=H256(int(0).to_bytes(32, "little"))):
        if store is None:
            store = Store()
        self._store = store
        self._root = root

    def new(self, root, store):
        return self.__class__(store, root)

    def root(self):
        return self._root

    def is_empty(self):
        return self._root.is_zero()

    def take_store(self):
        return self._store

    def store(self):
        return self._store

    def update(self, key: H256, value: Item):
        path = []
        if not self.is_empty():
            node = self._root
            while True:
                branch_node = self._store.get_branch(node)
                height = max(key.fork_height(branch_node.key), branch_node.fork_height)
                node_type = branch_node.node_at(height)
                if type(node_type) is tuple:
                    left, right = node_type
                    if height > branch_node.fork_height:
                        path.append((height, node))
                        break
                    else:
                        self._store.remove_branch(node)
                        if key.get_bit(height):
                            node = right
                            path.append((height, left))
                        else:
                            node = left
                            path.append((height, right))
                else:
                    if key == branch_node.key:
                        self._store.remove_leaf(node)
                        self._store.remove_branch(node)
                    else:
                        path.append((height, node))
                    break

        # compute and store new leaf
        node = hash_leaf(key, value.as_h256)
        if not node.is_zero():
            self._store.insert_leaf(node, LeafNode(key, value))
            self._store.insert_branch(node, BranchNode(0, key, node))

        for height, sibling in reversed(path):
            is_right = key.get_bit(height)
            parent = merge(sibling, node) if is_right else merge(node, sibling)
            if not node.is_zero():
                branch_node = BranchNode(height, key, (node, sibling))
                self._store.insert_branch(parent, branch_node)
            node = parent
        self._root = node
        return self._root

    def get(self, key):
        if self.is_empty():
            return H256.zero()

        node = self._root

        while True:
            branch_node = self._store.get_branch(node)
            node_type = branch_node.node_at(branch_node.fork_height)
            if type(node_type) is tuple:
                left, right = node_type
                is_right = key.get_bit(branch_node.fork_height)
                node = right if is_right else left
            else:
                if key == branch_node.key:
                    return self._store.get_leaf(node).value
                return H256.zero()

    def fetch_merkle_path(self, key, cache):
        node = self.root()
        branch = self._store.get_branch(node)
        height = (
            max(branch.key.fork_height(key), branch.fork_height)
            if branch is not None
            else 0
        )
        while not node.is_zero():
            if node.is_zero():
                break
            branch_node = self._store.get_branch(node)
            if branch_node is not None:
                if height > branch_node.fork_height:
                    fork_height = max(
                        key.fork_height(branch_node.key), branch_node.fork_height
                    )
                    is_right = key.get_bit(fork_height)
                    sibling_key = key.parent_path(fork_height)
                    if not is_right:
                        sibling_key.set_bit(height)
                    if not node.is_zero():
                        if not cache.has_key((fork_height, sibling_key)):
                            cache.insert((fork_height, sibling_key), node)
                    break
                (left, right) = branch_node.branch(height)
                is_right = key.get_bit(height)
                if is_right:
                    if node == right:
                        break
                    sibling = left
                    node = right
                else:
                    if node == left:
                        break
                    sibling = right
                    node = left
                sibling_key = key.parent_path(height)
                if not is_right:
                    sibling_key.set_bit(height)
                cache.insert((height, sibling_key), sibling)
                branch_node = self._store.get_branch(node)
                if branch_node is not None:
                    fork_height = max(
                        key.fork_height(branch_node.key), branch_node.fork_height
                    )
                    height = fork_height
            else:
                break

    def merkle_proof(self, keys):
        if not keys:
            return
        keys.sort()

        cache = OOBTree()
        for k in keys:
            self.fetch_merkle_path(k, cache)
        proof = []
        leaves_path = [[]]
        keys_len = len(keys)
        queue = deque([(k, 0, i) for (i, k) in enumerate(keys)])
        while True:
            try:
                (key, height, leaf_index) = queue.pop()
                if (not queue and not cache) or (
                    height == SparseMerkleTree.TREE_HEIGHT
                ):
                    if not leaves_path[leaf_index]:
                        leaves_path[leaf_index].append(SparseMerkleTree.UINT8_MAX)
                    break
                sibling_key = key.parent_path(height)
                is_right = key.get_bit(height)
                if is_right:
                    sibling_key.clear_bit(height)
                else:
                    sibling_key.set_bit(height)
                if (sibling_key, height, leaf_index) == queue[-1]:
                    (_sibling_key, height, leaf_index) = queue.pop()
                    leaves_path[leaf_index].append(height)
                else:
                    sibling = cache.remove((height, sibling_key))
                    if sibling is not None:
                        proof.append((sibling, height))
                    else:
                        if not is_right:
                            sibling_key.clear_bit(height)
                        parent_key = sibling_key
                        queue.appendleft((parent_key, height + 1, leaf_index))
                        continue
                leaves_path[leaf_index].append(height)
                if height < SparseMerkleTree.TREE_HEIGHT:
                    parent_key = sibling_key if is_right else key
                    queue.appendleft((parent_key, height + 1, leaf_index))
            except IndexError:
                print("index error")
                break
        return MerkleProof(leaves_path, proof)
