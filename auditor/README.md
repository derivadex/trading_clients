# DerivaDEX Auditor

The pre-packaged Pythonic Auditor connects to the DerivaDEX WebSocket Trader API upon initialization and underpins an extremely powerful, efficient, and easy-to-use setup for programmatic traders by allowing you to:

1. place orders, cancel orders, and signal withdrawal of funds

2. validate the honesty and integrity of the exchange's operations

3. use the Auditor as a backend datastore and API to ensure you are verifiably up-to-date with the latest exchange data

DerivaDEX's data model is powered by a Sparse Merkle Tree data store and a transaction log of state-modifying transitions. While these are emitted in raw formats by the DerivaDEX Trader API, they are not the most easily-consumable. Thus, the Auditor provides a worthwhile abstraction that efficiently allows you to locally maintain your own data validation while exposing an API for your easy consumption. If you would like to dive deeper into how the Auditor processes the state and raw transactions on DerivaDEX or would like to write your own Auditor tooling (in a different language perhaps, or to expose an interface more to your liking), please check out the Trader API <> Auditor section. Otherwise, you are more than welcome to just check out the Setup section to get the Python tooling running and serving your trading needs.

## Setup

### From source

To run the Auditor, follow these steps:

1. If you don't already have it, we recommend setting up Anaconda/Python(>3) on your machine

2. Initialize and activate a Conda environment (located at the root level of the repo) from which you will run the Auditor: `conda env create -f environment.yml && conda activate derivadex`

3. Set your PYTHONPATH: `export PYTHONPATH="${PYTHONPATH}:/your/full/path/upto/but/not/including/ddx_client"`

4. Navigate to the `auditor` subdirectory and create an `.auditor.conf.json` file using the template: `cp .auditor.conf.json.template .auditor.conf.json`

5. Run the Auditor with: `PYTHON_LOG=verbose python auditor_driver.py --config ".auditor.conf.json"`

You will immediately see logging messages (should be lots of green) with state initialized and transactions streaming through successfully.
