class Identifier:
    def encompasses_identifier(self, identifier):
        raise NotImplementedError

    @property
    def topic_string(self):
        raise NotImplementedError

    @classmethod
    def decode_topic_string_into_cls(cls, topic: str):
        """
        Decode a topic string into an Identifier wrapper.

        Parameters
        ----------
        topic : str
            Topic string
        """

        raise NotImplementedError

    @classmethod
    def decode_key_into_cls(cls, leaf_key: bytes, leaf_value=None):
        """
        Decode a key (and optionally, a leaf value as well) into an
        Identifier wrapper.

        Parameters
        ----------
        leaf_key : bytes
            Leaf key
        leaf_value
            Leaf value
        """

        raise NotImplementedError

    @classmethod
    def decode_tx_into_cls(cls, tx):
        """
        Decode a tx into an Identifier wrapper.

        Parameters
        ----------
        tx
            Transaction
        """

        raise NotImplementedError

    @property
    def is_max_granular_key(self):
        raise NotImplementedError
