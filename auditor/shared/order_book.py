"""
DerivaDEX Order Book (L2)
"""

from collections import deque
from typing import Dict, Optional, List, Tuple
from ddx_python.decimal import Decimal
import numpy as np

from ddx_client.auditor.smt.h256 import H256
from ddx_client.auditor.smt.smt import SparseMerkleTree
from ddx_client.auditor.state.book_order import BookOrder
from ddx_client.auditor.transactions.cancel import Cancel
from ddx_client.auditor.transactions.fill import Fill


class OrderBook:
    def __init__(self, smt: SparseMerkleTree) -> None:
        """
        Initialize an order book. Each order book is tied to a given
        market, and contains both L2 and L3 information. An L2 order
        book is an aggregate view of the market, providing information
        regarding unique price levels and total quantities at those
        price levels. L3 order books maintain order-by-order, granular
        data. Users could query the SMT to obtain order book data
        (by querying the BookOrder leaves), however this local order
        book allows for faster and more intuitive querying for use
        by any trader clients.
        """

        # Maintain a mapping of price level to FIFO order queue (L3).
        # It's worth noting that we don't actually store the orders
        # themselves as that would be wasteful given that all of that
        # data already resides in the SMT. However, we do store the
        # BookOrder keys, thus we can query the SMT with these keys
        # to always have an up-to-date L3 order book.
        self.book_orders: List[Tuple[H256, BookOrder]] = []
        self.bid_keys: Dict[Decimal, deque[H256]] = {}
        self.ask_keys: Dict[Decimal, deque[H256]] = {}

        # Maintain a mapping of price level to aggregate quantity (L2).
        self.bids: Dict[Decimal, Decimal] = {}
        self.asks: Dict[Decimal, Decimal] = {}

        # A copy of the DerivaDEX SMT, which is used to query for
        # certain leaves to validate against the incoming transactions
        self.smt = smt

    @property
    def best_bid_px(self) -> Optional[Decimal]:
        """
        Property returning the best available (highest) bid price.
        """

        return max(list(self.bids.keys())) if self.bids else None

    @property
    def best_ask_px(self) -> Optional[Decimal]:
        """
        Property returning the best available (lowest) ask price.
        """

        return min(list(self.asks.keys())) if self.asks else None

    @property
    def mid_px(self) -> Optional[Decimal]:
        """
        Property returning the mid price for the market.
        """

        if self.bids and self.asks:
            return np.mean([self.best_bid_px, self.best_ask_px])
        elif self.bids:
            return self.best_bid_px
        elif self.asks:
            return self.best_ask_px
        return None

    @property
    def order_book_l2(self) -> Dict:
        """
        Property returning the L2 order book.
        """

        return {"bids": self.bids, "asks": self.asks}

    @property
    def order_book_l3(self) -> Dict:
        """
        Property returning the L3 order book. Keep in mind, these aren't
        the orders themselves, but rather the book order keys. These can
        be used to query into the SMT to retrieve the full order data.
        """

        return {"bids": self.bid_keys, "asks": self.ask_keys}

    @staticmethod
    def is_post_tx_wider_than_price(post_tx, price: Decimal) -> bool:
        """
        Check whether the inbound post is wider than a given price. If
        a post is wider, that means it is farther off from matching. In
        other words, a bid is wider than a price if it is lower. If
        an ask is wider than a price, it is higher.

        Parameters
        ----------
        post_tx : Post
            Inbound post transaction
        price : Decimal
            Price point the post transaction will be compared against
        """

        return (
            (price is not None and post_tx.price < price)
            if post_tx.side == "Bid"
            else (price is not None and post_tx.price > price)
        )

    @staticmethod
    def is_post_tx_tighter_than_or_equal_to_price(post_tx, price: Decimal) -> bool:
        """
        Check whether the inbound post is tighter than a given price. If
        a post is tighter, that means it is more likely to match. In
        other words, a bid is tighter than a price if it is higher. If
        an ask is tighter than a price, it is lower.

        Parameters
        ----------
        post_tx : Post
            Inbound post transaction
        price : Decimal
            Price point the post transaction will be compared against
        """

        return (
            (price is not None and post_tx.price >= price)
            if post_tx.side == "Bid"
            else (price is not None and post_tx.price <= price)
        )

    def process_book_orders_from_queue(self):
        sorted_book_orders = sorted(
            self.book_orders,
            key=lambda book_order: book_order[1].book_ordinal,
        )
        for book_order_key, book_order in sorted_book_orders:
            # Obtain the sided L3 order book
            sided_book_keys = (
                self.bid_keys if book_order.side == "Bid" else self.ask_keys
            )

            # Obtain the sided L2 order book
            sided_book = self.bids if book_order.side == "Bid" else self.asks

            if book_order.price not in sided_book_keys:
                # If the book order's price is not in the book

                # Establish a new L3 order book price level with a
                # single-item deque (FIFO queue) of the book order key
                sided_book_keys[book_order.price] = deque([book_order_key])

                # Establish a new L2 order book price level with an
                # aggregate amount equal to this book order's amount
                sided_book[book_order.price] = book_order.amount
            else:
                # If the book order's price is in the book

                # Append this book order key to the back of the L3 order
                # book's queue at that price level
                sided_book_keys[book_order.price].append(book_order_key)

                # Increment the aggregate L2 order book amount for this
                # price level
                sided_book[book_order.price] += book_order.amount

    def process_book_order_from_state(
        self, book_order_key: H256, book_order: BookOrder
    ) -> None:
        self.book_orders.append((book_order_key, book_order))

    def apply_post_tx_to_book(self, book_order_key: H256, post_tx) -> None:
        """
        Apply post transaction to the order book.

        Parameters
        ----------
        book_order_key : H256
            BookOrder key for the order being posted
        post_tx : Post
            Post transaction being applied
        """

        # Obtain the sided L3 order book
        sided_book_keys = self.bid_keys if post_tx.side == "Bid" else self.ask_keys

        # Obtain the sided L2 order book
        sided_book = self.bids if post_tx.side == "Bid" else self.asks

        if self.is_post_tx_tighter_than_or_equal_to_price(
            post_tx, self.best_ask_px if post_tx.side == "Bid" else self.best_bid_px
        ):
            # Ensure post transaction is not crossing the book and
            # triggering a match
            raise Exception("Invalid tx log entry (post): order triggers a match")

        if post_tx.price not in sided_book_keys:
            # If the posted order price is not in the book

            # Establish a new L3 order book price level with a
            # single-item deque (FIFO queue) of the book order key
            sided_book_keys[post_tx.price] = deque([book_order_key])

            # Establish a new L2 order book price level with an
            # aggregate amount equal to this posted order amount
            sided_book[post_tx.price] = post_tx.amount
        else:
            # If the posted order's price is in the book

            # Append this book order key to the back of the L3 order
            # book's queue at that price level
            sided_book_keys[post_tx.price].append(book_order_key)

            # Increment the aggregate L2 order book amount for this
            # price level
            sided_book[post_tx.price] += post_tx.amount

    def apply_cancel_tx_to_book(self, book_order_key: H256, cancel_tx: Cancel) -> None:
        """
        Apply cancel transaction to the order book.

        Parameters
        ----------
        book_order_key : H256
            BookOrder key for the order being canceled
        cancel_tx : Cancel
            Cancel transaction being applied
        """

        # Get BookOrder leaf from the SMT given the key
        book_order: BookOrder = self.smt.get(book_order_key)

        # Obtain the sided L3 order book
        sided_book_keys = self.bid_keys if book_order.side == "Bid" else self.ask_keys

        # Obtain the sided L2 order book
        sided_book = self.bids if book_order.side == "Bid" else self.asks

        if book_order.amount != cancel_tx.amount:
            # Ensure the remaining amount for the BookOrder and the
            # amount being canceled as per the transaction log are equal
            raise Exception("Invalid tx log entry (cancel): amounts don't match")

        # Delete key from book order index since it's been canceled
        sided_book_keys[book_order.price].remove(book_order_key)

        # Reduce the L2 order book amount at this price level
        sided_book[book_order.price] -= cancel_tx.amount

        # Check if the local order book is empty
        if sided_book[book_order.price] == Decimal("0"):
            # Ensure that we have deleted the L3 order book index above
            if len(sided_book_keys[book_order.price]) != 0:
                raise Exception(
                    "Invalid tx log entry (cancel): mismatching L2 and L3 order books => indexes should be cleared"
                )

            # Delete the L3 and L2 price level
            del sided_book_keys[book_order.price]
            del sided_book[book_order.price]

    def apply_fill_to_book(self, book_order_key: H256, fill_tx: Fill) -> None:
        """
        Apply fill transaction to the order book.

        Parameters
        ----------
        book_order_key : H256
            BookOrder key for the maker order aspect of this fill
        fill_tx : Fill
            Fill transaction being applied
        """

        # Obtain the sided L3 order book
        sided_book_keys = (
            self.bid_keys if fill_tx.taker_side == "Ask" else self.ask_keys
        )

        # Obtain the sided L2 order book
        sided_book = self.bids if fill_tx.taker_side == "Ask" else self.asks

        # Given that matching is a FIFO operation, confirm that the
        # book order key corresponding to the maker order as specified
        # in the transaction log matches the first maker order index
        # in our local L3 order book
        if book_order_key != sided_book_keys[fill_tx.price][0]:
            raise Exception("Invalid tx log entry (fill): out of order match")

        # Get current maker book order from SMT
        current_maker_book_order: BookOrder = self.smt.get(book_order_key)

        if (
            fill_tx.maker_order_remaining_amount
            != current_maker_book_order.amount - fill_tx.amount
        ):
            # Ensure fill amount would result in the same remaining
            # asset amount
            raise Exception(
                "Invalid tx log entry (fill): mismatching remaining maker order amounts"
            )

        if current_maker_book_order.amount == fill_tx.amount:
            # Delete maker key from book order index if completely
            # filled
            sided_book_keys[fill_tx.price].popleft()

        # Reduce the L2 order book amount at this price level
        sided_book[fill_tx.price] -= fill_tx.amount

        if sided_book[fill_tx.price] == Decimal("0"):
            # If the local order book is empty
            if len(sided_book_keys[fill_tx.price]) != 0:
                # Ensure that after deleting the L3 order book index
                # above, there are no entries left at this level
                raise Exception(
                    "Invalid tx log entry (fill): mismatching L2 and L3 order books => indexes should be cleared"
                )

            # Delete the L3 and L2 price level
            del sided_book_keys[fill_tx.price]
            del sided_book[fill_tx.price]

    def __repr__(self):
        return f"Order book (L2): bids = {self.bids}; asks = {self.asks}"
