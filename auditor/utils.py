"""
Utils module
"""

from typing import List, TYPE_CHECKING, Dict
import simplejson as json
from ddx_python.decimal import Decimal
from decimal import Decimal as PyDecimal
from eth_abi.utils.padding import zpad_right
import bisect

from ddx_client.auditor.smt.h256 import H256

if TYPE_CHECKING:
    from ddx_client.auditor.state.position import Position


def round_to_unit(val: Decimal) -> Decimal:
    """
    Round a decimal value down to 6 units of precision.

    Parameters
    ----------
    val : Decimal
        Value to be rounded
    """

    return val.quantize(6)


def to_base_unit_amount(val: Decimal, decimals: int) -> int:
    """
    Convert a value to grains format (e.g. DDX grains would be
    multiplying by 10 ** 18).

    Parameters
    ----------
    val : Decimal
        Value to be scaled up
    decimals : int
        Number of decimal places to scale up to
    """

    return int(round_to_unit(val) * 10 ** decimals)


def to_unit_amount(val: int, decimals: int) -> Decimal:
    """
    Convert a value from grains format (e.g. from DDX grains would be
    dividing by 10 ** 18).

    Parameters
    ----------
    val : Decimal
        Value to be scaled down
    decimals : int
        Number of decimal places to scale down by
    """

    return Decimal(str(val)) / 10 ** decimals


def to_base_unit_amount_list(vals: List[Decimal], decimals: int) -> List[int]:
    """
    Convert values to grains format (e.g. DDX grains would be
    multiplying by 10 ** 18).

    Parameters
    ----------
    vals : List[Decimal]
        Values to be scaled up
    decimals : int
        Number of decimal places to scale up to
    """

    return [to_base_unit_amount(val, decimals) for val in vals]


def to_unit_amount_list(vals: List[int], decimals: int):
    """
    Convert values from grains format (e.g. from DDX grains would be
    dividing by 10 ** 18).

    Parameters
    ----------
    vals : List[int]
        Values to be scaled down
    decimals : int
        Number of decimal places to scale down by
    """

    return [to_unit_amount(val, decimals) for val in vals]


def to_adjusted_encoding_for_negative_val(val: int):
    """
    Adjust encoding for a potentially negative value

    Parameters
    ----------
    vals : int
        Values to be adjusted
    """

    return 16 ** 32 + abs(val) if val < 0 else val


def to_adjusted_encoding_for_negative_val_list(vals: List[int]):
    """
    Adjust encoding for list of potentially negative values

    Parameters
    ----------
    vals : List[int]
        List of values to be adjusted
    """

    return [to_adjusted_encoding_for_negative_val(val) for val in vals]


def from_adjusted_encoding_for_negative_val(val: int):
    """
    Adjust encoding for a potentially negative value

    Parameters
    ----------
    vals : int
        Values to be adjusted
    """

    return val if val < 16 ** 32 else -(val - 16 ** 32)


def member_want_role_to_int(want_role: str):
    """
    Convert 'NonVoter' and 'RegularNode' to 0 and 1 integer values.

    Parameters
    ----------
    want_role : str
        Either 'NonVoter' or 'RegularNode'
    """

    return 0 if want_role == "NonVoter" else 1


def member_int_to_want_role(want_role: int):
    """
    Convert 0 and 1 integer values to 'NonVoter' and 'RegularNode'.

    Parameters
    ----------
    want_role : int
        Either 0 or 1
    """

    return "NonVoter" if want_role == 0 else "RegularNode"


def strategy_update_kind_to_int(update_kind: str):
    """
    Convert 'Deposit' and 'Withdraw' to 0 and 1 integer values.

    Parameters
    ----------
    update_kind : str
        Either 'Deposit' or 'Withdraw'
    """

    return 0 if update_kind == "Deposit" else 1


def strategy_update_int_to_kind(update_kind: int):
    """
    Convert 'Deposit' and 'Withdraw' to 0 and 1 integer values.

    Parameters
    ----------
    update_kind : int
        Either 0 or 1
    """

    return "Deposit" if update_kind == 0 else "Withdraw"


def insurance_fund_update_kind_to_int(update_kind: str):
    """
    Convert 'Deposit' and 'Withdraw' to 0 and 1 integer values.

    Parameters
    ----------
    update_kind : str
        Either 'Deposit' or 'Withdraw'
    """

    return 0 if update_kind == "Deposit" else 1


def insurance_fund_update_int_to_kind(update_kind: int):
    """
    Convert 'Deposit' and 'Withdraw' to 0 and 1 integer values.

    Parameters
    ----------
    update_kind : int
        Either 0 or 1
    """

    return "Deposit" if update_kind == 0 else "Withdraw"


def book_order_side_to_int(side: str):
    """
    Convert 'Bid' and 'Ask' to 0 and 1 integer values.

    Parameters
    ----------
    side : str
        Either 'Bid' or 'Ask'
    """

    return 0 if side == "Bid" else 1


def book_order_int_to_side(side: int):
    """
    Convert 0 and 1 integer values to 'Bid' and 'Ask'.

    Parameters
    ----------
    side : int
        Either 0 or 1
    """

    return "Bid" if side == 0 else "Ask"


def position_side_to_int(side: str):
    """
    Convert 'Long' and 'Short' to 1 and 2 integer values.

    Parameters
    ----------
    side : str
        Either 'Long' or 'Short'
    """

    if side == "Long":
        return 1
    elif side == "Short":
        return 2
    return 0


def position_int_to_side(side: int):
    """
    Convert 1 and 2 integer values to 'Long' and 'Short'.

    Parameters
    ----------
    side : str
        Either 1 or 2
    """

    if side == 1:
        return "Long"
    elif side == 2:
        return "Short"
    return "None"


def get_side_for_order_and_trader(taker_side: str, is_maker: bool):
    """
    Get side for order and trader.

    Parameters
    ----------
    taker_side : str
        Either Bid or Ask
    is_maker : bool
        Whether trader is maker or taker
    """

    return ("Ask" if taker_side == "Bid" else "Bid") if is_maker else taker_side


def pack_bytes(text: str):
    charset = "0ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    symbol_bits = ""
    for letter in text:
        for i, char in enumerate(charset):
            if char == letter:
                bits = format(i, "08b")[::-1]
                symbol_bits += bits[:5]

    symbol_bytes = int(symbol_bits[::-1], 2).to_bytes(
        (len(symbol_bits) + 7) // 8, byteorder="little"
    )
    return zpad_right(symbol_bytes, 6)


def unpack_bytes(packed_text: bytes):
    charset = "0ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    symbol_bits = format(int.from_bytes(packed_text, "little"), "030b")[::-1]
    symbol_char_bit_chunks = [
        symbol_bits[i : i + 5] for i in range(0, len(symbol_bits), 5)
    ]
    symbol = ""
    for symbol_char_bit_chunk in symbol_char_bit_chunks:
        reversed_bit_chunk = symbol_char_bit_chunk[::-1]
        char_index = int(reversed_bit_chunk, 2)
        symbol += charset[char_index]
    return symbol


def bytes_array_to_h256(bytes_array: bytes) -> H256:
    """
    Convert bytes value to its H256 representation.

    Parameters
    ----------
    bytes_array : bytes
        Bytes representation of a value
    """

    return H256(bytes_array)


def h256_to_bytes_array(h256: H256) -> bytes:
    """
    Convert an H256 value to its bytes representation.

    Parameters
    ----------
    h256 : H256
        H256 representation of a value
    """

    return h256.bytes_array


def to_camel_case(snake_case_str: str) -> str:
    """
    Convert a string from snake_case to CamelCase.

    Parameters
    ----------
    snake_case_str : str
        snake_case string to be converted to CamelCase
    """

    return "".join(word.title() for word in snake_case_str.split("_"))


def is_valid_none_list(lst: List[any]) -> bool:
    """
    Whether a list is a valid list containing None values. A list is
    deemed valid if there are no gaps in None values, and that there are
    no non-None values after a None value, in other words None values
    must be tailing.

    Parameters
    ----------
    lst : List[any]
        List under consideration
    """

    none_idxs = [i for i, v in enumerate(lst) if v is None]
    return none_idxs == list(range(len(lst) - len(none_idxs), len(lst)))


def compute_avg_pnl(position_leaf: "Position", price: Decimal):
    return (
        price - position_leaf.avg_entry_price
        if position_leaf.side == "Long"
        else position_leaf.avg_entry_price - price
    )


def calculate_max_collateral(collateral_tranches: List, ddx_balance: Decimal):
    limit = Decimal("0")
    for ddx_threshold, max_collateral in collateral_tranches:
        limit = max_collateral
        if ddx_balance < ddx_threshold:
            break
    return limit


class ComplexOutputEncoder(json.JSONEncoder):
    """
    Custom JSON-encoder for serializing objects
    """

    def __init__(self, **kwargs):
        super(ComplexOutputEncoder, self).__init__(**kwargs)

    def default(self, o):
        if hasattr(o, "repr_json"):
            return o.repr_json()
        elif type(o) == Decimal:
            return PyDecimal(str(o))
        return json.JSONEncoder.default(self, o)
