"""
Strategy module
"""
from typing import Dict

from eth_abi.utils.padding import zpad32_right
from eth_abi import encode_single, decode_single
from web3.auto import w3
from ddx_python.decimal import Decimal

from ddx_client.auditor.state.abi_constant import AbiConstant
from ddx_client.auditor.state.empty import Empty
from ddx_client.auditor.state.item import Item
from ddx_client.auditor.state.item_kinds import ItemKind
from ddx_client.auditor.utils import (
    to_base_unit_amount,
    to_unit_amount,
    bytes_array_to_h256,
)


class Trader(Item):
    """
    Defines a Trader
    """

    def __init__(
        self,
        free_ddx_balance: Decimal,
        frozen_ddx_balance: Decimal,
        referral_address: str,
        pay_fees_in_ddx: bool,
    ):
        """
        Initialize a Trader leaf. A Trader contains information
        pertaining to a trader's free and frozen DDX balances, on-chain
        wallet contract address, and referral address (the person who
        referred them, if applicable).

        Parameters
        ----------
        free_ddx_balance : Decimal
           DDX collateral available for trading/staking/fees
        frozen_ddx_balance : Decimal
           DDX collateral available for on-chain withdrawal
        referral_address: str
           Referral address pertaining to the Ethereum address who
           referred this trader (if applicable)
        pay_fees_in_ddx: bool
           Whether trader has opted to pay fees in DDX by default
        """

        self.item_kind = ItemKind.TRADER
        self.free_ddx_balance = free_ddx_balance
        self.frozen_ddx_balance = frozen_ddx_balance
        self.referral_address = referral_address
        self.pay_fees_in_ddx = pay_fees_in_ddx

    @property
    def abi_encoded_value(self):
        """
        Property returning the custom ABI tokenization of the Strategy.
        """

        # Scale collateral amounts to DDX grains
        return encode_single(
            AbiConstant.TRADER,
            [
                self.item_kind,
                [
                    to_base_unit_amount(self.free_ddx_balance, 6),
                    to_base_unit_amount(self.frozen_ddx_balance, 6),
                    self.referral_address,
                    self.pay_fees_in_ddx,
                ],
            ],
        )

    @property
    def hash_value(self):
        """
        Property returning the keccak256 hash of the encoded Trader.
        """

        return w3.keccak(self.abi_encoded_value)

    @property
    def as_h256(self):
        """
        Property returning the H256 repr of the hash_value of the
        Trader.
        """

        return bytes_array_to_h256(self.hash_value)

    @classmethod
    def abi_decode_value_into_cls(cls, abi_encoded_value):
        """
        ABI Decode an ABI-encoded value from the state snapshot into
        a Trader leaf instance.

        Parameters
        ----------
        abi_encoded_value : str
           Hexstr repr of the ABI-encoded Price Trader from the
           state snapshot
        """

        (
            item_kind,
            (free_ddx_balance, frozen_ddx_balance, referral_address, pay_fees_in_ddx),
        ) = decode_single(
            AbiConstant.TRADER,
            w3.toBytes(hexstr=abi_encoded_value),
        )

        # Scale collateral amounts from DDX grains
        return cls(
            to_unit_amount(free_ddx_balance, 6),
            to_unit_amount(frozen_ddx_balance, 6),
            referral_address,
            pay_fees_in_ddx,
        )

    @classmethod
    def decode_value_into_cls(cls, raw_leaf: Dict):
        """
        Decode a raw leaf (dict) into a Trader
        instance.

        Parameters
        ----------
        raw_leaf : Dict
            Raw leaf being processed
        """

        if not raw_leaf:
            return Empty()

        return cls(
            Decimal(raw_leaf["freeDDXBalance"]),
            Decimal(raw_leaf["frozenDDXBalance"]),
            raw_leaf["referralAddress"],
            raw_leaf["payFeesInDDX"],
        )

    def repr_json(self):
        return {
            "freeDDXBalance": str(self.free_ddx_balance),
            "frozenDDXBalance": str(self.frozen_ddx_balance),
            "referralAddress": self.referral_address,
            "payFeesInDDX": self.pay_fees_in_ddx,
        }

    def __repr__(self):
        return f"Trader (state): item_kind = {self.item_kind}; free_ddx_balance = {self.free_ddx_balance}; frozen_ddx_balance = {self.frozen_ddx_balance}; referral_address = {self.referral_address}; pay_fees_in_ddx: {self.pay_fees_in_ddx}"
