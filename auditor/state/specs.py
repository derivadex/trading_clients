"""
Specs module
"""

from eth_abi import encode_single, decode_single
from web3.auto import w3
from typing import Dict

from ddx_client.auditor.state.abi_constant import AbiConstant
from ddx_client.auditor.state.empty import Empty
from ddx_client.auditor.state.item import Item
from ddx_client.auditor.state.item_kinds import ItemKind
from ddx_client.auditor.utils import (
    bytes_array_to_h256,
)


class Specs(Item):
    """
    Defines a Specs
    """

    def __init__(
        self,
        expr: str,
    ):
        """
        Initialize a Specs leaf. A Specs contains information
        pertaining to a market or market gateway's specs.

        Parameters
        ----------
        expr : str
            Specs expr
        """

        self.item_kind = ItemKind.SPECS
        self.expr = expr

    @property
    def abi_encoded_value(self):
        """
        Property returning the custom ABI tokenization of the Specs.
        """

        return encode_single(
            AbiConstant.SPECS,
            [
                [
                    self.item_kind,
                    self.expr,
                ]
            ],
        )

    @property
    def hash_value(self):
        """
        Property returning the keccak256 hash of the encoded Specs.
        """
        return w3.keccak(self.abi_encoded_value)

    @property
    def as_h256(self):
        """
        Property returning the H256 repr of the hash_value of the
        Specs.
        """

        return bytes_array_to_h256(self.hash_value)

    @classmethod
    def abi_decode_value_into_cls(cls, abi_encoded_value):
        """
        ABI Decode an ABI-encoded value from the state snapshot into
        a Specs leaf instance.

        Parameters
        ----------
        abi_encoded_value : str
           Hexstr repr of the ABI-encoded Specs from the
           state snapshot
        """

        ((item_kind, expr),) = decode_single(
            AbiConstant.SPECS,
            w3.toBytes(hexstr=abi_encoded_value),
        )

        return cls(expr)

    @classmethod
    def decode_value_into_cls(cls, raw_leaf: Dict):
        """
        Decode a raw leaf (dict) into a Specs
        instance.

        Parameters
        ----------
        raw_leaf : Dict
            Raw leaf being processed
        """

        if not raw_leaf:
            return Empty()

        return cls(
            raw_leaf["expr"],
        )

    def repr_json(self):
        return {
            "expr": self.expr,
        }

    def __repr__(self):
        return f"Specs (state): item_kind = {self.item_kind}; expr = {self.expr}"
