"""
ItemMessage module
"""

from typing import Dict, List, Any, Optional

from ddx_client.auditor.transactions.event import Event
from ddx_client.client.websocket_message import (
    WebsocketMessageType,
    WebsocketMessage,
    WebsocketEventType,
)


class ItemMessage(WebsocketMessage):
    """
    An ItemMessage class
    """

    def __init__(
        self,
        message_type: Optional[str],
        message_event: WebsocketEventType,
        item_data: [List[Any], Any],
        event_trigger: Event,
    ):
        super().__init__(
            message_type, {"item_data": item_data, "event_trigger": event_trigger}
        )
        self.message_event = message_event

    @classmethod
    def decode_value_into_cls(cls, raw_item_message: Dict):
        """
        Decode a raw item message into class

        Parameters
        ----------
        raw_item_message : Dict
            Raw item message
        """

        return cls(
            raw_item_message["t"],
            raw_item_message["e"],
            raw_item_message["c"]["itemData"],
            raw_item_message["c"]["eventTrigger"],
        )

    def repr_json(self):
        return {
            "t": self.message_type,
            "e": self.message_event,
            "c": {
                "itemData": self.message_content["item_data"],
                "eventTrigger": self.message_content["event_trigger"],
            },
        }

    def __repr__(self):
        return f"ItemMessage: message_type = {self.message_type}; message_event = {self.message_event}; message_content = {self.message_content}"
