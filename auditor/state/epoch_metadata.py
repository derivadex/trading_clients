"""
EpochMetadata module
"""

from eth_abi import encode_single, decode_single
from web3.auto import w3
from typing import Dict

from ddx_client.auditor.state.abi_constant import AbiConstant
from ddx_client.auditor.state.empty import Empty
from ddx_client.auditor.state.item import Item
from ddx_client.auditor.state.item_kinds import ItemKind
from ddx_client.auditor.utils import (
    bytes_array_to_h256,
)


class EpochMetadata(Item):
    """
    Defines an EpochMetadata
    """

    def __init__(
        self,
        next_book_ordinals: Dict,
    ):
        """
        Initialize an EpochMetadata leaf. An EpochMetadata contains information
        pertaining to metadata relevant for epoch transitions.

        Parameters
        ----------
        next_book_ordinals : Dict
           Dictionary ({symbol: next_book_ordinal})
           indicating the next book ordinal by symbol
        """

        self.item_kind = ItemKind.EPOCH_METADATA
        self.next_book_ordinals = next_book_ordinals

    @property
    def abi_encoded_value(self):
        """
        Property returning the custom ABI tokenization of the EpochMetadata.
        """

        sorted_next_book_ordinals = list(zip(*sorted(self.next_book_ordinals.items())))

        # Scale collateral amounts to DDX grains
        return encode_single(
            AbiConstant.EPOCH_METADATA,
            [
                [
                    self.item_kind,
                    [
                        [
                            [
                                len(symbol).to_bytes(1, byteorder="little")
                                + encode_single("bytes32", symbol.encode("utf8"))[:-1]
                                for symbol in sorted_next_book_ordinals[0]
                            ],
                            sorted_next_book_ordinals[1],
                        ]
                    ],
                ]
            ],
        )

    @property
    def hash_value(self):
        """
        Property returning the keccak256 hash of the encoded Strategy.
        """
        return w3.keccak(self.abi_encoded_value)

    @property
    def as_h256(self):
        """
        Property returning the H256 repr of the hash_value of the
        EpochMetadata.
        """

        return bytes_array_to_h256(self.hash_value)

    @classmethod
    def abi_decode_value_into_cls(cls, abi_encoded_value):
        """
        ABI Decode an ABI-encoded value from the state snapshot into
        an EpochMetadata leaf instance.

        Parameters
        ----------
        abi_encoded_value : str
           Hexstr repr of the ABI-encoded EpochMetadata from the
           state snapshot
        """

        ((item_kind, ((symbols, next_book_ordinals),),),) = decode_single(
            AbiConstant.EPOCH_METADATA,
            w3.toBytes(hexstr=abi_encoded_value),
        )

        return cls(
            {
                k[1 : 1 + k[0]].decode("utf8"): v
                for k, v in zip(list(symbols), list(next_book_ordinals))
            },
        )

    @classmethod
    def decode_value_into_cls(cls, raw_leaf: Dict):
        """
        Decode a raw leaf (dict) into an EpochMetadata
        instance.

        Parameters
        ----------
        raw_leaf : Dict
            Raw leaf being processed
        """

        if not raw_leaf:
            return Empty()

        return cls(
            {k: v for k, v in raw_leaf["nextBookOrdinals"].items()},
        )

    def repr_json(self):
        return {
            "nextBookOrdinals": {k: str(v) for k, v in self.next_book_ordinals.items()},
        }

    def __repr__(self):
        return f"EpochMetadata (state): item_kind = {self.item_kind}; next_book_ordinals = {self.next_book_ordinals}"

    @property
    def metrics_repr(self):
        return {
            "nextBookOrdinals": self.next_book_ordinals,
        }
