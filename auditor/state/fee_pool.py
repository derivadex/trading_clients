"""
FeePool module
"""
from typing import Dict
from eth_abi import encode_single, decode_single
from web3.auto import w3

from ddx_client.auditor.state.abi_constant import AbiConstant
from ddx_client.auditor.state.item import Item
from ddx_client.auditor.state.item_kinds import ItemKind
from ddx_client.auditor.utils import (
    to_unit_amount,
    bytes_array_to_h256,
    to_base_unit_amount_list,
)


class FeePool(Item):
    """
    Defines a FeePool
    """

    def __init__(
        self,
        pool: Dict,
    ):
        """
        Initialize an FeePool leaf. Contains fees grouped by tokens.

        Parameters
        ----------
        pool : Dict
           Dictionary ({collateral_address: collateral_value})
           indicating the pool
        """

        self.item_kind = ItemKind.FEE_POOL
        self.pool = pool

    @property
    def abi_encoded_value(self):
        """
        Property returning the custom ABI tokenization.
        """

        # Scale collateral amounts to DDX grains
        return encode_single(
            AbiConstant.FEE_POOL,
            [
                [
                    self.item_kind,
                    [
                        list(self.pool.keys()),
                        to_base_unit_amount_list(list(self.pool.values()), 6),
                    ],
                ]
            ],
        )

    @property
    def hash_value(self):
        """
        Property returning the keccak256 hash.
        """

        return w3.keccak(self.abi_encoded_value)

    @property
    def as_h256(self):
        """
        Property returning the H256 repr of the hash_value.
        """

        return bytes_array_to_h256(self.hash_value)

    @classmethod
    def abi_decode_value_into_cls(cls, abi_encoded_value):
        """
        ABI Decode an ABI-encoded value from the state snapshot into
        a leaf instance.

        Parameters
        ----------
        abi_encoded_value : str
           Hexstr repr of the ABI-encoded FeePool from the
           state snapshot
        """

        ((item_kind, (pool_tokens, pool_amounts,),),) = decode_single(
            AbiConstant.FEE_POOL,
            w3.toBytes(hexstr=abi_encoded_value),
        )

        # Scale collateral amounts from DDX grains
        return cls(
            {
                k: to_unit_amount(v, 6)
                for k, v in zip(list(pool_tokens), list(pool_amounts))
            },
        )

    def __repr__(self):
        return f"FeePool (state): item_kind = {self.item_kind}; pool = {self.pool}"

    @property
    def metrics_repr(self):
        return {
            "pool": self.pool,
        }
