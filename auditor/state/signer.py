"""
Signer module
"""

from eth_abi import encode_single, decode_single
from web3.auto import w3

from ddx_client.auditor.state.abi_constant import AbiConstant
from ddx_client.auditor.state.item_kinds import ItemKind
from ddx_client.auditor.utils import bytes_array_to_h256


class Signer:
    def __init__(self, release_hash: str):
        """
        Initialize a Signer leaf. A Signer contains information
        pertaining to a registered signer.

        Parameters
        ----------
        release_hash : str
           Release hash (mrEnclave, isvSvn)
        """

        self.item_kind = ItemKind.SIGNER
        self.release_hash = release_hash

    @property
    def abi_encoded_value(self):
        """
        Property returning the custom ABI tokenization of the Signer.
        """

        return encode_single(
            AbiConstant.SIGNER,
            [
                self.item_kind,
                [bytes.fromhex(self.release_hash[2:])],
            ],
        )

    @property
    def hash_value(self):
        """
        Property returning the keccak256 hash of the encoded Trader.
        """

        return w3.keccak(self.abi_encoded_value)

    @property
    def as_h256(self):
        """
        Property returning the H256 repr of the hash_value of the
        Trader.
        """

        return bytes_array_to_h256(self.hash_value)

    @classmethod
    def abi_decode_value_into_cls(cls, abi_encoded_value):
        """
        ABI Decode an ABI-encoded value from the state snapshot into
        a Signer leaf instance.

        Parameters
        ----------
        abi_encoded_value : str
           Hexstr repr of the ABI-encoded Signer leaf from the
           state snapshot
        """

        (item_kind, (release_hash,),) = decode_single(
            AbiConstant.SIGNER,
            w3.toBytes(hexstr=abi_encoded_value),
        )

        return cls(f"0x{release_hash.hex()}")
