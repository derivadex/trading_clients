"""
Stats module
"""

from ddx_python.decimal import Decimal
from eth_abi.utils.padding import zpad32_right
from web3.auto import w3
from eth_abi import encode_single, decode_single

from ddx_client.auditor.state.abi_constant import AbiConstant
from ddx_client.auditor.state.item import Item
from ddx_client.auditor.state.item_kinds import ItemKind
from ddx_client.auditor.utils import (
    to_unit_amount,
    bytes_array_to_h256,
    to_base_unit_amount,
)


class Stats(Item):
    """
    Defines a Stats
    """

    def __init__(
        self,
        maker_volume: Decimal,
        taker_volume: Decimal,
    ):
        """
        Initialize a Stats leaf. Stats contains data such as volume
        info for trade mining.

        Parameters
        ----------
        maker_volume: Decimal
            Maker volume
        taker_volume: Decimal
            Taker volume
        """

        self.item_kind = ItemKind.STATS
        self.maker_volume = maker_volume
        self.taker_volume = taker_volume

    @property
    def abi_encoded_value(self):
        """
        Property returning the custom ABI tokenization of the Strategy.
        """

        # Scale volume amounts to DDX grains
        return encode_single(
            AbiConstant.STATS,
            [
                self.item_kind,
                [
                    to_base_unit_amount(self.maker_volume, 6),
                    to_base_unit_amount(self.taker_volume, 6),
                ],
            ],
        )

    @property
    def hash_value(self):
        """
        Property returning the keccak256 hash of the encoded Stats.
        """
        return w3.keccak(self.abi_encoded_value)

    @property
    def as_h256(self):
        """
        Property returning the H256 repr of the hash_value of the
        Stats.
        """

        return bytes_array_to_h256(self.hash_value)

    @classmethod
    def abi_decode_value_into_cls(cls, abi_encoded_value):
        """
        ABI Decode an ABI-encoded value from the state snapshot into
        a Stats leaf instance.

        Parameters
        ----------
        abi_encoded_value : str
           Hexstr repr of the ABI-encoded Stats from the
           state snapshot
        """

        (item_kind, (maker_volume, taker_volume)) = decode_single(
            AbiConstant.STATS,
            w3.toBytes(hexstr=abi_encoded_value),
        )

        # Scale volumes from DDX grains
        return cls(
            to_unit_amount(maker_volume, 6),
            to_unit_amount(taker_volume, 6),
        )

    def __repr__(self):
        return f"Stats (state): item_kind = {self.item_kind}; maker_volume = {self.maker_volume}; taker_volume = {self.taker_volume}"
