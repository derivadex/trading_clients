"""
Position module
"""
from typing import Dict, List

from web3.auto import w3
from ddx_python.decimal import Decimal
from eth_abi import encode_single, decode_single

from ddx_client.auditor.state.abi_constant import AbiConstant
from ddx_client.auditor.state.empty import Empty
from ddx_client.auditor.state.identifiers.strategy_identifier import (
    StrategyIdentifier,
)
from ddx_client.auditor.state.item import Item
from ddx_client.auditor.state.item_kinds import ItemKind
from ddx_client.auditor.state.strategy import Strategy
from ddx_client.auditor.utils import (
    to_base_unit_amount,
    to_unit_amount,
    bytes_array_to_h256,
    pack_bytes,
    unpack_bytes,
    position_side_to_int,
    position_int_to_side,
)


class Position(Item):
    """
    Defines a Position
    """

    def __init__(
        self,
        side: str,
        balance: Decimal,
        avg_entry_price: Decimal,
    ):
        """
        Initialize a Position leaf. A Position contains information
        pertaining to an open position.

        Parameters
        ----------
        side : str
           Side of the position ('Long', 'Short')
        balance: Decimal
           Size of position
        avg_entry_price: Decimal
           Average entry price for the position
        """

        self.item_kind = ItemKind.POSITION
        self.side = side
        self.balance = balance
        self.avg_entry_price = avg_entry_price

    @property
    def abi_encoded_value(self):
        """
        Property returning the custom ABI tokenization of the Position.
        """

        # Scale balance and average entry price to DDX grains
        return encode_single(
            AbiConstant.POSITION,
            [
                self.item_kind,
                [
                    position_side_to_int(self.side),
                    to_base_unit_amount(self.balance, 6),
                    to_base_unit_amount(self.avg_entry_price, 6),
                ],
            ],
        )

    @property
    def hash_value(self):
        """
        Property returning the keccak256 hash of the encoded Position.
        """

        return w3.keccak(self.abi_encoded_value)

    @property
    def as_h256(self):
        """
        Property returning the H256 repr of the hash_value of the
        Position.
        """

        return bytes_array_to_h256(self.hash_value)

    @classmethod
    def abi_decode_value_into_cls(cls, abi_encoded_value):
        """
        ABI Decode an ABI-encoded value from the state snapshot into
        a Position leaf instance.

        Parameters
        ----------
        abi_encoded_value : str
           Hexstr repr of the ABI-encoded Position leaf from the
           state snapshot
        """

        (item_kind, (side, balance, avg_entry_price)) = decode_single(
            AbiConstant.POSITION,
            w3.toBytes(hexstr=abi_encoded_value),
        )

        # Scale balance and average entry price from DDX grains
        return cls(
            position_int_to_side(side),
            to_unit_amount(balance, 6),
            to_unit_amount(avg_entry_price, 6),
        )

    @classmethod
    def decode_value_into_cls(cls, raw_leaf: Dict):
        """
        Decode a raw leaf (dict) into a Position
        instance.

        Parameters
        ----------
        raw_leaf : Dict
            Raw leaf being processed
        """

        if not raw_leaf:
            return Empty()

        return cls(
            raw_leaf["side"],
            Decimal(raw_leaf["balance"]),
            Decimal(raw_leaf["avgEntryPrice"]),
        )

    def repr_json(self):
        return {
            "side": self.side,
            "balance": str(self.balance),
            "avgEntryPrice": str(self.avg_entry_price),
        }

    def __repr__(self):
        return f"Position (state): item_kind = {self.item_kind}; side = {self.side}; balance = {self.balance}; avg_entry_price: {self.avg_entry_price}"

    @property
    def metrics_repr(self):
        return {
            "side": self.side,
            "balance": self.balance,
            "avgEntryPrice": self.avg_entry_price,
        }
