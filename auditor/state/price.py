"""
Price module
"""

from eth_abi import encode_single, decode_single
from web3.auto import w3
from ddx_python.decimal import Decimal

from ddx_client.auditor.state.abi_constant import AbiConstant
from ddx_client.auditor.state.item import Item
from ddx_client.auditor.state.item_kinds import ItemKind
from ddx_client.auditor.utils import (
    to_base_unit_amount,
    to_unit_amount,
    bytes_array_to_h256,
    round_to_unit,
)


class Price(Item):
    """
    Defines a Price
    """

    def __init__(self, index_price: Decimal, ema: Decimal, ordinal: int):
        """
        Initialize a Price leaf. A Price contains information
        pertaining to a market's price checkpoint.

        Parameters
        ----------
        index_price : Decimal
           Composite index price for the market
        ema: Decimal
           EMA component to the price information, when combined with
           the index price, can arrive at the market's mark price
        ordinal: int
           Ordinal
        """

        self.item_kind = ItemKind.PRICE
        self.index_price = index_price
        self.ema = ema
        self.ordinal = ordinal

    @property
    def abi_encoded_value(self):
        """
        Property returning the custom ABI tokenization of the Price.
        """

        # Scale index price and ema to DDX grains
        price_encoding = encode_single(
            AbiConstant.PRICE,
            [
                self.item_kind,
                [
                    to_base_unit_amount(self.index_price, 6),
                    to_base_unit_amount(abs(self.ema), 6),
                    self.ordinal,
                ],
            ],
        )
        if self.ema < 0:
            price_encoding_byte_array = bytearray(price_encoding)
            price_encoding_byte_array[-49] = 1
            price_encoding = bytes(price_encoding_byte_array)

        return price_encoding

    @property
    def hash_value(self):
        """
        Property returning the keccak256 hash of the encoded Price.
        """

        return w3.keccak(self.abi_encoded_value)

    @property
    def as_h256(self):
        """
        Property returning the H256 repr of the hash_value of the
        Price.
        """

        return bytes_array_to_h256(self.hash_value)

    @classmethod
    def abi_decode_value_into_cls(cls, abi_encoded_value):
        """
        ABI Decode an ABI-encoded value from the state snapshot into
        a Price leaf instance.

        Parameters
        ----------
        abi_encoded_value : str
           Hexstr repr of the ABI-encoded Price leaf from the
           state snapshot
        """

        price_encoding_byte_array = bytearray(bytes.fromhex(abi_encoded_value[2:]))
        multiplier = -1 if price_encoding_byte_array[-49] == 1 else 1
        price_encoding_byte_array[-49] = 0
        abi_encoded_value = bytes(price_encoding_byte_array).hex()

        (item_kind, (index_price, ema, ordinal)) = decode_single(
            AbiConstant.PRICE,
            w3.toBytes(hexstr=abi_encoded_value),
        )

        # Scale index price and ema from DDX grains
        return cls(
            to_unit_amount(index_price, 6),
            to_unit_amount(ema * multiplier, 6),
            ordinal,
        )

    @property
    def mark_price(self):
        return round_to_unit(
            min(
                self.index_price * Decimal("1.005"),
                max(self.index_price * Decimal("0.995"), self.index_price + self.ema),
            )
        )

    def __repr__(self):
        return f"Price (state): item_kind = {self.item_kind}; index_price = {self.index_price}; ema: {self.ema}; ordinal: {self.ordinal}"
