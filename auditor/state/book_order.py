"""
BookOrder module
"""

from typing import Dict
from eth_abi import encode_single, decode_single
from web3.auto import w3
from ddx_python.decimal import Decimal

from ddx_client.auditor.state.abi_constant import AbiConstant
from ddx_client.auditor.state.empty import Empty
from ddx_client.auditor.state.item import Item
from ddx_client.auditor.state.item_kinds import ItemKind
from ddx_client.auditor.utils import (
    to_base_unit_amount,
    to_unit_amount,
    bytes_array_to_h256,
    book_order_side_to_int,
    book_order_int_to_side,
)


class BookOrder(Item):
    """
    Defines a BookOrder
    """

    def __init__(
        self,
        side: str,
        amount: Decimal,
        price: Decimal,
        trader_address: str,
        strategy_id_hash: str,
        book_ordinal: int,
        time_value: int,
    ):
        """
        Initialize a BookOrder leaf. A BookOrder contains information
        pertaining to a maker order in the order book.

        Parameters
        ----------
        side : str
           Side of the order ('Bid', 'Ask')
        amount: Decimal
           Amount/size of order
        price: Decimal
           Price the order has been placed at
        trader_address: str
           The order creator's Ethereum address prefixed with the
           blockchain discriminant
        strategy_id_hash: str
           First 4 bytes of strategy ID hash this order belongs to
        book_ordinal: int
           Book ordinal
        time_value: int
           Time value
        """

        self.item_kind = ItemKind.BOOK_ORDER
        self.side = side
        self.amount = amount
        self.price = price
        self.trader_address = trader_address
        self.strategy_id_hash = strategy_id_hash
        self.book_ordinal = book_ordinal
        self.time_value = time_value

    @property
    def abi_encoded_value(self):
        """
        Property returning the custom ABI tokenization of the BookOrder.
        """

        return encode_single(
            AbiConstant.BOOK_ORDER,
            [
                self.item_kind,
                [
                    book_order_side_to_int(self.side),
                    to_base_unit_amount(self.amount, 6),
                    to_base_unit_amount(self.price, 6),
                    bytes.fromhex(self.trader_address[2:]),
                    bytes.fromhex(self.strategy_id_hash[2:]),
                    self.book_ordinal,
                    self.time_value,
                ],
            ],
        )

    @property
    def hash_value(self):
        """
        Property returning the keccak256 hash of the encoded BookOrder.
        """

        return w3.keccak(self.abi_encoded_value)

    @property
    def as_h256(self):
        """
        Property returning the H256 repr of the hash_value of the
        BookOrder.
        """

        return bytes_array_to_h256(self.hash_value)

    @classmethod
    def abi_decode_value_into_cls(cls, abi_encoded_value):
        """
        ABI Decode an ABI-encoded value from the state snapshot into
        a BookOrder leaf instance.

        Parameters
        ----------
        abi_encoded_value : str
           Hexstr repr of the ABI-encoded BookOrder leaf from the
           state snapshot
        """

        (
            item_kind,
            (
                side,
                amount,
                price,
                trader_address,
                strategy_id_hash,
                book_ordinal,
                time_value,
            ),
        ) = decode_single(
            AbiConstant.BOOK_ORDER,
            w3.toBytes(hexstr=abi_encoded_value),
        )

        return cls(
            book_order_int_to_side(side),
            to_unit_amount(amount, 6),
            to_unit_amount(price, 6),
            f"0x{trader_address.hex()}",
            f"0x{strategy_id_hash[:4].hex()}",
            book_ordinal,
            time_value,
        )

    @classmethod
    def decode_value_into_cls(cls, raw_leaf: Dict):
        """
        Decode a raw leaf (dict) into a BookOrder
        instance.

        Parameters
        ----------
        raw_leaf : Dict
            Raw leaf being processed
        """

        if not raw_leaf:
            return Empty()

        return cls(
            raw_leaf["side"],
            Decimal(raw_leaf["amount"]),
            Decimal(raw_leaf["price"]),
            raw_leaf["traderAddress"],
            raw_leaf["strategyIdHash"],
            raw_leaf["bookOrdinal"],
            raw_leaf["timeValue"],
        )

    def repr_json(self):
        return {
            "side": self.side,
            "amount": str(self.amount),
            "price": str(self.price),
            "traderAddress": self.trader_address,
            "strategyIdHash": self.strategy_id_hash,
            "bookOrdinal": self.book_ordinal,
            "timeValue": self.time_value,
        }

    def __repr__(self):
        return f"BookOrder (state): item_kind = {self.item_kind}; side = {self.side}; amount = {self.amount}; price: {self.price}; trader_address: {self.trader_address}; strategy_id_hash: {self.strategy_id_hash}; book_ordinal: {self.book_ordinal}, time_value: {self.time_value}"
