"""
RegisteredMember module
"""
from typing import Dict
from web3.auto import w3
from eth_abi import encode_single, decode_single

from trading_clients.auditor.state.abi_constant import AbiConstant
from trading_clients.auditor.state.empty import Empty
from trading_clients.auditor.state.item import Item
from trading_clients.auditor.state.item_types import ItemType
from trading_clients.auditor.utils import (
    bytes_array_to_h256,
    member_want_role_to_int,
    member_int_to_want_role,
)


class RegisteredMember(Item):
    """
    Defines a RegisteredMember
    """

    def __init__(self, node_id: int, signing_address: str, url: str, want_role: str):
        """
        Initialize a RegisteredMember leaf. A RegisteredMember contains
        information pertaining to registered operator nodes on the
        network.

        Parameters
        ----------
        node_id : int
           The operator's unique identifier within the Raft
        signing_address: str
           The public key derived address required to validate all
           signatures for this operator
        url: str
           The operator's rpc server url
        want_role: str
           The intended role (not necessarily current role) of this node
           within the Raft
        """

        self.item_type = ItemType.MEMBER
        self.node_id = node_id
        self.signing_address = signing_address
        self.url = url
        self.want_role = want_role

    @property
    def abi_encoded_value(self):
        """
        Property returning the custom ABI tokenization of the
        RegisteredMember.
        """

        return encode_single(
            AbiConstant.REGISTERED_MEMBER,
            [
                [
                    self.item_type,
                    [
                        self.node_id,
                        self.signing_address,
                        self.url.encode("utf8"),
                        member_want_role_to_int(self.want_role),
                    ],
                ]
            ],
        )

    @property
    def hash_value(self):
        """
        Property returning the keccak256 hash of the encoded Registered
        Member.
        """

        return w3.keccak(self.abi_encoded_value)

    @property
    def as_h256(self):
        """
        Property returning the H256 repr of the hash_value of the
        RegisteredMember.
        """

        return bytes_array_to_h256(self.hash_value)

    @classmethod
    def abi_decode_value_into_cls(cls, abi_encoded_value):
        """
        ABI Decode an ABI-encoded value from the state snapshot into
        a RegisteredMember leaf instance.

        Parameters
        ----------
        abi_encoded_value : str
           Hexstr repr of the ABI-encoded RegisteredMember leaf from the
           state snapshot
        """

        ((item_type, (node_id, signing_address, url, want_role,),),) = decode_single(
            AbiConstant.REGISTERED_MEMBER,
            w3.toBytes(hexstr=abi_encoded_value),
        )

        return cls(
            node_id,
            signing_address,
            url.decode("utf8"),
            member_int_to_want_role(want_role),
        )

    @classmethod
    def decode_value_into_cls(cls, raw_leaf: Dict):
        """
        Decode a raw leaf (dict) into a RegisteredMember
        instance.

        Parameters
        ----------
        raw_leaf : Dict
            Raw leaf being processed
        """

        if not raw_leaf:
            return Empty()

        return cls(
            raw_leaf["nodeId"],
            raw_leaf["signingAddress"],
            raw_leaf["rpcUrlBytes"],
            raw_leaf["wantRole"],
        )

    def repr_json(self):
        return {
            "nodeId": self.node_id,
            "signingAddress": self.signing_address,
            "url": self.url,
            "wantRole": self.want_role,
        }

    def __repr__(self):
        return f"RegisteredMember (state): item_type = {self.item_type}; node_id = {self.node_id}; signing_address = {self.signing_address}; url: {self.url}; want_role: {self.want_role}"
