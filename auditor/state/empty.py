"""
Empty module
"""
from ddx_client.auditor.smt.h256 import H256
from ddx_client.auditor.state.item import Item
from ddx_client.auditor.state.item_kinds import ItemKind


class Empty(Item):
    """
    Defines Empty
    """

    def __init__(self):
        """
        Initialize an Empty leaf. An Empty leaf is input to the SMT in
        scenarios where a BookOrder now has nothing left or a Position
        leaf has 0 balance.
        """

        self.item_kind = ItemKind.EMPTY

    @property
    def abi_encoded_value(self):
        return

    @property
    def hash_value(self):
        return

    @property
    def as_h256(self):
        """
        Property returning the H256 repr of the hash_value of the
        Empty.
        """

        return H256.zero()

    @classmethod
    def abi_decode_value_into_cls(cls, abi_encoded_value):
        pass

    def repr_json(self):
        return {}

    def __repr__(self):
        return f"Empty (state): item_kind = {self.item_kind}"
