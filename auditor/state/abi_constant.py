"""
Abi Constants module
"""

from enum import Enum


class AbiConstant(str, Enum):
    TRADER = "(uint8,(uint128,uint128,address,bool))"
    STRATEGY = (
        "((uint8,(bytes32,(address[],uint256[]),(address[],uint128[]),uint64,bool)))"
    )
    POSITION = "(uint8,(uint8,uint128,uint128))"
    BOOK_ORDER = "(uint8,(uint8,uint128,uint128,bytes21,bytes32,uint64,uint64))"
    PRICE = "(uint8,(uint128,uint256,uint64))"
    ORGANIC_INSURANCE_FUND = "((uint8,(address[],uint128[])))"
    STATS = "(uint8,(uint128,uint128))"
    REGISTERED_MEMBER = "((uint8,(uint64,address,bytes,uint8)))"
    SPECS = "((uint8,string))"
    INSURANCE_FUND_CONTRIBUTION = (
        "((uint8,((address[],uint256[]),(address[],uint128[]))))"
    )
    FEE_POOL = "((uint8,(address[],uint128[])))"
    SIGNER = "(uint8,(bytes32))"
    EPOCH_METADATA = "((uint8,((bytes32[],uint64[]))))"
