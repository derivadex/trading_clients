"""
Price module
"""
from typing import Dict
from eth_abi import encode_single, decode_single
from web3.auto import w3

from ddx_client.auditor.state.abi_constant import AbiConstant
from ddx_client.auditor.state.item import Item
from ddx_client.auditor.state.item_kinds import ItemKind
from ddx_client.auditor.utils import (
    to_unit_amount,
    bytes_array_to_h256,
    to_base_unit_amount_list,
)


class InsuranceFundContribution(Item):
    """
    Defines a InsuranceFundContribution
    """

    def __init__(
        self,
        free_balance: Dict,
        frozen_balance: Dict,
    ):
        """
        Initialize an InsuranceFundContribution leaf. An
        InsuranceFundContribution contains information pertaining to the
        a contribution to the insurance fund.

        Parameters
        ----------
        free_balance : Dict
           Dictionary ({collateral_address: collateral_value})
           indicating the free contribution collateral
        frozen_balance : Dict
           Dictionary ({collateral_address: collateral_value})
           indicating the frozen contribution collateral
        """

        self.item_kind = ItemKind.INSURANCE_FUND_CONTRIBUTION
        self.free_balance = free_balance
        self.frozen_balance = frozen_balance

    @property
    def abi_encoded_value(self):
        """
        Property returning the custom ABI tokenization of the
        InsuranceFundContribution.
        """

        # Scale collateral amounts to DDX grains
        return encode_single(
            AbiConstant.INSURANCE_FUND_CONTRIBUTION,
            [
                [
                    self.item_kind,
                    [
                        [
                            list(self.free_balance.keys()),
                            to_base_unit_amount_list(
                                list(self.free_balance.values()), 6
                            ),
                        ],
                        [
                            list(self.frozen_balance.keys()),
                            to_base_unit_amount_list(
                                list(self.frozen_balance.values()), 6
                            ),
                        ],
                    ],
                ]
            ],
        )

    @property
    def hash_value(self):
        """
        Property returning the keccak256 hash of the encoded
        InsuranceFundContribution.
        """

        return w3.keccak(self.abi_encoded_value)

    @property
    def as_h256(self):
        """
        Property returning the H256 repr of the hash_value of the
        InsuranceFundContribution.
        """

        return bytes_array_to_h256(self.hash_value)

    @classmethod
    def abi_decode_value_into_cls(cls, abi_encoded_value):
        """
        ABI Decode an ABI-encoded value from the state snapshot into
        an InsuranceFundContribution leaf instance.

        Parameters
        ----------
        abi_encoded_value : str
           Hexstr repr of the ABI-encoded InsuranceFundContribution from
           the state snapshot
        """

        (
            (
                item_kind,
                (
                    (free_balance_tokens, free_balance_amounts),
                    (frozen_balance_tokens, frozen_balance_amounts),
                ),
            ),
        ) = decode_single(
            AbiConstant.INSURANCE_FUND_CONTRIBUTION,
            w3.toBytes(hexstr=abi_encoded_value),
        )

        # Scale collateral amounts from DDX grains
        return cls(
            {
                k: to_unit_amount(v, 6)
                for k, v in zip(list(free_balance_tokens), list(free_balance_amounts))
            },
            {
                k: to_unit_amount(v, 6)
                for k, v in zip(
                    list(frozen_balance_tokens), list(frozen_balance_amounts)
                )
            },
        )

    def __repr__(self):
        return f"InsuranceFundContribution (state): item_kind = {self.item_kind}; free_balance = {self.free_balance}; frozen_balance = {self.frozen_balance}"

    @property
    def metrics_repr(self):
        return {
            "free_balance": self.free_balance,
            "frozen_balance": self.frozen_balance,
        }
