"""
EpochMetadataIdentifier module
"""

from eth_abi import encode_single
from eth_abi.utils.padding import zpad32_right
from web3.auto import w3

from ddx_client.auditor.shared.identifier import Identifier
from ddx_client.auditor.shared.identifier_types import IdentifierTypes
from ddx_client.auditor.smt.h256 import H256
from ddx_client.auditor.state.item_kinds import ItemKind
from ddx_client.auditor.utils import is_valid_none_list, bytes_array_to_h256


class EpochMetadataIdentifier(Identifier):
    """
    Defines an EpochMetadataIdentifier.
    """

    def __init__(self, epoch_id: int):
        """
        Initialize a EpochMetadataIdentifier. Identifiers are used for a
        variety of purposes, most importantly to store identifying
        information for a particular leaf in the DerivaDEX SMT, or for
        a subset of leaves. This wrapper makes it convenient to generate
        topic strings for API subscriptions and key encodings.

        Parameters
        ----------
        epoch_id : int
            Epoch id for metadata
        """

        # Ensure that the list is valid
        if not is_valid_none_list([epoch_id]):
            raise Exception("Invalid EpochMetadataIdentifier generation")

        self.epoch_id = epoch_id

    def encompasses_identifier(self, identifier) -> bool:
        """
        Check whether this Identifier encompasses / is a
        superset of another StrategyIdentifier.

        Parameters
        ----------
        identifier : EpochMetadataIdentifier
            Another EpochMetadataIdentifier to compare against
        """

        if type(identifier).__name__ != "EpochMetadataIdentifier":
            # If the identifier we are comparing with isn't even of the
            # same type
            return False

        # Check varying degrees of encompassing depending on how
        # granular this Identifier is
        if self.epoch_id is not None:
            return self.epoch_id == identifier.epoch_id
        return True

    @property
    def topic_string(self) -> str:
        """
        Derive topic string for Identifier.
        """

        return f"{'/'.join(filter(None, ['STATE', IdentifierTypes.EPOCH_METADATA, str(self.epoch_id)]))}/"

    @classmethod
    def decode_topic_string_into_cls(cls, topic: str):
        """
        Decode a topic string into an EpochMetadataIdentifier.

        Parameters
        ----------
        topic : str
            Topic string
        """

        topic_parts = topic.split("/")[:-1]
        if topic_parts[1] != IdentifierTypes.EPOCH_METADATA:
            raise Exception(
                f"Invalid topic: must be of {IdentifierTypes.EPOCH_METADATA} type"
            )

        def safe_list_get(l, idx, default):
            try:
                return l[idx]
            except IndexError:
                return default

        epoch_id = safe_list_get(topic_parts, 2, None)
        return cls(epoch_id)

    @classmethod
    def decode_key_into_cls(cls, leaf_key: bytes, leaf_value=None):
        """
        Decode a strategy key into an EpochMetadataIdentifier.

        Parameters
        ----------
        leaf_key : bytes
            EpochMetadata leaf key
        leaf_value
            Leaf value
        """

        epoch_id = int.from_bytes(leaf_key[1:9], "big")

        return cls(epoch_id)

    @property
    def encoded_key(self) -> bytes:
        """
        Derive encoded key (bytes representation), which may be a full
        32-byte value if fully-granular, or a prefix to help capture
        a subset of leaves.
        """

        if self.epoch_id is not None:
            return zpad32_right(
                ItemKind.EPOCH_METADATA.to_bytes(1, byteorder="little")
                + self.epoch_id.to_bytes(8, byteorder="big")
            )
        return ItemKind.EPOCH_METADATA.to_bytes(1, byteorder="little")

    @property
    def as_h256(self) -> H256:
        """
        Derive an H256 representation of the encoded key, which can
        only be done in the case where the Identifier is fully-granular.
        """

        encoded_key = self.encoded_key
        if len(encoded_key) != 32:
            raise Exception("Must be 32-bytes to convert to H256")
        return bytes_array_to_h256(encoded_key)

    @property
    def is_max_granular_key(self):
        return not None in [self.epoch_id]
