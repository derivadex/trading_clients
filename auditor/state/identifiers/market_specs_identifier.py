"""
MarketSpecsIdentifier module
"""
from eth_abi.utils.padding import zpad32_right

from ddx_client.auditor.shared.identifier import Identifier
from ddx_client.auditor.shared.identifier_types import IdentifierTypes
from ddx_client.auditor.smt.h256 import H256
from ddx_client.auditor.state.item_kinds import ItemKind
from ddx_client.auditor.utils import (
    is_valid_none_list,
    bytes_array_to_h256,
    unpack_bytes,
    pack_bytes,
)


class MarketSpecsIdentifier(Identifier):
    """
    Defines an MarketSpecsIdentifier.
    """

    def __init__(self, symbol: str):
        """
        Initialize a MarketSpecsIdentifier. Identifiers are used for a
        variety of purposes, most importantly to store identifying
        information for a particular leaf in the DerivaDEX SMT, or for
        a subset of leaves. This wrapper makes it convenient to generate
        topic strings for API subscriptions and key encodings.

        Parameters
        ----------
        symbol : str
            Market symbol
        """

        # Ensure that the list is valid
        if not is_valid_none_list([symbol]):
            raise Exception("Invalid MarketSpecsIdentifier generation")

        self.symbol = symbol

    def encompasses_identifier(self, identifier) -> bool:
        """
        Check whether this Identifier encompasses / is a
        superset of another MarketSpecsIdentifier.

        Parameters
        ----------
        identifier : MarketSpecsIdentifier
            Another MarketSpecsIdentifier to compare against
        """

        if type(identifier).__name__ != "MarketSpecsIdentifier":
            # If the identifier we are comparing with isn't even of the
            # same type
            return False

        # Check varying degrees of encompassing depending on how
        # granular this Identifier is
        if self.symbol is not None:
            return self.symbol == identifier.symbol
        return True

    @property
    def topic_string(self) -> str:
        """
        Derive topic string for Identifier.
        """

        return f"{'/'.join(filter(None, ['STATE', IdentifierTypes.MARKET_SPECS, self.symbol]))}/"

    @classmethod
    def decode_topic_string_into_cls(cls, topic: str):
        """
        Decode a topic string into a MarketSpecsIdentifier.

        Parameters
        ----------
        topic : str
            Topic string
        """

        topic_parts = topic.split("/")[:-1]
        if topic_parts[1] != IdentifierTypes.MARKET_SPECS:
            raise Exception(
                f"Invalid topic: must be of {IdentifierTypes.MARKET_SPECS} type"
            )

        def safe_list_get(l, idx, default):
            try:
                return l[idx]
            except IndexError:
                return default

        return cls(
            safe_list_get(topic_parts, 2, None),
        )

    @classmethod
    def decode_key_into_cls(cls, leaf_key: bytes, leaf_value=None):
        """
        Decode a MarketSpecs key into a MarketSpecsIdentifier.

        Parameters
        ----------
        leaf_key : bytes
            MarketSpecs leaf key
        leaf_value
            Leaf value
        """

        symbol = unpack_bytes(leaf_key[1:7])

        return cls(symbol)

    @property
    def encoded_key(self) -> bytes:
        """
        Derive encoded key (bytes representation), which may be a full
        32-byte value if fully-granular, or a prefix to help capture
        a subset of leaves.
        """

        if self.symbol is not None:
            return zpad32_right(
                ItemKind.MARKET_SPECS.to_bytes(1, byteorder="little")
                + pack_bytes(self.symbol)
            )
        return ItemKind.MARKET_SPECS.to_bytes(1, byteorder="little")

    @property
    def as_h256(self) -> H256:
        """
        Derive an H256 representation of the encoded key, which can
        only be done in the case where the Identifier is fully-granular.
        """

        encoded_key = self.encoded_key
        if len(encoded_key) != 32:
            raise Exception("Must be 32-bytes to convert to H256")
        return bytes_array_to_h256(encoded_key)

    @property
    def is_max_granular_key(self):
        return not None in [
            self.symbol,
        ]
