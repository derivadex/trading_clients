"""
BookOrderIdentifier module
"""

from typing import Optional

from ddx_client.auditor.shared.identifier import Identifier
from ddx_client.auditor.shared.identifier_types import IdentifierTypes
from ddx_client.auditor.state.item_kinds import ItemKind
from ddx_client.auditor.utils import is_valid_none_list, pack_bytes, unpack_bytes


class BookOrderIdentifier(Identifier):
    """
    Defines an BookOrderIdentifier.
    """

    def __init__(
        self,
        symbol: Optional[str],
        order_hash: Optional[str],
        trader_address: Optional[str],
        abbrev_strategy_id_hash: Optional[str],
    ):
        """
        Initialize a BookOrderIdentifier. Identifiers are used for a
        variety of purposes, most importantly to store identifying
        information for a particular leaf in the DerivaDEX SMT, or for
        a subset of leaves. This wrapper makes it convenient to generate
        topic strings for API subscriptions and key encodings.

        Parameters
        ----------
        symbol : str
            Market symbol
        order_hash : str
            First 25 bytes of order's unique hash
        trader_address : str
            Trader's Ethereum address
        abbrev_strategy_id_hash : str
            First 4 bytes of the hash of the trader's strategy ID
        """

        self.symbol = symbol
        self.order_hash = order_hash
        self.trader_address = trader_address
        self.abbrev_strategy_id_hash = abbrev_strategy_id_hash

    def encompasses_identifier(self, identifier) -> bool:
        """
        Check whether this Identifier encompasses / is a
        superset of another BookOrderIdentifier.

        Parameters
        ----------
        identifier : BookOrderIdentifier
            Another BookOrderIdentifier to compare against
        """

        if type(identifier).__name__ != "BookOrderIdentifier":
            # If the identifier we are comparing with isn't even of the
            # same type
            return False

        # Check varying degrees of encompassing depending on how
        # granular this Identifier is
        if self.abbrev_strategy_id_hash is not None:
            return (
                self.symbol == identifier.symbol
                and self.trader_address == identifier.trader_address
                and self.abbrev_strategy_id_hash == identifier.abbrev_strategy_id_hash
            )
        elif self.trader_address is not None:
            return (
                self.symbol == identifier.symbol
                and self.trader_address == identifier.trader_address
            )
        elif self.symbol is not None:
            return self.symbol == identifier.symbol
        return True

    @property
    def topic_string(self):
        """
        Derive topic string for Identifier.
        """

        return f"{'/'.join(filter(None, ['STATE', IdentifierTypes.BOOK_ORDER, self.symbol, self.trader_address, self.abbrev_strategy_id_hash, self.order_hash]))}/"

    @classmethod
    def decode_topic_string_into_cls(cls, topic: str):
        """
        Decode a topic string into a BookOrderIdentifier.

        Parameters
        ----------
        topic : str
            Topic string
        """

        topic_parts = topic.split("/")[:-1]
        if topic_parts[1] != IdentifierTypes.BOOK_ORDER:
            raise Exception(
                f"Invalid topic: must be of {IdentifierTypes.BOOK_ORDER} type"
            )

        def safe_list_get(l, idx, default):
            try:
                return l[idx]
            except IndexError:
                return default

        trader_address = safe_list_get(topic_parts, 3, None)
        return cls(
            safe_list_get(topic_parts, 2, None),
            safe_list_get(topic_parts, 5, None),
            trader_address.lower() if trader_address is not None else trader_address,
            safe_list_get(topic_parts, 4, None),
        )

    @classmethod
    def decode_key_into_cls(cls, leaf_key: bytes, leaf_value=None):
        """
        Decode a book order key and leaf into a BookOrderIdentifier.

        Parameters
        ----------
        leaf_key : bytes
            BookOrder leaf key
        leaf_value
            Leaf value
        """

        symbol = unpack_bytes(leaf_key[1:7])
        order_hash = f"0x{leaf_key[7:].hex()}"

        return cls(
            symbol,
            order_hash,
            leaf_value.trader_address,
            leaf_value.strategy_id_hash,
        )

    @property
    def encoded_key(self) -> bytes:
        """
        Derive encoded key (bytes representation), which may be a full
        32-byte value if fully-granular, or a prefix to help capture
        a subset of leaves.
        """

        if self.order_hash is not None:
            return (
                ItemKind.BOOK_ORDER.to_bytes(1, byteorder="little")
                + pack_bytes(self.symbol)
                + bytes.fromhex(self.order_hash[2:52])
            )
        elif self.symbol is not None:
            return ItemKind.BOOK_ORDER.to_bytes(1, byteorder="little") + pack_bytes(
                self.symbol
            )
        return ItemKind.BOOK_ORDER.to_bytes(1, byteorder="little")

    @property
    def is_max_granular_key(self):
        return not None in [
            self.symbol,
            self.order_hash,
        ]

    @property
    def is_max_granular(self):
        return not None in [
            self.symbol,
            self.order_hash,
            self.trader_address,
            self.abbrev_strategy_id_hash,
        ]
