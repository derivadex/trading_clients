"""
ReleaseIdentifier module
"""

from eth_abi import encode_single
from eth_abi.utils.padding import zpad32_right
from web3.auto import w3

from ddx_client.auditor.shared.identifier import Identifier
from ddx_client.auditor.shared.identifier_types import IdentifierTypes
from ddx_client.auditor.smt.h256 import H256
from ddx_client.auditor.state.item_kinds import ItemKind
from ddx_client.auditor.utils import is_valid_none_list, bytes_array_to_h256


class ReleaseIdentifier(Identifier):
    """
    Defines an ReleaseIdentifier.
    """

    def __init__(self, release_hash: str):
        """
        Initialize a ReleaseIdentifier. Identifiers are used for a
        variety of purposes, most importantly to store identifying
        information for a particular leaf in the DerivaDEX SMT, or for
        a subset of leaves. This wrapper makes it convenient to generate
        topic strings for API subscriptions and key encodings.

        Parameters
        ----------
        release_hash: str
            enclave's release hash (MR enclave + isvsvn) as a hex string
            full hash is 32 bytes but can also except 31 bytes
        """

        try:
            int(release_hash, 16)
        except ValueError:
            raise Exception("release_hash is not a hex string")

        if len(release_hash[2:]) == 64:
            release_hash = release_hash[:-2]

        if len(release_hash[2:]) != 62:
            raise Exception("invalid release_hash length")

        self.release_hash = release_hash

    def encompasses_identifier(self, identifier) -> bool:
        """
        Check whether this Identifier encompasses / is a
        superset of another ReleaseIdentifier.

        Parameters
        ----------
        identifier : ReleaseIdentifier
            Another ReleaseIdentifier to compare against
        """

        if type(identifier).__name__ != "ReleaseIdentifier":
            # If the identifier we are comparing with isn't even of the
            # same type
            return False

        if self.release_hash is not None:
            return self.release_hash == identifier.release_hash

        return True

    @property
    def topic_string(self) -> str:
        """
        Derive topic string for Identifier.
        """

        return f"{'/'.join(filter(None, ['STATE', IdentifierTypes.RELEASE, self.release_hash]))}/"

    @classmethod
    def decode_topic_string_into_cls(cls, topic: str):
        """
        Decode a topic string into a ReleaseIdentifier.

        Parameters
        ----------
        topic : str
            Topic string
        """

        topic_parts = topic.split("/")[:-1]
        if topic_parts[1] != IdentifierTypes.RELEASE:
            raise Exception(f"Invalid topic: must be of {IdentifierTypes.RELEASE} type")

        def safe_list_get(l, idx, default):
            try:
                return l[idx]
            except IndexError:
                return default

        return cls(safe_list_get(topic_parts, 2, None))

    @classmethod
    def decode_key_into_cls(cls, leaf_key: bytes, leaf_value=None):
        """
        Decode a release key into a ReleaseIdentifier.

        Parameters
        ----------
        leaf_key : bytes
            Strategy leaf key
        leaf_value
            Leaf value
        """
        release_hash = f"0x{leaf_key[1:].hex()}"
        return cls(release_hash)

    @property
    def encoded_key(self) -> bytes:
        """
        Derive encoded key (bytes representation), which may be a full
        32-byte value if fully-granular, or a prefix to help capture
        a subset of leaves.
        """

        return zpad32_right(
            ItemKind.RELEASE.to_bytes(1, byteorder="little")
            + bytes.fromhex(self.release_hash[2:])
        )

    @property
    def as_h256(self) -> H256:
        """
        Derive an H256 representation of the encoded key, which can
        only be done in the case where the Identifier is fully-granular.
        """

        encoded_key = self.encoded_key
        if len(encoded_key) != 32:
            raise Exception("Must be 32-bytes to convert to H256")
        return bytes_array_to_h256(encoded_key)

    @property
    def is_max_granular_key(self):
        return True
