"""
FeePoolIdentifier module
"""
from typing import Optional
from eth_abi.utils.padding import zpad32_right

from ddx_client.auditor.shared.identifier import Identifier
from ddx_client.auditor.shared.identifier_types import IdentifierTypes
from ddx_client.auditor.smt.h256 import H256
from ddx_client.auditor.state.item_kinds import ItemKind
from ddx_client.auditor.utils import bytes_array_to_h256, is_valid_none_list


class FeePoolIdentifier(Identifier):
    """
    Defines an FeePoolIdentifier.
    """

    def __init__(
        self,
        epoch_id: Optional[int],
    ):
        """
        Initialize a FeePoolIdentifier. Identifiers are used for a
        variety of purposes, most importantly to store identifying
        information for a particular leaf in the DerivaDEX SMT, or for
        a subset of leaves. This wrapper makes it convenient to generate
        topic strings for API subscriptions and key encodings.

        Parameters
        ----------
        epoch_id : int
            Epoch id
        """

        # Ensure that the list is valid
        if not is_valid_none_list([epoch_id]):
            raise Exception("Invalid FeePoolIdentifier generation")

        self.epoch_id = epoch_id

    def encompasses_identifier(self, identifier) -> bool:
        """
        Check whether this Identifier encompasses / is a
        superset of another FeePoolIdentifier.

        Parameters
        ----------
        identifier : FeePoolIdentifier
            Another FeePoolIdentifier to compare against
        """

        return type(identifier).__name__ == "FeePoolIdentifier"

    @property
    def topic_string(self):
        """
        Derive topic string for Identifier.
        """

        return f"{'/'.join(filter(None, ['STATE', IdentifierTypes.FEE_POOL, str(self.epoch_id)]))}/"

    @classmethod
    def decode_topic_string_into_cls(cls, topic: str):
        """
        Decode a topic string into a FeePoolIdentifier.

        Parameters
        ----------
        topic : str
            Topic string
        """

        topic_parts = topic.split("/")[:-1]
        if topic_parts[1] != IdentifierTypes.FEE_POOL:
            raise Exception(
                f"Invalid topic: must be of {IdentifierTypes.FEE_POOL} type"
            )

        def safe_list_get(l, idx, default):
            try:
                return l[idx]
            except IndexError:
                return default

        return cls(
            int(safe_list_get(topic_parts, 2, None)),
        )

    @classmethod
    def decode_key_into_cls(cls, leaf_key: bytes, leaf_value=None):
        """
        Decode a fee pool key into a FeePoolIdentifier.

        Parameters
        ----------
        leaf_key : bytes
            FeePool leaf key
        leaf_value
            Leaf value
        """

        return (
            cls(-1)
            if leaf_key[7:8] == b"\x00"
            else cls(int(leaf_key[8:].rstrip(b"\x00").decode("utf8")))
        )

    @property
    def encoded_key(self) -> bytes:
        """
        Derive encoded key (bytes representation), which may be a full
        32-byte value if fully-granular, or a prefix to help capture
        a subset of leaves.
        """

        if self.epoch_id is not None:
            return zpad32_right(
                ItemKind.FEE_POOL.to_bytes(1, byteorder="little")
                + (
                    f"F33800".encode("utf8")
                    if self.epoch_id == -1
                    else f"F33800x{self.epoch_id}".encode("utf8")
                )
            )

        return ItemKind.FEE_POOL.to_bytes(1, byteorder="little")

    @property
    def as_h256(self) -> H256:
        """
        Derive an H256 representation of the encoded key, which can
        only be done in the case where the Identifier is fully-granular.
        """

        encoded_key = self.encoded_key
        if len(encoded_key) != 32:
            raise Exception("Must be 32-bytes to convert to H256")
        return bytes_array_to_h256(encoded_key)

    @property
    def is_max_granular_key(self):
        return True
