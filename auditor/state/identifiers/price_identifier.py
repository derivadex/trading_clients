"""
PriceIdentifier module
"""

from eth_abi.utils.padding import zpad32_right

from ddx_client.auditor.shared.identifier import Identifier
from ddx_client.auditor.shared.identifier_types import IdentifierTypes
from ddx_client.auditor.smt.h256 import H256
from ddx_client.auditor.state.item_kinds import ItemKind
from ddx_client.auditor.utils import (
    is_valid_none_list,
    bytes_array_to_h256,
    unpack_bytes,
    pack_bytes,
)


class PriceIdentifier(Identifier):
    """
    Defines an PriceIdentifier.
    """

    def __init__(self, symbol: str, index_price_hash: str):
        """
        Initialize a PriceIdentifier. Identifiers are used for a
        variety of purposes, most importantly to store identifying
        information for a particular leaf in the DerivaDEX SMT, or for
        a subset of leaves. This wrapper makes it convenient to generate
        topic strings for API subscriptions and key encodings.

        Parameters
        ----------
        symbol : str
            Market symbol
        index_price_hash : str
            Index price hash
        """

        # Ensure that the list is valid
        if not is_valid_none_list([symbol, index_price_hash]):
            raise Exception("Invalid PriceIdentifier generation")

        self.symbol = symbol
        self.index_price_hash = index_price_hash

    def encompasses_identifier(self, identifier) -> bool:
        """
        Check whether this Identifier encompasses / is a
        superset of another PriceIdentifier.

        Parameters
        ----------
        identifier : PriceIdentifier
            Another PriceIdentifier to compare against
        """

        if type(identifier).__name__ != "PriceIdentifier":
            # If the identifier we are comparing with isn't even of the
            # same type
            return False

        # Check varying degrees of encompassing depending on how
        # granular this Identifier is
        if self.index_price_hash is not None:
            return (
                self.index_price_hash == identifier.index_price_hash
                and self.symbol == identifier.symbol
            )
        if self.symbol is not None:
            return self.symbol == identifier.symbol
        return True

    @property
    def topic_string(self):
        """
        Derive topic string for Identifier.
        """

        return f"{'/'.join(filter(None, ['STATE', IdentifierTypes.PRICE, self.symbol, self.index_price_hash]))}/"

    @classmethod
    def decode_topic_string_into_cls(cls, topic: str):
        """
        Decode a topic string into a PriceIdentifier.

        Parameters
        ----------
        topic : str
            Topic string
        """

        topic_parts = topic.split("/")[:-1]
        if topic_parts[1] != IdentifierTypes.PRICE_CHECKPOINT:
            raise Exception(
                f"Invalid topic: must be of {IdentifierTypes.PRICE_CHECKPOINT} type"
            )

        def safe_list_get(l, idx, default):
            try:
                return l[idx]
            except IndexError:
                return default

        return cls(
            safe_list_get(topic_parts, 2, None), safe_list_get(topic_parts, 3, None)
        )

    @classmethod
    def decode_key_into_cls(cls, leaf_key: bytes, leaf_value=None):
        """
        Decode a Price key into a PriceIdentifier.

        Parameters
        ----------
        leaf_key : bytes
            Price leaf key
        leaf_value
            Leaf value
        """

        symbol = unpack_bytes(leaf_key[1:7])
        index_price_hash = f"0x{leaf_key[7:].hex()}"

        return cls(symbol, index_price_hash)

    @property
    def encoded_key(self) -> bytes:
        """
        Derive encoded key (bytes representation), which may be a full
        32-byte value if fully-granular, or a prefix to help capture
        a subset of leaves.
        """

        if self.index_price_hash is not None:
            return (
                ItemKind.PRICE.to_bytes(1, byteorder="little")
                + pack_bytes(self.symbol)
                + bytes.fromhex(self.index_price_hash[2:])
            )
        elif self.symbol is not None:
            return zpad32_right(
                ItemKind.PRICE.to_bytes(1, byteorder="little") + pack_bytes(self.symbol)
            )
        return ItemKind.PRICE.to_bytes(1, byteorder="little")

    @property
    def as_h256(self) -> H256:
        """
        Derive an H256 representation of the encoded key, which can
        only be done in the case where the Identifier is fully-granular.
        """

        encoded_key = self.encoded_key
        if len(encoded_key) != 32:
            raise Exception("Must be 32-bytes to convert to H256")
        return bytes_array_to_h256(encoded_key)

    @property
    def is_max_granular_key(self):
        return not None in [
            self.symbol,
        ]
