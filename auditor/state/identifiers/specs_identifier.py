"""
SpecsIdentifier module
"""

from eth_abi.utils.padding import zpad32_right

from ddx_client.auditor.shared.identifier import Identifier
from ddx_client.auditor.shared.identifier_types import IdentifierTypes
from ddx_client.auditor.smt.h256 import H256
from ddx_client.auditor.state.item_kinds import ItemKind
from ddx_client.auditor.utils import is_valid_none_list, bytes_array_to_h256


class SpecsIdentifier(Identifier):
    """
    Defines an SpecsIdentifier.
    """

    def __init__(self, chain_discriminant: int, kind: int, name: str):
        """
        Initialize a SpecsIdentifier. Identifiers are used for a
        variety of purposes, most importantly to store identifying
        information for a particular leaf in the DerivaDEX SMT, or for
        a subset of leaves. This wrapper makes it convenient to generate
        topic strings for API subscriptions and key encodings.

        Parameters
        ----------
        chain_discriminant : int
            Blockchain discriminant
        kind : int
            Specs kind
        name : str
            Specs name
        """

        # Ensure that the list is valid
        if not is_valid_none_list([chain_discriminant, kind, name]):
            raise Exception("Invalid SpecsIdentifier generation")

        self.chain_discriminant = chain_discriminant
        self.kind = kind
        self.name = name

    def encompasses_identifier(self, identifier) -> bool:
        """
        Check whether this Identifier encompasses / is a
        superset of another SpecsIdentifier.

        Parameters
        ----------
        identifier : SpecsIdentifier
            Another SpecsIdentifier to compare against
        """

        if type(identifier).__name__ != "SpecsIdentifier":
            # If the identifier we are comparing with isn't even of the
            # same type
            return False

        # Check varying degrees of encompassing depending on how
        # granular this Identifier is
        if self.name is not None:
            return (
                self.chain_discriminant == identifier.chain_discriminant
                and self.kind == identifier.kind
                and self.name == identifier.name
            )
        elif self.kind is not None:
            return (
                self.chain_discriminant == identifier.chain_discriminant
                and self.kind == identifier.kind
            )
        elif self.chain_discriminant is not None:
            return self.chain_discriminant == identifier.chain_discriminant
        return True

    @property
    def topic_string(self) -> str:
        """
        Derive topic string for Identifier.
        """

        return f"{'/'.join(filter(None, ['STATE', IdentifierTypes.SPECS, self.chain_discriminant, self.kind, self.name]))}/"

    @classmethod
    def decode_topic_string_into_cls(cls, topic: str):
        """
        Decode a topic string into a SpecsIdentifier.

        Parameters
        ----------
        topic : str
            Topic string
        """

        topic_parts = topic.split("/")[:-1]
        if topic_parts[1] != IdentifierTypes.SPECS:
            raise Exception(
                f"Invalid topic: must be of {IdentifierTypes.STRATEGY} type"
            )

        def safe_list_get(l, idx, default):
            try:
                return l[idx]
            except IndexError:
                return default

        return cls(
            int(safe_list_get(topic_parts, 2, None)),
            int(safe_list_get(topic_parts, 3, None)),
            safe_list_get(topic_parts, 4, None),
        )

    @classmethod
    def decode_key_into_cls(cls, leaf_key: bytes, leaf_value=None):
        """
        Decode a strategy key into a SpecsIdentifier.

        Parameters
        ----------
        leaf_key : bytes
            Specs leaf key
        leaf_value
            Leaf value
        """

        chain_discriminant = int.from_bytes(leaf_key[1:2], byteorder="little")
        kind = int.from_bytes(leaf_key[2:3], byteorder="little")
        name_len = int.from_bytes(leaf_key[3:4], byteorder="little")
        name = leaf_key[4 : 4 + name_len].decode("utf8")

        return cls(
            chain_discriminant,
            kind,
            name,
        )

    @property
    def encoded_key(self) -> bytes:
        """
        Derive encoded key (bytes representation), which may be a full
        32-byte value if fully-granular, or a prefix to help capture
        a subset of leaves.
        """

        if self.name is not None:
            return zpad32_right(
                ItemKind.SPECS.to_bytes(1, byteorder="little")
                + self.chain_discriminant.to_bytes(1, byteorder="little")
                + self.kind.to_bytes(1, byteorder="little")
                + len(self.name).to_bytes(1, byteorder="little")
                + self.name.encode("utf8")
            )
        elif self.kind is not None:
            return (
                ItemKind.SPECS.to_bytes(1, byteorder="little")
                + self.chain_discriminant.to_bytes(1, byteorder="little")
                + self.kind.to_bytes(1, byteorder="little")
            )
        elif self.chain_discriminant is not None:
            return ItemKind.SPECS.to_bytes(
                1, byteorder="little"
            ) + self.chain_discriminant.to_bytes(1, byteorder="little")
        return ItemKind.SPECS.to_bytes(1, byteorder="little")

    @property
    def as_h256(self) -> H256:
        """
        Derive an H256 representation of the encoded key, which can
        only be done in the case where the Identifier is fully-granular.
        """

        encoded_key = self.encoded_key
        if len(encoded_key) != 32:
            raise Exception("Must be 32-bytes to convert to H256")
        return bytes_array_to_h256(encoded_key)

    @property
    def is_max_granular_key(self):
        return not None in [
            self.chain_discriminant,
            self.kind,
            self.name,
        ]
