"""
OrganicInsuranceFundIdentifier module
"""

from eth_abi.utils.padding import zpad32_right

from ddx_client.auditor.shared.identifier import Identifier
from ddx_client.auditor.shared.identifier_types import IdentifierTypes
from ddx_client.auditor.smt.h256 import H256
from ddx_client.auditor.state.item_kinds import ItemKind
from ddx_client.auditor.utils import bytes_array_to_h256


class OrganicInsuranceFundIdentifier(Identifier):
    """
    Defines an OrganicInsuranceFundIdentifier.
    """

    def encompasses_identifier(self, identifier) -> bool:
        """
        Check whether this Identifier encompasses / is a
        superset of another OrganicInsuranceFundIdentifier.

        Parameters
        ----------
        identifier : OrganicInsuranceFundIdentifier
            Another OrganicInsuranceFundIdentifier to compare against
        """

        return type(identifier).__name__ == "OrganicInsuranceFundIdentifier"

    @property
    def topic_string(self):
        """
        Derive topic string for Identifier.
        """

        return f"{'/'.join(filter(None, ['STATE', IdentifierTypes.ORGANIC_INSURANCE_FUND]))}/"

    @classmethod
    def decode_topic_string_into_cls(cls, topic: str):
        """
        Decode a topic string into a OrganicInsuranceFundIdentifier.

        Parameters
        ----------
        topic : str
            Topic string
        """

        topic_parts = topic.split("/")[:-1]
        if topic_parts[1] != IdentifierTypes.ORGANIC_INSURANCE_FUND:
            raise Exception(
                f"Invalid topic: must be of {IdentifierTypes.ORGANIC_INSURANCE_FUND} type"
            )

        return cls()

    @classmethod
    def decode_key_into_cls(cls, leaf_key: bytes, leaf_value=None):
        """
        Decode a strategy key into a OrganicInsuranceFundIdentifier.

        Parameters
        ----------
        leaf_key : bytes
            OrganicInsuranceFund leaf key
        leaf_value
            Leaf value
        """

        return cls()

    @property
    def encoded_key(self) -> bytes:
        """
        Derive encoded key (bytes representation), which may be a full
        32-byte value if fully-granular, or a prefix to help capture
        a subset of leaves.
        """

        return zpad32_right(
            ItemKind.INSURANCE_FUND.to_bytes(1, byteorder="little")
            + "OrganicInsuranceFund".encode("utf8")
        )

    @property
    def as_h256(self) -> H256:
        """
        Derive an H256 representation of the encoded key, which can
        only be done in the case where the Identifier is fully-granular.
        """

        encoded_key = self.encoded_key
        if len(encoded_key) != 32:
            raise Exception("Must be 32-bytes to convert to H256")
        return bytes_array_to_h256(encoded_key)

    @property
    def is_max_granular_key(self):
        return True
