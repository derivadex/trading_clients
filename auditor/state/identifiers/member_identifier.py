"""
MemberIdentifier module
"""

from eth_abi.utils.padding import zpad32_right

from trading_clients.auditor.shared.identifier import Identifier
from trading_clients.auditor.shared.identifier_types import IdentifierTypes
from trading_clients.auditor.smt.h256 import H256
from trading_clients.auditor.state.item_types import ItemType
from trading_clients.auditor.utils import is_valid_none_list, bytes_array_to_h256


class MemberIdentifier(Identifier):
    """
    Defines an MemberIdentifier.
    """

    def __init__(self, node_id: int):
        """
        Initialize a MemberIdentifier. Identifiers are used for a
        variety of purposes, most importantly to store identifying
        information for a particular leaf in the DerivaDEX SMT, or for
        a subset of leaves. This wrapper makes it convenient to generate
        topic strings for API subscriptions and key encodings.

        Parameters
        ----------
        node_id : int
            Node id
        """

        # Ensure that the list is valid
        if not is_valid_none_list([node_id]):
            raise Exception("Invalid MemberIdentifier generation")

        self.node_id = node_id

    def encompasses_identifier(self, identifier) -> bool:
        """
        Check whether this Identifier encompasses / is a
        superset of another MemberIdentifier.

        Parameters
        ----------
        identifier : MemberIdentifier
            Another MemberIdentifier to compare against
        """

        if type(identifier).__name__ != "MemberIdentifier":
            # If the identifier we are comparing with isn't even of the
            # same type
            return False

        # Check varying degrees of encompassing depending on how
        # granular this Identifier is
        if self.node_id is not None:
            return self.node_id == identifier.node_id
        return True

    @property
    def topic_string(self) -> str:
        """
        Derive topic string for Identifier.
        """

        return f"{'/'.join(filter(None, ['STATE', IdentifierTypes.MEMBER, self.node_id]))}/"

    @classmethod
    def decode_topic_string_into_cls(cls, topic: str):
        """
        Decode a topic string into a MemberIdentifier.

        Parameters
        ----------
        topic : str
            Topic string
        """

        topic_parts = topic.split("/")[:-1]
        if topic_parts[1] != IdentifierTypes.MEMBER:
            raise Exception(f"Invalid topic: must be of {IdentifierTypes.MEMBER} type")

        def safe_list_get(l, idx, default):
            try:
                return l[idx]
            except IndexError:
                return default

        return cls(int(safe_list_get(topic_parts, 2, None)))

    @classmethod
    def decode_key_into_cls(cls, leaf_key: bytes, leaf_value=None):
        """
        Decode a member key into a MemberIdentifier.

        Parameters
        ----------
        leaf_key : bytes
            Member leaf key
        leaf_value
            Leaf value
        """

        node_id = int.from_bytes(leaf_key[1:2], "little")

        return cls(node_id)

    @property
    def encoded_key(self) -> bytes:
        """
        Derive encoded key (bytes representation), which may be a full
        32-byte value if fully-granular, or a prefix to help capture
        a subset of leaves.
        """

        if self.node_id is not None:
            return zpad32_right(
                ItemType.MEMBER.to_bytes(1, byteorder="little")
                + self.node_id.to_bytes(8, byteorder="big")
            )
        return ItemType.MEMBER.to_bytes(1, byteorder="little")

    @property
    def as_h256(self) -> H256:
        """
        Derive an H256 representation of the encoded key, which can
        only be done in the case where the Identifier is fully-granular.
        """

        encoded_key = self.encoded_key
        if len(encoded_key) != 32:
            raise Exception("Must be 32-bytes to convert to H256")
        return bytes_array_to_h256(encoded_key)

    @property
    def is_max_granular_key(self):
        return not None in [self.node_id]
