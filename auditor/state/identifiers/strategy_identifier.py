"""
StrategyIdentifier module
"""
from typing import Optional

from eth_abi import encode_single
from eth_abi.utils.padding import zpad32_right
from web3.auto import w3

from ddx_client.auditor.shared.identifier import Identifier
from ddx_client.auditor.shared.identifier_types import IdentifierTypes
from ddx_client.auditor.smt.h256 import H256
from ddx_client.auditor.state.item_kinds import ItemKind
from ddx_client.auditor.utils import is_valid_none_list, bytes_array_to_h256


class StrategyIdentifier(Identifier):
    """
    Defines an StrategyIdentifier.
    """

    def __init__(
        self, trader_address: Optional[str], abbrev_strategy_id_hash: Optional[str]
    ):
        """
        Initialize a StrategyIdentifier. Identifiers are used for a
        variety of purposes, most importantly to store identifying
        information for a particular leaf in the DerivaDEX SMT, or for
        a subset of leaves. This wrapper makes it convenient to generate
        topic strings for API subscriptions and key encodings.

        Parameters
        ----------
        trader_address : str
            Trader's Ethereum address prefixed with the blockchain
            discriminant
        abbrev_strategy_id_hash : str
            First 4 bytes of the hash of the trader's strategy ID
        """

        # Ensure that the list is valid
        if not is_valid_none_list([trader_address, abbrev_strategy_id_hash]):
            raise Exception("Invalid StrategyIdentifier generation")

        self.trader_address = trader_address
        self.abbrev_strategy_id_hash = abbrev_strategy_id_hash

    def encompasses_identifier(self, identifier) -> bool:
        """
        Check whether this Identifier encompasses / is a
        superset of another StrategyIdentifier.

        Parameters
        ----------
        identifier : StrategyIdentifier
            Another StrategyIdentifier to compare against
        """

        if type(identifier).__name__ != "StrategyIdentifier":
            # If the identifier we are comparing with isn't even of the
            # same type
            return False

        # Check varying degrees of encompassing depending on how
        # granular this Identifier is
        if self.abbrev_strategy_id_hash is not None:
            return (
                self.trader_address == identifier.trader_address
                and self.abbrev_strategy_id_hash == identifier.abbrev_strategy_id_hash
            )
        elif self.trader_address is not None:
            return self.trader_address == identifier.trader_address
        return True

    @property
    def topic_string(self) -> str:
        """
        Derive topic string for Identifier.
        """

        return f"{'/'.join(filter(None, ['STATE', IdentifierTypes.STRATEGY, self.trader_address, self.abbrev_strategy_id_hash]))}/"

    @classmethod
    def decode_topic_string_into_cls(cls, topic: str):
        """
        Decode a topic string into a StrategyIdentifier.

        Parameters
        ----------
        topic : str
            Topic string
        """

        topic_parts = topic.split("/")[:-1]
        if topic_parts[1] != IdentifierTypes.STRATEGY:
            raise Exception(
                f"Invalid topic: must be of {IdentifierTypes.STRATEGY} type"
            )

        def safe_list_get(l, idx, default):
            try:
                return l[idx]
            except IndexError:
                return default

        trader_address = safe_list_get(topic_parts, 2, None)
        return cls(
            trader_address.lower() if trader_address is not None else trader_address,
            safe_list_get(topic_parts, 3, None),
        )

    @classmethod
    def decode_key_into_cls(cls, leaf_key: bytes, leaf_value=None):
        """
        Decode a strategy key into a StrategyIdentifier.

        Parameters
        ----------
        leaf_key : bytes
            Strategy leaf key
        leaf_value
            Leaf value
        """

        trader_address = f"0x{leaf_key[1:22].hex()}"
        abbrev_strategy_id_hash = f"0x{leaf_key[22:26].hex()}"

        return cls(trader_address, abbrev_strategy_id_hash)

    @staticmethod
    def generate_strategy_id_hash(strategy_id: str) -> bytes:
        """
        Generate the abbreviated strategy ID given a strategy ID.
        An abbreviated strategy ID is the first 4 bytes of the hash of
        the strategy ID.

        Parameters
        ----------
        strategy_id : str
            Strategy ID (e.g. "main")
        """

        return w3.keccak(
            len(strategy_id).to_bytes(1, byteorder="little")
            + encode_single("bytes32", strategy_id.encode("utf8"))[:-1]
        )[:4]

    @property
    def encoded_key(self) -> bytes:
        """
        Derive encoded key (bytes representation), which may be a full
        32-byte value if fully-granular, or a prefix to help capture
        a subset of leaves.
        """

        if self.abbrev_strategy_id_hash is not None:
            return zpad32_right(
                ItemKind.STRATEGY.to_bytes(1, byteorder="little")
                + bytes.fromhex(self.trader_address[2:])
                + bytes.fromhex(self.abbrev_strategy_id_hash[2:])
            )
        elif self.trader_address is not None:
            return ItemKind.STRATEGY.to_bytes(1, byteorder="little") + bytes.fromhex(
                self.trader_address[2:]
            )
        return ItemKind.STRATEGY.to_bytes(1, byteorder="little")

    @property
    def as_h256(self) -> H256:
        """
        Derive an H256 representation of the encoded key, which can
        only be done in the case where the Identifier is fully-granular.
        """

        encoded_key = self.encoded_key
        if len(encoded_key) != 32:
            raise Exception("Must be 32-bytes to convert to H256")
        return bytes_array_to_h256(encoded_key)

    @classmethod
    def decode_position_key_into_cls(cls, position_key: bytes):
        """
        A convenience method to obtain the StrategyIdentifier/key from a
        Position leaf's key.

        Parameters
        ----------
        position_key : bytes
            Position leaf's key to derive a Strategy key
        """

        trader_address = f"0x{position_key[7:28].hex()}"
        abbrev_strategy_id_hash = f"0x{position_key[28:].hex()}"

        return cls(trader_address, abbrev_strategy_id_hash)

    @property
    def is_max_granular_key(self):
        return not None in [
            self.trader_address,
            self.abbrev_strategy_id_hash,
        ]
