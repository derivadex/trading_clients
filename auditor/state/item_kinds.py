"""
ItemKind module
"""

from enum import Enum


class ItemKind(int, Enum):
    """
    Defines an item type
    """

    EMPTY = 0
    TRADER = 1
    STRATEGY = 2
    POSITION = 3
    BOOK_ORDER = 4
    PRICE = 5
    INSURANCE_FUND = 6
    STATS = 7
    SIGNER = 8
    SPECS = 9
    INSURANCE_FUND_CONTRIBUTION = 10
    FEE_POOL = 11
    EPOCH_METADATA = 12
