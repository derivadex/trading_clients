"""
Release module
"""

from eth_abi import encode_single, decode_single
from web3.auto import w3

from ddx_client.auditor.state.abi_constant import AbiConstant
from ddx_client.auditor.state.item_kinds import ItemKind
from ddx_client.auditor.utils import bytes_array_to_h256


class Release:
    def __init__(self, effective_epoch_id: int):
        """
        Initialize a Release leaf. A Release contains information
        pertaining to a release.

        Parameters
        ----------
        effective_epoch_id : int
           Epoch id release takes effect
        """

        self.item_kind = ItemKind.RELEASE
        self.effective_epoch_id = effective_epoch_id

    @property
    def abi_encoded_value(self):
        """
        Property returning the custom ABI tokenization of the Release.
        """

        return encode_single(
            AbiConstant.RELEASE,
            [self.item_kind, self.effective_epoch_id],
        )

    @property
    def hash_value(self):
        """
        Property returning the keccak256 hash of the encoded Trader.
        """

        return w3.keccak(self.abi_encoded_value)

    @property
    def as_h256(self):
        """
        Property returning the H256 repr of the hash_value of the
        Trader.
        """

        return bytes_array_to_h256(self.hash_value)

    @classmethod
    def abi_decode_value_into_cls(cls, abi_encoded_value):
        """
        ABI Decode an ABI-encoded value from the state snapshot into
        a Release leaf instance.

        Parameters
        ----------
        abi_encoded_value : str
           Hexstr repr of the ABI-encoded Release leaf from the
           state snapshot
        """

        (item_kind, effective_epoch_id) = decode_single(
            AbiConstant.RELEASE,
            w3.toBytes(hexstr=abi_encoded_value),
        )

        return cls(effective_epoch_id)
