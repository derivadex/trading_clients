"""
Change module
"""
import asyncio
from typing import Dict

from trading_clients.auditor.smt.smt import SparseMerkleTree
from trading_clients.auditor.state.identifiers.member_identifier import MemberIdentifier
from trading_clients.auditor.state.registered_member import RegisteredMember
from trading_clients.auditor.transactions.event_types import EventType
from trading_clients.auditor.transactions.membership_change import MembershipChange
from trading_clients.auditor.utils import bytes_array_to_h256


class Change(MembershipChange):
    """
    Defines an Change
    """

    def __init__(
        self,
        action: str,
        node_id: int,
        signing_address: str,
        url: str,
        want_role: str,
        request_index: int,
    ):
        """
        Initialize a Change MembershipChange transaction.

        Parameters
        ----------
        action : str
           Type of MembershipChange (Change | Leave)
        node_id : str
           The operator's unique identifier within the Raft
        signing_address: str
           The public key derived address required to validate all
           signatures for this operator
        url: str
           The operator's rpc server url
        want_role: str
           The intended role (not necessarily current role) of this node
           within the Raft
        request_index : int
            Sequenced request index of transaction
        """

        super().__init__(action, node_id, request_index)
        self.signing_address = signing_address
        self.url = url
        self.want_role = want_role

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a Change
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        change_tx_event = raw_tx_log_event["event"]["c"]

        return cls(
            change_tx_event["action"],
            change_tx_event["nodeId"],
            change_tx_event["signingAddress"],
            change_tx_event["url"],
            change_tx_event["wantRole"],
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a Change transaction of type MembershipChange. This
        indicates a change to the raft membership.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to Post transactions
        """

        # Construct a MemberIdentifier and corresponding encoded key
        member_identifier = MemberIdentifier(self.node_id)
        member_key = member_identifier.encoded_key
        member_key_h256 = bytes_array_to_h256(member_key)

        # Initialize a new Member leaf
        member_leaf = RegisteredMember(
            self.node_id,
            self.signing_address,
            self.url,
            self.want_role,
        )

        # Update the SMT with the H256 repr of the key and the Member
        # leaf
        smt.update(member_key_h256, member_leaf)

    def repr_json(self):
        return {
            "eventType": EventType.MEMBERSHIP_CHANGE,
            "requestIndex": self.request_index,
            "action": self.action,
            "nodeId": self.node_id,
            "signingAddress": self.signing_address,
            "url": self.url,
            "wantRole": self.want_role,
        }

    def __repr__(self):
        return f"Change (event): request_index = {self.request_index}; action: {self.action}; node_id: {self.node_id}; signing_address: {self.signing_address}; url: {self.url}; want_role: {self.want_role}"
