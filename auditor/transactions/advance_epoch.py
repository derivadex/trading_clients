"""
AdvanceEpoch module
"""
import asyncio
from typing import Dict

from ddx_python.decimal import Decimal

from ddx_client.auditor.smt.smt import SparseMerkleTree
from ddx_client.auditor.state.epoch_metadata import EpochMetadata
from ddx_client.auditor.state.fee_pool import FeePool
from ddx_client.auditor.state.identifiers.epoch_metadata_identifier import (
    EpochMetadataIdentifier,
)
from ddx_client.auditor.state.identifiers.fee_pool_identifier import FeePoolIdentifier
from ddx_client.auditor.transactions.epoch_marker import EpochMarker
from ddx_client.auditor.transactions.event_types import EventType
from ddx_client.auditor.utils import bytes_array_to_h256


class AdvanceEpoch(EpochMarker):
    """
    Defines an AdvanceEpoch
    """

    def __init__(
        self,
        kind: str,
        next_book_ordinals: Dict,
        new_epoch_id: int,
        request_index: int,
    ):
        """
        Initialize an AdvanceEpoch non-transitioning transaction.

        Parameters
        ----------
        kind : str
           Type of EpochMarker (Genesis | AdvanceEpoch)
        next_book_ordinals : Dict
           Dictionary ({symbol: next_book_ordinal})
           indicating the next book ordinal by symbol
        new_epoch_id : int
           New epoch ID after epoch marker
        request_index : int
            Sequenced request index of transaction
        """

        super().__init__(kind, request_index)
        self.next_book_ordinals = next_book_ordinals
        self.new_epoch_id = new_epoch_id

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into an AdvanceEpoch
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        advance_epoch_tx_event = raw_tx_log_event["event"]["c"]

        return cls(
            advance_epoch_tx_event["kind"],
            advance_epoch_tx_event["nextBookOrdinals"],
            advance_epoch_tx_event["newEpochId"],
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process an EpochMarker transaction of type AdvanceEpoch. This
        indicates the a new epoch in the transaction log, although
        it is not state-transitioning in the way typical transactions
        are.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to Post transactions
        """

        # Initialize EpochMetadata leaf
        epoch_metadata_identifier = EpochMetadataIdentifier(self.new_epoch_id - 1)
        epoch_metadata_identifier_key = epoch_metadata_identifier.encoded_key
        epoch_metadata_identifier_key_h256 = bytes_array_to_h256(
            epoch_metadata_identifier_key
        )
        epoch_metadata_leaf = EpochMetadata(self.next_book_ordinals)
        smt.update(
            epoch_metadata_identifier_key_h256,
            epoch_metadata_leaf,
        )

        # Evaluate current fee pool to swap
        fee_pool_identifier = FeePoolIdentifier(-1)
        fee_pool_identifier_key = fee_pool_identifier.encoded_key
        fee_pool_identifier_key_h256 = bytes_array_to_h256(fee_pool_identifier_key)
        fee_pool = smt.get(fee_pool_identifier_key_h256)
        if kwargs["ddx_address"] in fee_pool.pool and fee_pool.pool[
            kwargs["ddx_address"]
        ] != Decimal("0"):
            fee_pool_prev_identifier = FeePoolIdentifier(self.new_epoch_id - 1)
            fee_pool_prev_identifier_key = fee_pool_prev_identifier.encoded_key
            fee_pool_prev_identifier_key_h256 = bytes_array_to_h256(
                fee_pool_prev_identifier_key
            )
            smt.update(fee_pool_prev_identifier_key_h256, fee_pool)

            smt.update(fee_pool_identifier_key_h256, FeePool({}))

        # Set the expected epoch ID to be the new epoch ID and the
        # expected tx ordinal to be -1, because we immediately increment
        # this by 1, thus setting it to 0, which will be the first
        # tx ordinal of the next epoch
        kwargs["expected_epoch_id"](kwargs["auditor_instance"], self.new_epoch_id)
        kwargs["expected_tx_ordinal"](kwargs["auditor_instance"], -1)

    def repr_json(self):
        return {
            "eventType": EventType.EPOCH_MARKER,
            "requestIndex": self.request_index,
            "kind": self.kind,
            "nextBookOrdinals": self.next_book_ordinals,
            "newEpochId": self.new_epoch_id,
        }

    @property
    def metrics_repr(self):
        return {
            "eventType": EventType.EPOCH_MARKER,
            "requestIndex": self.request_index,
            "kind": self.kind,
            "nextBookOrdinals": self.next_book_ordinals,
            "newEpochId": self.new_epoch_id,
        }

    def __repr__(self):
        return f"AdvanceEpoch (event): request_index = {self.request_index}; kind: {self.kind}; next_book_ordinals: {self.next_book_ordinals}; new_epoch_id: {self.new_epoch_id}"
