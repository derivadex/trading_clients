"""
AllPriceCheckpoints module
"""
import asyncio
from typing import Dict, List
from ddx_python.decimal import Decimal

from ddx_client.auditor.smt.smt import SparseMerkleTree
from ddx_client.auditor.transactions.event import Event
from ddx_client.auditor.transactions.price_checkpoint import PriceCheckpoint


class AllPriceCheckpoints(Event):
    """
    Defines a AllPriceCheckpoints
    """

    def __init__(self, price_checkpoints: List[PriceCheckpoint], request_index: int):
        """
        Initialize an AllPriceCheckpoints transaction. An
        AllPriceCheckpoint is when a market registers an update to the
        composite index price a perpetual is tracking along with the ema
        component for 1+ symbols.

        Parameters
        ----------
        price_checkpoints : PriceCheckpoint
           The price checkpoints that make up this transaction
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.price_checkpoints = price_checkpoints

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into an
        AllPriceCheckpoints instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        all_price_checkpoint_tx_event = raw_tx_log_event["event"]["c"]

        return cls(
            [
                PriceCheckpoint(
                    all_price_checkpoints_key,
                    Decimal(all_price_checkpoints_val["ema"]),
                    all_price_checkpoints_val["indexPriceHash"],
                    Decimal(all_price_checkpoints_val["indexPrice"]),
                    all_price_checkpoints_val["ordinal"],
                    raw_tx_log_event["requestIndex"],
                )
                for all_price_checkpoints_key, all_price_checkpoints_val in all_price_checkpoint_tx_event.items()
            ],
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process an AllPriceCheckpoints transaction. An
        AllPriceCheckpoints consists of information relating to a new
        price checkpoint for 1+ symbols.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to Post transactions
        """

        # Loop through each price checkpoint event and process them
        # individually
        for price_checkpoint in self.price_checkpoints:
            price_checkpoint.process_tx(
                smt, trader_auditor_queue, suppress_trader_queue, **kwargs
            )

    def __repr__(self):
        return f"AllPriceCheckpoints (event): request_index = {self.request_index}; price_checkpoints = {self.price_checkpoints}"
