"""
EventMessage module
"""

from typing import Dict

from ddx_client.auditor.transactions.event import Event
from ddx_client.client.websocket_message import (
    WebsocketMessage,
    WebsocketEventType,
)


class EventMessage(WebsocketMessage):
    """
    An EventMessage class
    """

    def __init__(
        self,
        message_type: str,
        message_event: WebsocketEventType,
        event_data: Dict,
    ):
        super().__init__(message_type, event_data)
        self.message_event = message_event

    @classmethod
    def decode_value_into_cls(cls, raw_event_message: Dict):
        """
        Decode a raw item message into class

        Parameters
        ----------
        raw_event_message : Dict
            Raw event message
        """

        return cls(
            raw_event_message["t"],
            raw_event_message["e"],
            raw_event_message["c"],
        )

    def repr_json(self):
        return {
            "t": self.message_type,
            "e": self.message_event,
            "c": self.message_content,
        }
