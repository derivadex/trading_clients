"""
TradeMining module
"""
import asyncio
from typing import Dict
from ddx_python.decimal import Decimal

from ddx_client.auditor.smt.smt import SparseMerkleTree
from ddx_client.auditor.state.empty import Empty
from ddx_client.auditor.state.identifiers.stats_identifier import StatsIdentifier
from ddx_client.auditor.state.identifiers.trader_identifier import TraderIdentifier
from ddx_client.auditor.state.item_message import ItemMessage
from ddx_client.auditor.transactions.event import Event
from ddx_client.auditor.transactions.event_types import EventType
from ddx_client.auditor.transactions.total_volume import TotalVolume
from ddx_client.auditor.utils import (
    round_to_unit,
    h256_to_bytes_array,
    bytes_array_to_h256,
)
from ddx_client.client.asyncio_utils import place_message_in_queue
from ddx_client.client.websocket_message import WebsocketEventType


class TradeMining(Event):
    """
    Defines a TradeMining
    """

    def __init__(
        self,
        trade_mining_epoch_id: int,
        ddx_distributed: Decimal,
        total_volume: TotalVolume,
        request_index: int,
    ):
        """
        Initialize a TradeMining transaction. A TradeMining is
        when a there is a trade mining distribution.

        Parameters
        ----------
        trade_mining_epoch_id : int
           The epoch id for the trade mining event.
        ddx_distributed : Decimal
           The total DDX distributed in this interval.
        total_volume: Dict[str, Decimal]
           The total maker and taker volume for this interval.
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.trade_mining_epoch_id = trade_mining_epoch_id
        self.ddx_distributed = ddx_distributed
        self.total_volume = total_volume

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a Funding
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        trade_mining_tx_event = raw_tx_log_event["event"]["c"]

        return cls(
            trade_mining_tx_event["tradeMiningEpochId"],
            Decimal(trade_mining_tx_event["ddxDistributed"]),
            TotalVolume(
                Decimal(trade_mining_tx_event["totalVolume"]["makerVolume"]),
                Decimal(trade_mining_tx_event["totalVolume"]["takerVolume"]),
            ),
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a TradeMining transaction. A TradeMining event consists
        of consists of information relating to the trade mining
        distribution, when DDX will be allocated to traders due to
        their maker and taker volume contributions as a proportion to
        the overall exchange volume.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to Post transactions
        """

        if self.ddx_distributed != Decimal("0"):
            # If trade mining did distribute DDX during the trade
            # mining epoch, we should handle DDX distributions,
            # otherwise we can gracefully skip

            # The overall trade mining allocation is 75% of the
            # liquidity mining supply (50mm DDX) issued over a 10-year
            # schedule, 3 times a day
            ddx_reward_per_epoch = round_to_unit(Decimal("35_000_000") / (10 * 365 * 3))

            # Get the stats leaves for all the traders
            stats_leaves = kwargs["get_stats_leaves"]()

            # Loop through all the stats leaves
            for stats_key_h256, stats_leaf_value in stats_leaves:
                # Derive StatsIdentifier given the key
                stats_identifier = StatsIdentifier.decode_key_into_cls(
                    h256_to_bytes_array(stats_key_h256)
                )

                # Compute the DDX gained as a result of maker volume
                # for the trader (20% of the DDX rewards go to makers)
                ddx_accrued_as_maker = (
                    stats_leaf_value.maker_volume
                    / self.total_volume.maker_volume
                    * ddx_reward_per_epoch
                    * Decimal("0.2")
                    if self.total_volume.maker_volume != Decimal("0")
                    else Decimal("0")
                )

                # Compute the DDX gained as a result of taker volume
                # for the trader (80% of the DDX rewards go to takers)
                ddx_accrued_as_taker = (
                    stats_leaf_value.taker_volume
                    / self.total_volume.taker_volume
                    * ddx_reward_per_epoch
                    * Decimal("0.8")
                    if self.total_volume.taker_volume != Decimal("0")
                    else Decimal("0")
                )

                if ddx_accrued_as_maker != Decimal(
                    "0"
                ) or ddx_accrued_as_taker != Decimal("0"):
                    # If DDX accrued for trader is non-zero, handle
                    # distribution

                    # Derive TraderIdentifier given the key
                    trader_identifier = TraderIdentifier.decode_stats_key_into_cls(
                        h256_to_bytes_array(stats_key_h256)
                    )
                    trader_key = trader_identifier.encoded_key
                    trader_key_h256 = bytes_array_to_h256(trader_key)

                    # Get Trader leaf given the key from above
                    trader_leaf = smt.get(trader_key_h256)

                    # Increment the Trader leaf's free balance by the
                    # DDX accrued (both maker and taker)
                    trader_leaf.free_ddx_balance = round_to_unit(
                        trader_leaf.free_ddx_balance
                        + ddx_accrued_as_maker
                        + ddx_accrued_as_taker
                    )

                    # Update the SMT with the H256 repr of the key and
                    # the Trader leaf
                    smt.update(trader_key_h256, trader_leaf)

                    # Place the Trader leaf update message on the queue to
                    # consider sending to the Trader
                    place_message_in_queue(
                        trader_auditor_queue,
                        suppress_trader_queue,
                        ItemMessage(
                            trader_identifier.topic_string,
                            WebsocketEventType.UPDATE,
                            {"t": trader_identifier.topic_string, "c": trader_leaf},
                            self,
                        ),
                    )

                    # Update the SMT with the H256 repr of the key and
                    # the Stats leaf
                    smt.update(stats_key_h256, Empty())

                    # Place the Stats leaf update message on the queue to
                    # consider sending to the Trader
                    place_message_in_queue(
                        trader_auditor_queue,
                        suppress_trader_queue,
                        ItemMessage(
                            stats_identifier.topic_string,
                            WebsocketEventType.UPDATE,
                            {"t": stats_identifier.topic_string, "c": Empty()},
                            self,
                        ),
                    )

    def repr_json(self):
        return {
            "eventType": EventType.TRADE_MINING,
            "requestIndex": self.request_index,
            "tradeMiningEpochId": self.trade_mining_epoch_id,
            "ddxDistributed": str(self.ddx_distributed),
            "totalVolume": self.total_volume,
        }

    def __repr__(self):
        return f"TradeMining (event): request_index = {self.request_index}; trade_mining_epoch_id = {self.trade_mining_epoch_id}; ddx_distributed = {self.ddx_distributed}; total_volume = {self.total_volume}"
