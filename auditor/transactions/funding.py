"""
Funding module
"""
import asyncio
from typing import Dict, List
from ddx_python.decimal import Decimal

from ddx_client.auditor.smt.smt import SparseMerkleTree
from ddx_client.auditor.state.empty import Empty
from ddx_client.auditor.state.identifiers.price_identifier import PriceIdentifier
from ddx_client.auditor.state.identifiers.strategy_identifier import (
    StrategyIdentifier,
)
from ddx_client.auditor.state.item_message import ItemMessage
from ddx_client.auditor.transactions.adl_outcome import AdlOutcome
from ddx_client.auditor.transactions.cancel import Cancel
from ddx_client.auditor.transactions.event import Event
from ddx_client.auditor.transactions.event_types import EventType
from ddx_client.auditor.transactions.liquidated_position import LiquidatedPosition
from ddx_client.auditor.transactions.liquidation import Liquidation
from ddx_client.auditor.transactions.liquidation_entry import LiquidationEntry
from ddx_client.auditor.transactions.liquidation_fill import LiquidationFill
from ddx_client.auditor.transactions.outcome import Outcome
from ddx_client.auditor.transactions.price_checkpoint import PriceCheckpoint
from ddx_client.auditor.utils import (
    round_to_unit,
    h256_to_bytes_array,
    bytes_array_to_h256,
    to_camel_case,
)
from ddx_client.client.asyncio_utils import place_message_in_queue
from ddx_client.client.websocket_message import WebsocketEventType


class Funding(Event):
    """
    Defines a Funding
    """

    def __init__(
        self,
        settlement_epoch_id: int,
        liquidation: Liquidation,
        time_value: int,
        request_index: int,
    ):
        """
        Initialize a Funding transaction. A Funding is
        when a there is a funding rate distribution.

        Parameters
        ----------
        settlement_epoch_id : int
           The epoch id for the funding event.
        time_value: int
           Time value
        liquidation : Liquidation
            Liquidations
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.settlement_epoch_id = settlement_epoch_id
        self.liquidation = liquidation
        self.time_value = time_value

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a Funding
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        funding_tx_event = raw_tx_log_event["event"]["c"][0]
        liquidation_tx_event = raw_tx_log_event["event"]["c"][1]

        return cls(
            funding_tx_event["settlementEpochId"],
            Liquidation(
                [],
                [
                    LiquidationEntry(
                        liquidation_entry["traderAddress"],
                        liquidation_entry["strategyId"],
                        [
                            Cancel(
                                canceled_order["symbol"],
                                canceled_order["orderHash"],
                                Decimal(canceled_order["amount"]),
                                to_camel_case(EventType.LIQUIDATION.name),
                                raw_tx_log_event["requestIndex"],
                            )
                            for canceled_order in liquidation_entry["canceledOrders"]
                        ],
                        [
                            (
                                liquidated_position_key,
                                LiquidatedPosition(
                                    Decimal(liquidated_position_val["amount"]),
                                    [
                                        LiquidationFill(
                                            trade_outcome["Fill"]["reason"],
                                            trade_outcome["Fill"]["symbol"],
                                            trade_outcome["Fill"]["indexPriceHash"],
                                            trade_outcome["Fill"]["makerOrderHash"],
                                            Decimal(
                                                trade_outcome["Fill"][
                                                    "makerOrderRemainingAmount"
                                                ]
                                            ),
                                            Decimal(trade_outcome["Fill"]["amount"]),
                                            Decimal(trade_outcome["Fill"]["price"]),
                                            trade_outcome["Fill"]["takerSide"],
                                            Outcome(
                                                trade_outcome["Fill"]["makerOutcome"][
                                                    "trader"
                                                ],
                                                trade_outcome["Fill"]["makerOutcome"][
                                                    "strategy"
                                                ],
                                                Decimal(
                                                    trade_outcome["Fill"][
                                                        "makerOutcome"
                                                    ]["fee"]
                                                ),
                                                trade_outcome["Fill"]["makerOutcome"][
                                                    "ddxFeeElection"
                                                ],
                                            ),
                                            raw_tx_log_event["timeValue"],
                                            raw_tx_log_event["requestIndex"],
                                        )
                                        if "Fill" in trade_outcome
                                        else Cancel(
                                            trade_outcome["Cancel"]["symbol"],
                                            trade_outcome["Cancel"]["orderHash"],
                                            Decimal(trade_outcome["Cancel"]["amount"]),
                                            to_camel_case(EventType.LIQUIDATION.name),
                                            raw_tx_log_event["requestIndex"],
                                        )
                                        for trade_outcome in liquidated_position_val[
                                            "tradeOutcomes"
                                        ]
                                    ],
                                    [
                                        AdlOutcome(
                                            adl_outcome["traderAddress"],
                                            adl_outcome["strategyId"],
                                            raw_tx_log_event["requestIndex"],
                                        )
                                        for adl_outcome in liquidated_position_val[
                                            "adlOutcomes"
                                        ]
                                    ],
                                    Decimal(
                                        liquidated_position_val["newInsuranceFundCap"]
                                    ),
                                    raw_tx_log_event["requestIndex"],
                                ),
                            )
                            for (
                                liquidated_position_key,
                                liquidated_position_val,
                            ) in liquidation_entry["positions"]
                        ],
                        raw_tx_log_event["requestIndex"],
                    )
                    for liquidation_entry in liquidation_tx_event
                ],
                raw_tx_log_event["requestIndex"],
            ),
            raw_tx_log_event["timeValue"],
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a Funding transaction. A Funding event consists of
        consists of information relating to funding rate-related
        distributions. All open positions will result in traders
        either paying or receiving a USDC debit/credit to their
        free collateral as a function of the funding rate (given the
        Price leaves in the SMT at this time) and their
        position notional (given the latest mark price).

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to Post transactions
        """

        funding_strategies = {}

        # Loop through the funding rate symbols and values as specified
        # in the transaction
        for funding_rate_symbol in kwargs["latest_price_leaves"]:
            funding_rate = kwargs["get_funding_rate"](funding_rate_symbol)

            if funding_rate != Decimal("0"):
                # If funding rate is non-zero, handle funding payments

                # Get position leaves for symbol
                position_leaves_for_symbol = kwargs["get_position_leaves_for_symbol"](
                    funding_rate_symbol
                )

                # Obtain latest mark price
                mark_price = kwargs["latest_price_leaves"][funding_rate_symbol][
                    1
                ].mark_price

                # Loop through each open position
                for (
                    position_key_h256,
                    position_leaf_value,
                ) in position_leaves_for_symbol:
                    # Compute the funding payment for the trader. When
                    # the funding rate is positive, long traders will
                    # pay and short traders will receive payments. When
                    # the funding rate is negative, long traders will
                    # receive payments and short traders will pay.
                    funding_delta = (
                        (
                            Decimal("-1.0")
                            if position_leaf_value.side == "Long"
                            else Decimal("1.0")
                        )
                        * funding_rate
                        * position_leaf_value.balance
                        * mark_price
                    )

                    # Construct a StrategyIdentifier and corresponding encoded
                    # key
                    strategy_identifier = (
                        StrategyIdentifier.decode_position_key_into_cls(
                            h256_to_bytes_array(position_key_h256)
                        )
                    )
                    strategy_key = strategy_identifier.encoded_key

                    funding_strategies[strategy_key] = (
                        funding_strategies[strategy_key] + funding_delta
                        if strategy_key in funding_strategies
                        else funding_delta
                    )

            price_leaves_for_symbol = kwargs["get_price_leaves_for_symbol"](
                funding_rate_symbol
            )
            for price_key_h256, price_leaf_value in price_leaves_for_symbol:
                if (
                    price_key_h256
                    == kwargs["latest_price_leaves"][funding_rate_symbol][0]
                ):
                    # We should keep the latest Price leaf, so we skip
                    continue

                smt.update(price_key_h256, Empty())

                # Derive StatsIdentifier given the key
                price_identifier = PriceIdentifier.decode_key_into_cls(
                    h256_to_bytes_array(price_key_h256)
                )
                # Place the Price leaf update message on the
                # queue to consider sending to the Trader
                place_message_in_queue(
                    trader_auditor_queue,
                    suppress_trader_queue,
                    ItemMessage(
                        price_identifier.topic_string,
                        WebsocketEventType.UPDATE,
                        {"t": price_identifier.topic_string, "c": Empty()},
                        self,
                    ),
                )

        for strategy_key, funding_delta in funding_strategies.items():
            strategy_identifier = StrategyIdentifier.decode_key_into_cls(strategy_key)
            strategy_key_h256 = bytes_array_to_h256(strategy_key)

            # Get the Strategy leaf given the key from above
            strategy_leaf = smt.get(strategy_key_h256)

            # Credit/debit the trader's Strategy leaf by the
            # funding delta from above
            strategy_leaf.free_collateral[kwargs["usdc_address"]] = round_to_unit(
                strategy_leaf.free_collateral[kwargs["usdc_address"]] + funding_delta
            )

            # Update the SMT with the H256 repr of the key and
            # the Strategy leaf
            smt.update(strategy_key_h256, strategy_leaf)

            # Place the Strategy leaf update message on the
            # queue to consider sending to the Trader
            place_message_in_queue(
                trader_auditor_queue,
                suppress_trader_queue,
                ItemMessage(
                    strategy_identifier.topic_string,
                    WebsocketEventType.UPDATE,
                    {
                        "t": strategy_identifier.topic_string,
                        "c": strategy_leaf,
                    },
                    self,
                ),
            )

        # Process liquidation
        self.liquidation.process_tx(
            smt, trader_auditor_queue, suppress_trader_queue, **kwargs
        )

    def repr_json(self):
        return {
            "eventType": EventType.FUNDING,
            "requestIndex": self.request_index,
            "settlementEpochId": self.settlement_epoch_id,
            "liquidation": self.liquidation,
            "timeValue": self.time_value,
        }

    def __repr__(self):
        return f"Funding (event): request_index = {self.request_index}; settlement_epoch_id = {self.settlement_epoch_id}; time_value = {self.time_value}"

    @property
    def metrics_repr(self):
        return {
            "eventType": EventType.FUNDING,
            "requestIndex": self.request_index,
            "settlementEpochId": self.settlement_epoch_id,
            "timeValue": self.time_value,
        }
