"""
PnlSettlement module
"""
import asyncio
from ddx_python.decimal import Decimal
from typing import Dict, Tuple, List

from ddx_client.auditor.smt.h256 import H256
from ddx_client.auditor.smt.smt import SparseMerkleTree
from ddx_client.auditor.state.identifiers.position_identifier import (
    PositionIdentifier,
)
from ddx_client.auditor.state.identifiers.strategy_identifier import (
    StrategyIdentifier,
)
from ddx_client.auditor.state.item_message import ItemMessage
from ddx_client.auditor.state.position import Position
from ddx_client.auditor.transactions.event import Event
from ddx_client.auditor.transactions.event_types import EventType
from ddx_client.auditor.transactions.price_checkpoint import PriceCheckpoint
from ddx_client.auditor.utils import (
    h256_to_bytes_array,
    bytes_array_to_h256,
    round_to_unit,
)
from ddx_client.client.asyncio_utils import place_message_in_queue
from ddx_client.client.websocket_message import WebsocketEventType


class PnlSettlement(Event):
    """
    Defines a PnlSettlement
    """

    def __init__(
        self,
        price_checkpoints: List[PriceCheckpoint],
        settlement_epoch_id: int,
        time_value: int,
        request_index: int,
    ):
        """
        Initialize a PnlSettlement transaction. A PnlSettlement when
        all strategies have unrealized pnl realized.

        Parameters
        ----------
        price_checkpoints : PriceCheckpoint
           The price checkpoints that make up this transaction
        settlement_epoch_id : int
           Settlement epoch id
        time_value: int
           Time value
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.price_checkpoints = price_checkpoints
        self.settlement_epoch_id = settlement_epoch_id
        self.time_value = time_value

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a PnlSettlement
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        all_price_checkpoint_tx_event = raw_tx_log_event["event"]["c"][0]
        pnl_settlement_tx_event = raw_tx_log_event["event"]["c"][1]

        return cls(
            [
                PriceCheckpoint(
                    all_price_checkpoints_key,
                    Decimal(all_price_checkpoints_val["ema"]),
                    all_price_checkpoints_val["indexPriceHash"],
                    Decimal(all_price_checkpoints_val["indexPrice"]),
                    all_price_checkpoints_val["ordinal"],
                    raw_tx_log_event["requestIndex"],
                )
                for all_price_checkpoints_key, all_price_checkpoints_val in all_price_checkpoint_tx_event.items()
            ],
            pnl_settlement_tx_event["settlementEpochId"],
            raw_tx_log_event["timeValue"],
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a PnlSettlement transaction. A PnlSettlement event
        consists of information relating to when unrealized pnl is
        settled/realized for all traders' strategies. This will result
        in a credit/debit for all strategies. Furthermore, the
        average entry price for any open positions will be set to the
        current mark price.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to Post transactions
        """

        # Loop through each price checkpoint event and process them
        # individually
        for price_checkpoint in self.price_checkpoints:
            price_checkpoint.process_tx(
                smt, trader_auditor_queue, suppress_trader_queue, **kwargs
            )

        # Derive a PositionIdentifier and encoded key to obtain all
        # Position leaves
        position_identifier = PositionIdentifier(None, None, None)
        position_key_prefix = position_identifier.encoded_key

        # Obtain the Position leaves that adhere to the prefix computed
        # above
        position_leaves: List[Tuple[H256, PositionIdentifier, Position]] = [
            (
                node.key,
                PositionIdentifier.decode_key_into_cls(h256_to_bytes_array(node.key)),
                node.value,
            )
            for node_hash, node in smt.store().leaves_map().items()
            if h256_to_bytes_array(node.key)[: len(position_key_prefix)]
            == position_key_prefix
        ]

        # Sort position leaves based on the symbol
        sorted_position_leaves = sorted(
            position_leaves,
            key=lambda x: x[1].symbol,
        )

        for (
            position_key_h256,
            position_identifier,
            position_leaf_value,
        ) in sorted_position_leaves:
            # Obtain latest mark price
            mark_price = kwargs["latest_price_leaves"][position_identifier.symbol][
                1
            ].mark_price

            # Compute unrealized PNL for position
            pnl_multiplier = (
                Decimal("1") if position_leaf_value.side == "Long" else Decimal("-1")
            )
            unrealized_pnl = (
                position_leaf_value.balance
                * (mark_price - position_leaf_value.avg_entry_price)
                * pnl_multiplier
            )

            # Construct a StrategyIdentifier and corresponding encoded
            # key
            strategy_identifier = StrategyIdentifier.decode_position_key_into_cls(
                h256_to_bytes_array(position_key_h256)
            )
            strategy_key = strategy_identifier.encoded_key
            strategy_key_h256 = bytes_array_to_h256(strategy_key)

            # Get the Strategy leaf given the key from above
            strategy_leaf = smt.get(strategy_key_h256)

            # Credit/debit the trader's Strategy leaf by the
            # unrealized PNL to settle
            strategy_leaf.free_collateral[kwargs["usdc_address"]] = round_to_unit(
                strategy_leaf.free_collateral[kwargs["usdc_address"]] + unrealized_pnl
            )

            # Update the SMT with the H256 repr of the key and
            # the Strategy leaf
            smt.update(strategy_key_h256, strategy_leaf)

            # Place the Strategy leaf update message on the
            # queue to consider sending to the Trader
            place_message_in_queue(
                trader_auditor_queue,
                suppress_trader_queue,
                ItemMessage(
                    strategy_identifier.topic_string,
                    WebsocketEventType.UPDATE,
                    {"t": strategy_identifier.topic_string, "c": strategy_leaf},
                    self,
                ),
            )

            # Set the Position's average entry price to the mark price
            # since settlement has just taken place
            position_leaf_value.avg_entry_price = mark_price

            # Update the SMT with the H256 repr of the key and
            # the Position leaf
            smt.update(position_key_h256, position_leaf_value)

            # Place the Position leaf update message on the
            # queue to consider sending to the Trader
            place_message_in_queue(
                trader_auditor_queue,
                suppress_trader_queue,
                ItemMessage(
                    position_identifier.topic_string,
                    WebsocketEventType.UPDATE,
                    {"t": position_identifier.topic_string, "c": position_leaf_value},
                    self,
                ),
            )

    def repr_json(self):
        return {
            "eventType": EventType.PNL_SETTLEMENT,
            "requestIndex": self.request_index,
            "settlementEpochId": self.settlement_epoch_id,
            "timeValue": self.time_value,
        }

    def __repr__(self):
        return f"PnlSettlement (event): request_index = {self.request_index}; settlement_epoch_id = {self.settlement_epoch_id}; time_value = {self.time_value}"
