"""
Leave module
"""
import asyncio
from typing import Dict

from trading_clients.auditor.smt.smt import SparseMerkleTree
from trading_clients.auditor.state.empty import Empty
from trading_clients.auditor.state.identifiers.member_identifier import MemberIdentifier
from trading_clients.auditor.transactions.event_types import EventType
from trading_clients.auditor.transactions.membership_change import MembershipChange
from trading_clients.auditor.utils import bytes_array_to_h256


class Leave(MembershipChange):
    """
    Defines an Leave
    """

    def __init__(
        self,
        action: str,
        node_id: int,
        request_index: int,
    ):
        """
        Initialize a Leave MembershipChange transaction.

        Parameters
        ----------
        action : str
           Type of MembershipChange (Change | Leave)
        node_id : str
           The operator's unique identifier within the Raft
        request_index : int
            Sequenced request index of transaction
        """

        super().__init__(action, node_id, request_index)

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a Leave
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        leave_tx_event = raw_tx_log_event["event"]["c"]

        return cls(
            leave_tx_event["action"],
            leave_tx_event["nodeId"],
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a Leave transaction of type MembershipChange. This
        indicates the an exit from the raft.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to Post transactions
        """

        # Construct a MemberIdentifier and corresponding encoded key
        member_identifier = MemberIdentifier(self.node_id)
        member_key = member_identifier.encoded_key
        member_key_h256 = bytes_array_to_h256(member_key)

        # Update the SMT with the H256 repr of the key and the Member
        # leaf
        smt.update(member_key_h256, Empty())

    def repr_json(self):
        return {
            "eventType": EventType.MEMBERSHIP_CHANGE,
            "requestIndex": self.request_index,
            "action": self.action,
            "nodeId": self.node_id,
        }

    def __repr__(self):
        return f"Leave (event): request_index = {self.request_index}; action: {self.action}; node_id: {self.node_id}"
