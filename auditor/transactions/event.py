"""
Event module
"""
import asyncio
from typing import Dict

from ddx_client.auditor.smt.smt import SparseMerkleTree


class Event:
    """
    An Event class from which all Transaction classes inherit
    """

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into an instance of
        the class.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        raise NotImplementedError()

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs
    ):
        """
        Process transaction log event by modifying the SMT state
        and emitting any corresponding events to the Trader when
        appropriate.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to various transaction types
        """

        raise NotImplementedError()
