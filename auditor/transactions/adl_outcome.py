"""
AdlOutcome module
"""

from ddx_python.decimal import Decimal

from ddx_client.auditor.transactions.event import Event
from ddx_client.auditor.transactions.event_types import EventType


class AdlOutcome(Event):
    """
    Defines an AdlOutcome
    """

    def __init__(
        self,
        trader_address: str,
        strategy_id: str,
        request_index: int,
    ):
        """
        Initialize an AdlOutcome. An AdlOutcome represents a scenario
        where a strategy has been auto-deleveraged due to a liquidation.

        Parameters
        ----------
        trader_address : str
            Auto-deleveraged trader's ethereum address
        strategy_id : str
            Auto-deleveraged strategy ID
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.trader_address = trader_address
        self.strategy_id = strategy_id

    def repr_json(self):
        return {
            "eventType": EventType.LIQUIDATION,
            "requestIndex": self.request_index,
            "traderAddress": self.trader_address,
            "strategyId": self.strategy_id,
        }

    def __repr__(self):
        return f"AdlOutcome (event)"

    @property
    def metrics_repr(self):
        return {
            "eventType": EventType.LIQUIDATION,
            "requestIndex": self.request_index,
            "traderAddress": self.trader_address,
            "strategyId": self.strategy_id,
        }
