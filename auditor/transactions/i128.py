"""
PriceCheckpointEMA module
"""

from ddx_python.decimal import Decimal


class I128:
    """
    Defines an I128
    """

    def __init__(
        self,
        absolute_value: Decimal,
        negative_sign: bool,
    ):
        """
        Initialize an I128. This holds information including the absolute
        value and whether it is positive/negative.

        Parameters
        ----------
        absolute_value : Decimal
           Absolute value / raw magnitude
        negative_sign: bool
           Whether positive (False) or negative (True)
        """

        self.absolute_value = absolute_value
        self.negative_sign = negative_sign

    @property
    def value(self):
        return self.absolute_value if not self.negative_sign else -self.absolute_value

    def repr_json(self):
        return {
            "absoluteValue": str(self.absolute_value),
            "negativeSign": self.negative_sign,
        }

    def __str__(self):
        return f"I128: absolute_value = {self.absolute_value}; negative_sign = {self.negative_sign}"
