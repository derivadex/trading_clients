"""
InsuranceFundUpdate
"""
import asyncio
from ddx_python.decimal import Decimal
from typing import Dict

from ddx_client.auditor.smt.h256 import H256
from ddx_client.auditor.smt.smt import SparseMerkleTree
from ddx_client.auditor.state.empty import Empty
from ddx_client.auditor.state.identifiers.insurance_fund_contribution_identifier import (
    InsuranceFundContributionIdentifier,
)
from ddx_client.auditor.state.identifiers.organic_insurance_fund_identifier import (
    OrganicInsuranceFundIdentifier,
)
from ddx_client.auditor.state.insurance_fund_contribution import (
    InsuranceFundContribution,
)
from ddx_client.auditor.transactions.event import Event
from ddx_client.auditor.transactions.event_types import EventType
from ddx_client.auditor.utils import (
    bytes_array_to_h256,
    insurance_fund_update_kind_to_int,
    insurance_fund_update_int_to_kind,
    h256_to_bytes_array,
)


class InsuranceFundUpdate(Event):
    """
    Defines an InsuranceFundUpdate
    """

    def __init__(
        self,
        address: str,
        collateral_address: str,
        amount: Decimal,
        update_kind: int,
        tx_hash: str,
        request_index: int,
    ):
        """
        Initialize an InsuranceFundUpdate transaction. An
        InsuranceFundUpdate is an update to a trader's insurance fund
        contribution(such as depositing or withdrawing
        collateral).

        Parameters
        ----------
        address : str
           Trader's Ethereum address this insurance fund update belongs to
        collateral_address: str
           Collateral's Ethereum address a deposit/withdrawal has been
           made with
        amount: Decimal
           The amount of collateral deposited or withdrawn
        update_kind: int
           Update kind (Deposit=0, Withdraw=1)
        tx_hash: str
           The Ethereum transaction's hash
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.address = address
        self.collateral_address = collateral_address
        self.amount = amount
        self.update_kind = update_kind
        self.tx_hash = tx_hash

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into an
        InsuranceFundUpdate instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        insurance_fund_update_tx_event = raw_tx_log_event["event"]["c"]

        return cls(
            insurance_fund_update_tx_event["address"],
            insurance_fund_update_tx_event["collateralAddress"],
            Decimal(insurance_fund_update_tx_event["amount"]),
            insurance_fund_update_kind_to_int(
                insurance_fund_update_tx_event["updateKind"]
            ),
            insurance_fund_update_tx_event["txHash"],
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process an InsuranceFundUpdate transaction. An
        InsuranceFundUpdate consists of information relating to updates
        for a trader's insurance fund contribution, such
        as when their free or frozen balance has changed due to a
        deposit or withdrawal. This will update the
        InsuranceFundContribution leaf in the SMT.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to InsuranceFundUpdate transactions
        """

        # Construct an InsuranceFundContributionIdentifier and
        # corresponding encoded key
        insurance_fund_contribution_identifier = InsuranceFundContributionIdentifier(
            self.address,
        )
        insurance_fund_contribution_key = (
            insurance_fund_contribution_identifier.encoded_key
        )
        insurance_fund_contribution_key_h256 = bytes_array_to_h256(
            insurance_fund_contribution_key
        )

        # Get the InsuranceFundContribution leaf given the key above
        insurance_fund_contribution_leaf = smt.get(insurance_fund_contribution_key_h256)

        # Construct an OrganicInsuranceFundIdentifier and
        # corresponding encoded key
        organic_insurance_fund_identifier = OrganicInsuranceFundIdentifier()
        organic_insurance_fund_key = organic_insurance_fund_identifier.encoded_key
        organic_insurance_fund_key_h256 = bytes_array_to_h256(
            organic_insurance_fund_key
        )
        organic_insurance_fund_leaf = smt.get(organic_insurance_fund_key_h256)

        if self.update_kind == 0:
            # If InsuranceFundUpdate is of deposit type

            if insurance_fund_contribution_leaf == H256.zero():
                # If we haven't yet seen the InsuranceFundContribution
                # leaf, create a new one
                insurance_fund_contribution_leaf = InsuranceFundContribution(
                    {self.collateral_address: self.amount},
                    {},
                )
                if (
                    self.collateral_address
                    not in organic_insurance_fund_leaf.capitalization
                ):
                    organic_insurance_fund_leaf.capitalization[
                        self.collateral_address
                    ] = self.amount
                else:
                    organic_insurance_fund_leaf.capitalization[
                        self.collateral_address
                    ] += self.amount
            else:
                # Adjust the existing strategy leaf by incrementing the
                # free collateral by the amount in the deposit event
                if (
                    self.collateral_address
                    not in insurance_fund_contribution_leaf.free_balance
                ):
                    insurance_fund_contribution_leaf.free_balance[
                        self.collateral_address
                    ] = self.amount
                else:
                    insurance_fund_contribution_leaf.free_balance[
                        self.collateral_address
                    ] += self.amount

                if (
                    self.collateral_address
                    not in organic_insurance_fund_leaf.capitalization
                ):
                    organic_insurance_fund_leaf.capitalization[
                        self.collateral_address
                    ] = self.amount
                else:
                    organic_insurance_fund_leaf.capitalization[
                        self.collateral_address
                    ] += self.amount

            # Update the SMT with the H256 repr of the key and the
            # InsuranceFundContribution leaf
            smt.update(
                insurance_fund_contribution_key_h256, insurance_fund_contribution_leaf
            )
            smt.update(organic_insurance_fund_key_h256, organic_insurance_fund_leaf)
        else:
            # If InsuranceFundUpdate is of withdrawal type
            if (
                insurance_fund_contribution_leaf == H256.zero()
                or isinstance(insurance_fund_contribution_leaf, Empty)
                or insurance_fund_contribution_leaf.frozen_balance[
                    self.collateral_address
                ]
                < self.amount
            ):
                raise Exception(
                    "InsuranceFundContribution leaf either non-existent or insufficiently capitalized to facilitate withdrawal"
                )

            # Adjust the existing InsuranceFundContribution leaf by decrementing the
            # free balance by the amount in the withdrawal event
            insurance_fund_contribution_leaf.frozen_balance[
                self.collateral_address
            ] -= self.amount
            if insurance_fund_contribution_leaf.frozen_balance[
                self.collateral_address
            ] == Decimal("0"):
                del insurance_fund_contribution_leaf.frozen_balance[
                    self.collateral_address
                ]

            # Update the SMT with the H256 repr of the key and the
            # Strategy leaf
            smt.update(
                insurance_fund_contribution_key_h256, insurance_fund_contribution_leaf
            )

    def repr_json(self):
        return {
            "eventType": EventType.INSURANCE_FUND_UPDATE,
            "requestIndex": self.request_index,
            "address": self.address,
            "collateralAddress": self.collateral_address,
            "amount": str(self.amount),
            "updateKind": self.update_kind,
            "txHash": self.tx_hash,
        }

    def __repr__(self):
        return (
            f"InsuranceFundUpdate (event): "
            f"request_index = {self.request_index}; "
            f"address = {self.address}; "
            f"collateral_address = {self.collateral_address}; "
            f"amount: {self.amount}; "
            f"update_kind: {self.update_kind}; "
            f"tx_hash: {self.tx_hash}"
        )

    @property
    def metrics_repr(self):
        return {
            "eventType": EventType.INSURANCE_FUND_UPDATE,
            "requestIndex": self.request_index,
            "address": self.address,
            "collateralAddress": self.collateral_address,
            "amount": self.amount,
            "updateKind": insurance_fund_update_int_to_kind(self.update_kind),
            "txHash": self.tx_hash,
        }
