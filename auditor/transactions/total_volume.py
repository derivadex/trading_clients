"""
TotalVolume module
"""

from ddx_python.decimal import Decimal


class TotalVolume:
    """
    Defines a TotalVolume
    """

    def __init__(
        self,
        maker_volume: Decimal,
        taker_volume: Decimal,
    ):
        """
        Initialize a TotalVolume (a part of the TradeMining
        transaction). This holds information including the maker and
        taker volumes.

        Parameters
        ----------
        maker_volume : Decimal
           Total maker volume
        taker_volume : Decimal
           Total maker volume
        """

        self.maker_volume = maker_volume
        self.taker_volume = taker_volume

    def repr_json(self):
        return {
            "makerVolume": str(self.maker_volume),
            "takerVolume": str(self.taker_volume),
        }

    def __str__(self):
        return f"TotalVolume (event): maker_volume = {self.maker_volume}; taker_volume = {self.taker_volume}"
