"""
LiquidatedPosition module
"""

from ddx_python.decimal import Decimal
from typing import Dict, List, Union

from ddx_client.auditor.transactions.adl_outcome import AdlOutcome
from ddx_client.auditor.transactions.cancel import Cancel
from ddx_client.auditor.transactions.event import Event
from ddx_client.auditor.transactions.event_types import EventType
from ddx_client.auditor.transactions.fill import Fill
from ddx_client.auditor.transactions.i128 import I128
from ddx_client.auditor.transactions.liquidation_fill import LiquidationFill
from ddx_client.auditor.transactions.price_checkpoint import PriceCheckpoint


class LiquidatedPosition(Event):
    """
    Defines a LiquidatedPosition
    """

    def __init__(
        self,
        amount: Decimal,
        trade_outcomes: List[Union[LiquidationFill, Cancel]],
        adl_outcomes: List[AdlOutcome],
        new_insurance_fund_cap: Decimal,
        request_index: int,
    ):
        """
        Initialize a LiquidatedPosition transaction. A LiquidatedPosition
        has data pertaining to a liquidated position.

        Parameters
        ----------
        amount : Decimal
            Liquidated balance amount
        trade_outcomes : List[Union[LiquidationFill, Cancel]]
            A list of trade outcome objects
        adl_outcomes : List[AdlOutcome]
            Positions that were ADL'd as a result of the liquidation
        new_insurance_fund_cap : Decimal
            Insurance fund capitalization after the liquidation
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.amount = amount
        self.trade_outcomes = trade_outcomes
        self.adl_outcomes = adl_outcomes
        self.new_insurance_fund_cap = new_insurance_fund_cap

    def repr_json(self):
        return {
            "eventType": EventType.LIQUIDATION,
            "requestIndex": self.request_index,
            "amount": str(self.amount),
            "trade_outcomes": self.trade_outcomes,
            "adlOutcomes": self.adl_outcomes,
            "newInsuranceFundCap": self.new_insurance_fund_cap,
        }

    def __repr__(self):
        return f"LiquidatedPosition (event)"
