"""
PostOrder module
"""
import asyncio
from typing import Dict, List
from ddx_python.decimal import Decimal

from ddx_client.auditor.smt.smt import SparseMerkleTree
from ddx_client.auditor.state.book_order import BookOrder
from ddx_client.auditor.state.identifiers.book_order_identifier import (
    BookOrderIdentifier,
)
from ddx_client.auditor.state.identifiers.strategy_identifier import (
    StrategyIdentifier,
)
from ddx_client.auditor.state.item_message import ItemMessage
from ddx_client.auditor.transactions.cancel import Cancel
from ddx_client.auditor.transactions.event import Event
from ddx_client.auditor.transactions.event_message import EventMessage
from ddx_client.auditor.transactions.event_types import EventType
from ddx_client.auditor.transactions.identifiers.post_identifier import (
    PostIdentifier,
)
from ddx_client.auditor.shared.order_book import OrderBook
from ddx_client.auditor.transactions.post import Post
from ddx_client.auditor.transactions.price_checkpoint import PriceCheckpoint
from ddx_client.auditor.utils import bytes_array_to_h256, to_camel_case
from ddx_client.client.asyncio_utils import place_message_in_queue
from ddx_client.client.websocket_message import WebsocketEventType


class PostOrder(Event):
    """
    Defines a PostOrder
    """

    def __init__(
        self,
        post: Post,
        price_checkpoints: List[PriceCheckpoint],
        canceled_orders: List[Cancel],
        request_index: int,
    ):
        """
        Initialize a PostOrder transaction. A PostOrder is an order that enters
        the order book along with any canceled maker orders.

        Parameters
        ----------
        post : Post
           The posted order.
        price_checkpoints : PriceCheckpoint
           The price checkpoints that make up this transaction
        canceled_orders: List[Cancel]
           Canceled maker orders as a result of the posted order
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.post = post
        self.price_checkpoints = price_checkpoints
        self.canceled_orders = canceled_orders

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a PostOrder
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        post_tx_event = raw_tx_log_event["event"]["c"][0]
        all_price_checkpoint_tx_event = raw_tx_log_event["event"]["c"][1]
        cancel_orders_tx_event = raw_tx_log_event["event"]["c"][2]

        return cls(
            Post(
                post_tx_event["symbol"],
                post_tx_event["orderHash"],
                post_tx_event["side"],
                Decimal(post_tx_event["amount"]),
                Decimal(post_tx_event["price"]),
                post_tx_event["traderAddress"],
                post_tx_event["strategyId"],
                post_tx_event["bookOrdinal"],
                raw_tx_log_event["timeValue"],
                to_camel_case(EventType.POST_ORDER.name),
                raw_tx_log_event["requestIndex"],
            ),
            [
                PriceCheckpoint(
                    all_price_checkpoints_key,
                    Decimal(all_price_checkpoints_val["ema"]),
                    all_price_checkpoints_val["indexPriceHash"],
                    Decimal(all_price_checkpoints_val["indexPrice"]),
                    all_price_checkpoints_val["ordinal"],
                    raw_tx_log_event["requestIndex"],
                )
                for all_price_checkpoints_key, all_price_checkpoints_val in all_price_checkpoint_tx_event.items()
            ],
            [
                Cancel(
                    canceled_order["Cancel"]["symbol"],
                    canceled_order["Cancel"]["orderHash"],
                    Decimal(canceled_order["Cancel"]["amount"]),
                    to_camel_case(EventType.POST_ORDER.name),
                    raw_tx_log_event["requestIndex"],
                )
                for canceled_order in cancel_orders_tx_event
            ],
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a PostOrder transaction. We will need to create a new
        BookOrder leaf with this information.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to PostOrder transactions
        """

        # Loop through each price checkpoint event and process them
        # individually
        for price_checkpoint in self.price_checkpoints:
            price_checkpoint.process_tx(
                smt, trader_auditor_queue, suppress_trader_queue, **kwargs
            )

        # Loop through each cancel event and process them individually
        for canceled_order in self.canceled_orders:
            canceled_order.process_tx(
                smt, trader_auditor_queue, suppress_trader_queue, **kwargs
            )

        # Process the post event
        self.post.process_tx(smt, trader_auditor_queue, suppress_trader_queue, **kwargs)

    def __repr__(self):
        return f"PostOrder (event): request_index = {self.request_index}; post = {self.post}; price_checkpoints = {self.price_checkpoints}; canceled_orders = {self.canceled_orders}"
