"""
MembershipChange module
"""

from typing import Dict

from ddx_client.auditor.transactions.event import Event


class MembershipChange(Event):
    """
    Defines an MembershipChange
    """

    def __init__(
        self,
        action: str,
        node_id: int,
        request_index: int,
    ):
        """
        Initialize a MembershipChange transaction.

        Parameters
        ----------
        action : str
           Type of MembershipChange (Change | Leave)
        node_id : str
           The operator's unique identifier within the Raft
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.action = action
        self.node_id = node_id

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        raise NotImplementedError
