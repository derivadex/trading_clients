"""
Withdraw module
"""
import asyncio
from ddx_python.decimal import Decimal
from typing import Dict

from ddx_client.auditor.smt.h256 import H256
from ddx_client.auditor.smt.smt import SparseMerkleTree
from ddx_client.auditor.state.identifiers.strategy_identifier import (
    StrategyIdentifier,
)
from ddx_client.auditor.state.identifiers.trader_identifier import TraderIdentifier
from ddx_client.auditor.state.item_message import ItemMessage
from ddx_client.auditor.state.strategy import Strategy
from ddx_client.auditor.state.trader import Trader
from ddx_client.auditor.transactions.event import Event
from ddx_client.auditor.transactions.event_types import EventType
from ddx_client.auditor.utils import bytes_array_to_h256
from ddx_client.client.asyncio_utils import place_message_in_queue
from ddx_client.client.websocket_message import WebsocketEventType


class Withdraw(Event):
    """
    Defines a Withdraw Update
    """

    def __init__(
        self,
        signer_address: str,
        recipient_address: str,
        strategy_id: str,
        collateral_address: str,
        amount: Decimal,
        request_index: int,
    ):
        """
        Initialize a Withdraw transaction. A Withdraw is when
        a withdrawal of collateral is signaled).

        Parameters
        ----------
        signer_address : str
           Signer's Ethereum address withdrawal is taking place from
        recipient_address: str
           Trader address DDX is being withdrawn to
        amount: Decimal
           The amount of DDX being withdrawn
        strategy_id: str
           Cross-margined strategy ID for which this withdrawal applies
        collateral_address: str
           Collateral ERC-20 token address being withdrawn
        request_index : int
           Sequenced request index of transaction
        """

        self.request_index = request_index
        self.signer_address = signer_address
        self.recipient_address = recipient_address
        self.strategy_id = strategy_id
        self.collateral_address = collateral_address
        self.amount = amount

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a Withdraw
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        withdraw_tx_event = raw_tx_log_event["event"]["c"]

        return cls(
            withdraw_tx_event["signerAddress"],
            withdraw_tx_event["recipientAddress"],
            withdraw_tx_event["strategy"],
            withdraw_tx_event["currency"],
            Decimal(withdraw_tx_event["amount"]),
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a Withdraw transaction. A Withdraw event consists
        of consists of information relating to withdrawal of collateral.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to Post transactions
        """

        # Construct a StrategyIdentifier for the withdrawal signer (the
        # address funds are being withdrawn from) and corresponding
        # encoded key
        signer_identifier = StrategyIdentifier(
            self.signer_address,
            f"{StrategyIdentifier.generate_strategy_id_hash(self.strategy_id).hex()}",
        )
        signer_key = signer_identifier.encoded_key
        signer_key_h256 = bytes_array_to_h256(signer_key)

        # Get the Strategy leaf given the key above
        signer_leaf: Strategy = smt.get(signer_key_h256)

        # Decrement the free balance by the withdrawn amount
        signer_leaf.free_collateral[self.collateral_address] -= self.amount
        if signer_leaf.free_collateral[self.collateral_address] == Decimal("0"):
            del signer_leaf.free_collateral[self.collateral_address]

        # Update the SMT with the H256 repr of the key and
        # the Strategy leaf for the signer
        smt.update(signer_key_h256, signer_leaf)

        # Place the Signer Strategy leaf update message on the queue to
        # consider sending to the Trader
        place_message_in_queue(
            trader_auditor_queue,
            suppress_trader_queue,
            ItemMessage(
                signer_identifier.topic_string,
                WebsocketEventType.UPDATE,
                {"t": signer_identifier.topic_string, "c": signer_leaf},
                self,
            ),
        )

        # Construct a StrategyIdentifier for the withdrawal recipient (the
        # address funds are being withdrawn to) and corresponding
        # encoded key
        recipient_identifier = StrategyIdentifier(
            self.recipient_address,
            f"{StrategyIdentifier.generate_strategy_id_hash(self.strategy_id).hex()}",
        )
        recipient_key = recipient_identifier.encoded_key
        recipient_key_h256 = bytes_array_to_h256(recipient_key)

        # Get the Strategy leaf given the key above
        recipient_leaf: Strategy = smt.get(recipient_key_h256)

        # Construct a TraderIdentifier for the withdrawal recipient (the
        # address funds are being withdrawn to) and corresponding
        # encoded key
        recipient_trader_identifier = TraderIdentifier(self.recipient_address)
        recipient_trader_key = recipient_trader_identifier.encoded_key
        recipient_trader_key_h256 = bytes_array_to_h256(recipient_trader_key)

        # Get the Trader leaf given the key above
        recipient_trader_leaf: Trader = smt.get(recipient_trader_key_h256)

        if recipient_trader_leaf == H256.zero():
            # Initialize a new Trader Leaf
            recipient_trader_leaf = Trader(
                Decimal("0"),
                Decimal("0"),
                "0x0000000000000000000000000000000000000000",
                False,
            )

            # Update the SMT with the H256 repr of the key and the
            # Trader leaf
            smt.update(recipient_trader_key_h256, recipient_trader_leaf)

        if recipient_leaf == H256.zero():
            # If we haven't yet seen the Strategy leaf, create a new
            # one
            recipient_leaf = Strategy(
                self.strategy_id,
                {},
                {},
                3,
                False,
            )

        # Increment the frozen balance by the withdrawn amount
        if self.collateral_address not in recipient_leaf.frozen_collateral:
            recipient_leaf.frozen_collateral[self.collateral_address] = self.amount
        else:
            recipient_leaf.frozen_collateral[self.collateral_address] += self.amount

        # Update the SMT with the H256 repr of the key and
        # the Strategy leaf for the recipient
        smt.update(recipient_key_h256, recipient_leaf)

        # Place the Trader Strategy leaf update message on the queue to
        # consider sending to the Trader
        place_message_in_queue(
            trader_auditor_queue,
            suppress_trader_queue,
            ItemMessage(
                recipient_identifier.topic_string,
                WebsocketEventType.UPDATE,
                {"t": recipient_identifier.topic_string, "c": signer_leaf},
                self,
            ),
        )

    def repr_json(self):
        return {
            "eventType": EventType.WITHDRAW,
            "requestIndex": self.request_index,
            "signerAddress": self.signer_address,
            "recipientAddress": self.recipient_address,
            "strategyId": self.strategy_id,
            "collateralAddress": self.collateral_address,
            "amount": str(self.amount),
        }

    def __repr__(self):
        return (
            f"Withdraw(event): "
            f"request_index = {self.request_index}; "
            f"signer_address = {self.signer_address}; "
            f"recipient_address = {self.recipient_address}; "
            f"strategy_id = {self.strategy_id}; "
            f"collateral_address = {self.collateral_address}; "
            f"amount: {self.amount};"
        )
