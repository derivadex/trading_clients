"""
DisasterRecovery module
"""
import asyncio
from typing import Dict, List, Tuple
from ddx_python.decimal import Decimal

from ddx_client.auditor.smt.h256 import H256
from ddx_client.auditor.smt.smt import SparseMerkleTree
from ddx_client.auditor.state.book_order import BookOrder
from ddx_client.auditor.state.empty import Empty
from ddx_client.auditor.state.identifiers.book_order_identifier import (
    BookOrderIdentifier,
)
from ddx_client.auditor.state.identifiers.position_identifier import PositionIdentifier
from ddx_client.auditor.state.identifiers.strategy_identifier import (
    StrategyIdentifier,
)
from ddx_client.auditor.state.position import Position
from ddx_client.auditor.transactions.event import Event
from ddx_client.auditor.shared.order_book import OrderBook
from ddx_client.auditor.utils import (
    bytes_array_to_h256,
    h256_to_bytes_array,
    round_to_unit,
)


class DisasterRecovery(Event):
    """
    Defines a DisasterRecovery
    """

    def __init__(self, request_index: int):
        """
        Initialize a DisasterRecovery transaction. A DisasterRecovery is when
        the system is wound down in an extreme recovery scenario.

        Parameters
        ----------
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a DisasterRecovery
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        return cls(
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a DisasterRecovery transaction.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to PostOrder transactions
        """

        def get_unrealized_pnl_for_position_item(position_item):
            position_key_h256, position_leaf_value = position_item
            # Derive PositionIdentifier from the key
            position_identifier = PositionIdentifier.decode_key_into_cls(
                h256_to_bytes_array(position_key_h256)
            )

            # Obtain latest mark price
            mark_price = kwargs["latest_price_leaves"][position_identifier.symbol][
                1
            ].mark_price

            # Compute unrealized PNL for position
            pnl_multiplier = (
                Decimal("1") if position_leaf_value.side == "Long" else Decimal("-1")
            )
            return (
                position_leaf_value.balance
                * (mark_price - position_leaf_value.avg_entry_price)
                * pnl_multiplier
            )

        # Derive a PositionIdentifier and encoded key to obtain all
        # Position leaves
        position_identifier = PositionIdentifier(None, None, None)
        position_key_prefix = position_identifier.encoded_key

        # Obtain the Position leaves that adhere to the prefix computed
        # above
        position_leaves: List[Tuple[H256, Position]] = [
            (node.key, node.value)
            for node_hash, node in smt.store().leaves_map().items()
            if h256_to_bytes_array(node.key)[: len(position_key_prefix)]
            == position_key_prefix
        ]

        sorted_position_leaves = sorted(
            position_leaves,
            key=lambda x: get_unrealized_pnl_for_position_item(x),
            reverse=True,
        )

        for position_key_h256, position_leaf_value in sorted_position_leaves:
            # Derive PositionIdentifier from the key
            position_identifier = PositionIdentifier.decode_key_into_cls(
                h256_to_bytes_array(position_key_h256)
            )

            # Obtain latest mark price
            mark_price = kwargs["latest_price_leaves"][position_identifier.symbol][
                1
            ].mark_price

            # Compute unrealized PNL for position
            pnl_multiplier = (
                Decimal("1") if position_leaf_value.side == "Long" else Decimal("-1")
            )
            unrealized_pnl = (
                position_leaf_value.balance
                * (mark_price - position_leaf_value.avg_entry_price)
                * pnl_multiplier
            )

            # Construct a StrategyIdentifier and corresponding encoded
            # key
            strategy_identifier = StrategyIdentifier.decode_position_key_into_cls(
                h256_to_bytes_array(position_key_h256)
            )
            strategy_key = strategy_identifier.encoded_key
            strategy_key_h256 = bytes_array_to_h256(strategy_key)

            # Get the Strategy leaf given the key from above
            strategy_leaf = smt.get(strategy_key_h256)

            # Credit/debit the trader's Strategy leaf by the
            # unrealized PNL to settle
            strategy_leaf.free_collateral[kwargs["usdc_address"]] = round_to_unit(
                strategy_leaf.free_collateral[kwargs["usdc_address"]] + unrealized_pnl
            )

            # Update the SMT with the H256 repr of the key and
            # the Strategy leaf
            smt.update(strategy_key_h256, strategy_leaf)

            # Update the SMT with the H256 repr of the key and
            # the Position leaf
            smt.update(position_key_h256, Empty())

        # Derive a BookOrder and encoded key to obtain all
        # BookOrder leaves
        book_order_identifier = BookOrderIdentifier(None, None, None, None)
        book_order_key_prefix = book_order_identifier.encoded_key

        # Obtain the BookOrder leaves that adhere to the prefix computed
        # above
        book_order_leaves: List[Tuple[H256, BookOrder]] = [
            (node.key, node.value)
            for node_hash, node in smt.store().leaves_map().items()
            if h256_to_bytes_array(node.key)[: len(book_order_key_prefix)]
            == book_order_key_prefix
        ]

        for book_order_key_h256, book_order_leaf_value in book_order_leaves:
            # Update the SMT with the H256 repr of the key and
            # the BookOrder leaf
            smt.update(book_order_key_h256, Empty())

        # Reinitialize order books
        for symbol in kwargs["order_books"]:
            kwargs["order_books"][symbol] = OrderBook(smt)

    def __repr__(self):
        return f"DisasterRecovery (event): request_index = {self.request_index}"
