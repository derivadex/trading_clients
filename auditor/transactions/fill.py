"""
Fill module
"""
import asyncio
from ddx_python.decimal import Decimal
from typing import Dict

from ddx_client.auditor.smt.h256 import H256
from ddx_client.auditor.smt.smt import SparseMerkleTree
from ddx_client.auditor.state.empty import Empty
from ddx_client.auditor.state.identifiers.organic_insurance_fund_identifier import (
    OrganicInsuranceFundIdentifier,
)
from ddx_client.auditor.state.identifiers.position_identifier import (
    PositionIdentifier,
)
from ddx_client.auditor.state.identifiers.stats_identifier import StatsIdentifier
from ddx_client.auditor.state.identifiers.strategy_identifier import (
    StrategyIdentifier,
)
from ddx_client.auditor.state.identifiers.trader_identifier import TraderIdentifier
from ddx_client.auditor.state.item_message import ItemMessage
from ddx_client.auditor.state.organic_insurance_fund import OrganicInsuranceFund
from ddx_client.auditor.state.position import Position
from ddx_client.auditor.state.stats import Stats
from ddx_client.auditor.transactions.event import Event
from ddx_client.auditor.transactions.outcome import Outcome
from ddx_client.auditor.utils import (
    bytes_array_to_h256,
    round_to_unit,
    get_side_for_order_and_trader,
    compute_avg_pnl,
)
from ddx_client.client.asyncio_utils import place_message_in_queue
from ddx_client.client.websocket_message import WebsocketEventType
from ddx_client.auditor.state.identifiers.fee_pool_identifier import (
    FeePoolIdentifier,
)


class Fill(Event):
    """
    Defines a Fill
    """

    def __init__(
        self,
        reason: str,
        symbol: str,
        maker_order_hash: str,
        maker_order_remaining_amount: Decimal,
        amount: Decimal,
        price: Decimal,
        taker_side: str,
        maker_outcome: Outcome,
        request_index: int,
    ):
        self.request_index = request_index
        self.reason = reason
        self.symbol = symbol
        self.maker_order_hash = maker_order_hash
        self.maker_order_remaining_amount = maker_order_remaining_amount
        self.amount = amount
        self.price = price
        self.taker_side = taker_side
        self.maker_outcome = maker_outcome

    @property
    def taker_is_bid(self):
        return self.taker_side == "Bid"

    def compute_avg_entry_price(self, position_leaf: Position):
        return round_to_unit(
            (
                position_leaf.avg_entry_price * position_leaf.balance
                + self.price * self.amount
            )
            / (position_leaf.balance + self.amount)
        )

    def increase(
        self,
        position_leaf: Position,
    ) -> Decimal:
        position_leaf.avg_entry_price = self.compute_avg_entry_price(position_leaf)
        position_leaf.balance += self.amount
        return Decimal("0")

    def decrease(
        self,
        position_leaf: Position,
    ) -> Decimal:
        pnl = self.amount * compute_avg_pnl(position_leaf, self.price)
        position_leaf.balance -= self.amount
        return pnl

    def cross_over(
        self,
        position_leaf: Position,
    ) -> Decimal:
        pnl = position_leaf.balance * compute_avg_pnl(position_leaf, self.price)
        position_leaf.side = "Long" if position_leaf.side == "Short" else "Short"
        position_leaf.balance = self.amount - position_leaf.balance
        position_leaf.avg_entry_price = self.price
        return pnl

    def adjust_for_maker_taker(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        maker_book_order_time_value: int,
        is_maker: bool,
        trade_mining_active: bool,
        usdc_address: str,
        ddx_address: str,
    ):
        """
        Make some adjustments to the SMT based on whether we are
        considering the maker or the taker component of the Trade.
        In this method, we will be adjusting the the Strategy,
        Position, Trader, and Stats leaves.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        maker_book_order_time_value: int
            Maker book order time value
        is_maker : bool
            Whether outcome is for the maker or taker
        trade_mining_active : bool
            Whether trade mining is active
        usdc_address : str
            USDC ERC-20 token contract address
        ddx_address : str
            DDX ERC-20 token contract address
        """

        if not is_maker and self.reason != "Trade":
            raise Exception("No taker for Liquidation")

        # Determine outcome based on whether we are considering maker or
        # taker
        outcome: Outcome = self.maker_outcome if is_maker else self.taker_outcome

        # Construct a PositionIdentifier and corresponding encoded
        # key
        position_identifier = PositionIdentifier(
            self.symbol,
            outcome.trader_address,
            f"{StrategyIdentifier.generate_strategy_id_hash(outcome.strategy_id).hex()}",
        )
        position_key = position_identifier.encoded_key
        position_key_h256 = bytes_array_to_h256(position_key)

        # Get the Position leaf using the key from above
        position_leaf: [Position, Empty] = smt.get(position_key_h256)
        order_side = get_side_for_order_and_trader(self.taker_side, is_maker)
        if position_leaf == H256.zero():
            # If Position leaf doesn't exist in the tree, we need
            # to create/add a new one

            position_leaf = Position(
                "Long" if order_side == "Bid" else "Short", Decimal("0"), Decimal("0")
            )
            pnl = self.increase(position_leaf)
        else:
            if (order_side == "Bid" and position_leaf.side == "Long") or (
                order_side == "Ask" and position_leaf.side == "Short"
            ):
                pnl = self.increase(position_leaf)
            else:
                # order_side == "Bid" and position_leaf.side == "Short"
                # order_side == "Ask" and position_leaf.side == "Long"
                if self.amount > position_leaf.balance:
                    pnl = self.cross_over(position_leaf)
                else:
                    pnl = self.decrease(position_leaf)

        if position_leaf.balance == Decimal("0"):
            position_leaf = Empty()

        smt.update(position_key_h256, position_leaf)

        # Construct a StrategyIdentifier and corresponding encoded
        # key
        strategy_identifier = StrategyIdentifier(
            outcome.trader_address,
            f"{StrategyIdentifier.generate_strategy_id_hash(outcome.strategy_id).hex()}",
        )
        strategy_key = strategy_identifier.encoded_key
        strategy_key_h256 = bytes_array_to_h256(strategy_key)

        # Get the Strategy leaf given the key from above
        strategy_leaf = smt.get(strategy_key_h256)
        # If we are not paying with DDX, modify the strategy's
        # free collateral to the transaction log entry's new
        # collateral value (which will include adjustments made
        # due to realized pnl and fees)
        strategy_leaf.free_collateral[usdc_address] = round_to_unit(
            strategy_leaf.free_collateral[usdc_address]
            + (pnl - (outcome.fee if not outcome.ddx_fee_election else Decimal("0")))
        )

        # Update the SMT with the H256 repr of the key and the
        # Strategy leaf
        smt.update(strategy_key_h256, strategy_leaf)

        # Place the Strategy leaf update message on the queue to
        # consider sending to the Trader
        place_message_in_queue(
            trader_auditor_queue,
            suppress_trader_queue,
            ItemMessage(
                strategy_identifier.topic_string,
                WebsocketEventType.UPDATE,
                {
                    "t": strategy_identifier.topic_string,
                    "c": strategy_leaf,
                },
                self,
            ),
        )

        if outcome.ddx_fee_election:
            # This doesn't check whether the trader
            # actually has enough DDX to pay for the fees

            trader_key_h256 = bytes_array_to_h256(
                TraderIdentifier(outcome.trader_address).encoded_key
            )
            trader_leaf = smt.get(trader_key_h256)
            trader_leaf.free_ddx_balance = trader_leaf.free_ddx_balance - outcome.fee
            smt.update(trader_key_h256, trader_leaf)

            # Update FeePool leaf
            fee_pool_identifier = FeePoolIdentifier(-1)
            fee_pool_key = fee_pool_identifier.encoded_key
            fee_pool_key_h256 = bytes_array_to_h256(fee_pool_key)
            fee_pool_leaf = smt.get(fee_pool_key_h256)
            if ddx_address not in fee_pool_leaf.pool:
                # If there is no entry in the fee pool mapping
                # for DDX, add it and initialize with the fee
                fee_pool_leaf.pool[ddx_address] = outcome.fee
            else:
                # If there is already an entry in the fee pool mapping
                # for DDX, credit it by the fee
                fee_pool_leaf.pool[ddx_address] += outcome.fee
            smt.update(fee_pool_key_h256, fee_pool_leaf)

        if trade_mining_active and self.time_value > maker_book_order_time_value + 1:
            # Construct a StatsIdentifier and corresponding encoded
            # key
            stats_identifier = StatsIdentifier(outcome.trader_address)
            stats_key = stats_identifier.encoded_key
            stats_key_h256 = bytes_array_to_h256(stats_key)

            # Get the Stats leaf given the key from above
            stats_leaf = smt.get(stats_key_h256)
            notional_amount = self.amount * self.price
            if stats_leaf == H256.zero():
                # If Stats leaf doesn't exist in the tree, we need
                # to create/add a new one
                if is_maker:
                    # Initialize the trader's maker volume
                    stats_leaf = Stats(round_to_unit(notional_amount), Decimal("0"))
                else:
                    # Initialize the trader's taker volume
                    stats_leaf = Stats(Decimal("0"), round_to_unit(notional_amount))
            else:
                # If Stats leaf does exist, we update the existing leaf
                if is_maker:
                    # Increment the trader's maker volume
                    stats_leaf.maker_volume = round_to_unit(
                        stats_leaf.maker_volume + notional_amount
                    )
                else:
                    # Increment the trader's taker volume
                    stats_leaf.taker_volume = round_to_unit(
                        stats_leaf.taker_volume + notional_amount
                    )

            # Update the SMT with the H256 repr of the key and the
            # Stats leaf
            smt.update(stats_key_h256, stats_leaf)

            # Place the Stats leaf update message on the queue to
            # consider sending to the Trader
            place_message_in_queue(
                trader_auditor_queue,
                suppress_trader_queue,
                ItemMessage(
                    stats_identifier.topic_string,
                    WebsocketEventType.UPDATE,
                    {"t": stats_identifier.topic_string, "c": stats_leaf},
                    self,
                ),
            )

        if not is_maker:
            if not outcome.ddx_fee_election:
                # This is fairly opinionated to the initialized
                # system parameters in that it assumes that fees are
                # only paid by takers (i.e. maker fee sched = 0%)

                # Construct OrganicInsuranceFund identifier
                organic_insurance_fund_identifier = OrganicInsuranceFundIdentifier()

                # Derive the OrganicInsuranceFund encoded key and H256
                # repr
                organic_insurance_fund_identifier_key = (
                    organic_insurance_fund_identifier.encoded_key
                )
                organic_insurance_fund_identifier_h256 = bytes_array_to_h256(
                    organic_insurance_fund_identifier_key
                )
                organic_insurance_fund_leaf: OrganicInsuranceFund = smt.get(
                    organic_insurance_fund_identifier_h256
                )

                if usdc_address not in organic_insurance_fund_leaf.capitalization:
                    # If there is no entry in the capitalization
                    # mapping for this token address, add it
                    # and initialize with the delta
                    organic_insurance_fund_leaf.capitalization[
                        usdc_address
                    ] = outcome.fee
                else:
                    # If there is already an entry in the
                    # capitalization mapping for this token address,
                    # credit/debit it by the delta
                    organic_insurance_fund_leaf.capitalization[
                        usdc_address
                    ] += outcome.fee

                smt.update(
                    organic_insurance_fund_identifier_h256, organic_insurance_fund_leaf
                )

                # Place the OrganicInsuranceFund leaf update message on the queue to
                # consider sending to the Trader
                place_message_in_queue(
                    trader_auditor_queue,
                    suppress_trader_queue,
                    ItemMessage(
                        organic_insurance_fund_identifier.topic_string,
                        WebsocketEventType.UPDATE,
                        {
                            "t": organic_insurance_fund_identifier.topic_string,
                            "c": organic_insurance_fund_leaf,
                        },
                        self,
                    ),
                )

    @classmethod
    def decode_event_trigger_into_cls(cls, event_trigger: Dict):
        raise NotImplementedError

    def repr_json(self):
        raise NotImplementedError
