"""
WithdrawDDX module
"""
import asyncio
from ddx_python.decimal import Decimal
from typing import Dict

from ddx_client.auditor.smt.smt import SparseMerkleTree
from ddx_client.auditor.state.identifiers.trader_identifier import TraderIdentifier
from ddx_client.auditor.state.item_message import ItemMessage
from ddx_client.auditor.transactions.event import Event
from ddx_client.auditor.transactions.event_types import EventType
from ddx_client.auditor.utils import bytes_array_to_h256
from ddx_client.client.asyncio_utils import place_message_in_queue
from ddx_client.client.websocket_message import WebsocketEventType


class WithdrawDDX(Event):
    """
    Defines a WithdrawDDX Update
    """

    def __init__(
        self,
        signer_address: str,
        recipient_address: str,
        amount: Decimal,
        request_index: int,
    ):
        """
        Initialize a WithdrawDDX transaction. A WithdrawDDX is when
        a withdrawal of DDX is signaled).

        Parameters
        ----------
        signer_address : str
           Signer's Ethereum address withdrawal is taking place from
        recipient_address: str
           Ethereum address DDX is being withdrawn to
        amount: Decimal
           The amount of DDX being withdrawn
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.signer_address = signer_address
        self.recipient_address = recipient_address
        self.amount = amount

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a WithdrawDDX
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        withdraw_ddx_tx_event = raw_tx_log_event["event"]["c"]

        return cls(
            withdraw_ddx_tx_event["signerAddress"],
            withdraw_ddx_tx_event["recipientAddress"],
            Decimal(withdraw_ddx_tx_event["amount"]),
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a WithdrawDDX transaction. A WithdrawDDX event consists
        of consists of information relating to withdrawal of DDX.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to Post transactions
        """

        # Construct a TraderIdentifier for the withdrawal signer (the
        # address funds are being withdrawn from) and corresponding
        # encoded key
        signer_identifier = TraderIdentifier(self.signer_address)
        signer_key = signer_identifier.encoded_key
        signer_key_h256 = bytes_array_to_h256(signer_key)

        # Get the Trader leaf given the key above
        signer_leaf = smt.get(signer_key_h256)

        # Decrement the free balance by the withdrawn amount
        signer_leaf.free_ddx_balance -= self.amount

        # Update the SMT with the H256 repr of the key and
        # the Trader leaf for the signer
        smt.update(signer_key_h256, signer_leaf)

        # Place the Signer Trader leaf update message on the queue to
        # consider sending to the Trader
        place_message_in_queue(
            trader_auditor_queue,
            suppress_trader_queue,
            ItemMessage(
                signer_identifier.topic_string,
                WebsocketEventType.UPDATE,
                {"t": signer_identifier.topic_string, "c": signer_leaf},
                self,
            ),
        )

        # Construct a TraderIdentifier for the withdrawal trader (the
        # address funds are being sent to) and corresponding encoded key
        trader_identifier = TraderIdentifier(self.recipient_address)
        trader_key = trader_identifier.encoded_key
        trader_key_h256 = bytes_array_to_h256(trader_key)

        # Get the Trader leaf given the key above
        trader_leaf = smt.get(trader_key_h256)

        # Increment the frozen balance by the withdrawn amount
        trader_leaf.frozen_ddx_balance += self.amount

        # Update the SMT with the H256 repr of the key and
        # the Trader leaf for the trader
        smt.update(trader_key_h256, trader_leaf)

        # Place the Trader Trader leaf update message on the queue to
        # consider sending to the Trader
        place_message_in_queue(
            trader_auditor_queue,
            suppress_trader_queue,
            ItemMessage(
                trader_identifier.topic_string,
                WebsocketEventType.UPDATE,
                {"t": trader_identifier.topic_string, "c": signer_leaf},
                self,
            ),
        )

    def repr_json(self):
        return {
            "eventType": EventType.WITHDRAW_DDX,
            "requestIndex": self.request_index,
            "signerAddress": self.signer_address,
            "recipientAddress": self.recipient_address,
            "amount": str(self.amount),
        }

    def __repr__(self):
        return (
            f"WithdrawDDX(event): "
            f"request_index = {self.request_index}; "
            f"signer_address = {self.signer_address}; "
            f"recipient_address = {self.recipient_address}; "
            f"amount: {self.amount};"
        )
