"""
Outcome module
"""

from ddx_python.decimal import Decimal


class Outcome:
    """
    Defines a Outcome
    """

    def __init__(
        self,
        trader_address: str,
        strategy_id: str,
        fee: Decimal,
        ddx_fee_election: bool,
    ):
        """
        Initialize an Outcome (a part of the Fill
        transaction). This holds information including the fees paid,
        the strategy ID and trader address, and whether fees
        are being paid in DDX or not (i.e. USDC).

        Parameters
        ----------
        trader_address : str
           Trader's Ethereum address
        strategy_id: str
           Strategy ID this fill belongs to for the specified trader
        fee: Decimal
           Fees paid for filled trade
        ddx_fee_election: bool
           Whether fees are being paid in DDX (True) or USDC (False)
        """

        self.trader_address = trader_address
        self.strategy_id = strategy_id
        self.fee = fee
        self.ddx_fee_election = ddx_fee_election

    def repr_json(self):
        return {
            "traderAddress": self.trader_address,
            "strategyId": self.strategy_id,
            "fee": str(self.fee),
            "ddxFeeElection": self.ddx_fee_election,
        }

    def __str__(self):
        return f"Outcome (event): trader = {self.trader_address}; strategy = {self.strategy_id}; fee: {self.fee}; ddx_fee_election: {self.ddx_fee_election}"
