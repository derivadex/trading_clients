"""
LiquidationEntry module
"""

from typing import List, Tuple

from ddx_client.auditor.transactions.cancel import Cancel
from ddx_client.auditor.transactions.event import Event
from ddx_client.auditor.transactions.event_types import EventType
from ddx_client.auditor.transactions.liquidated_position import LiquidatedPosition


class LiquidationEntry(Event):
    """
    Defines a LiquidationEntry
    """

    def __init__(
        self,
        trader_address: str,
        strategy_id: str,
        canceled_orders: List[Cancel],
        positions: List[Tuple[str, LiquidatedPosition]],
        request_index: int,
    ):
        """
        Initialize a LiquidationEntry. A LiquidationEntry contains data
        pertaining to individual trader and strategy liquidations.

        Parameters
        ----------
        trader_address : str
            Liquidated trader's Ethereum address
        strategy_id : str
            Liquidated strategy ID
        canceled_orders : List[Cancel]
            Canceled orders for liquidated trader
        positions : List[Tuple[str, LiquidatedPosition]]
            Contains information pertaining to individual liquidated
            positions by symbol
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.trader_address = trader_address
        self.strategy_id = strategy_id
        self.canceled_orders = canceled_orders
        self.positions = positions

    def repr_json(self):
        return {
            "eventType": EventType.LIQUIDATION,
            "requestIndex": self.request_index,
            "traderAddress": self.trader_address,
            "strategyId": self.strategy_id,
            "canceledOrders": self.canceled_orders,
            "positions": self.positions,
        }

    def __repr__(self):
        return f"LiquidationEntry (event)"
