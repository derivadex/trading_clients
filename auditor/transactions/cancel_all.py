"""
CancelAll module
"""
import asyncio
from typing import Dict, List, Tuple
from ddx_python.decimal import Decimal

from ddx_client.auditor.smt.h256 import H256
from ddx_client.auditor.smt.smt import SparseMerkleTree
from ddx_client.auditor.state.book_order import BookOrder
from ddx_client.auditor.state.empty import Empty
from ddx_client.auditor.state.identifiers.book_order_identifier import (
    BookOrderIdentifier,
)
from ddx_client.auditor.state.identifiers.strategy_identifier import StrategyIdentifier
from ddx_client.auditor.state.item_message import ItemMessage
from ddx_client.auditor.transactions.cancel import Cancel
from ddx_client.auditor.transactions.event import Event
from ddx_client.auditor.transactions.event_message import EventMessage
from ddx_client.auditor.transactions.event_types import EventType
from ddx_client.auditor.transactions.identifiers.cancel_identifier import (
    CancelIdentifier,
)
from ddx_client.auditor.utils import (
    bytes_array_to_h256,
    to_camel_case,
    h256_to_bytes_array,
)
from ddx_client.client.asyncio_utils import place_message_in_queue
from ddx_client.client.websocket_message import WebsocketEventType


class CancelAll(Event):
    """
    Defines a CancelAll
    """

    def __init__(
        self,
        trader_address: str,
        strategy_id_hash: str,
        request_index: int,
    ):
        """
        Initialize a CancelAll transaction. A Cancel is when an existing
        order is canceled and removed from the order book.

        Parameters
        ----------
        trader_address : str
           The trader address component of the strategy key pertaining
           to the CancelAll tx.
        strategy_id_hash : str
           The strategy id hash component of the strategy key pertaining
           to the CancelAll tx.
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.trader_address = trader_address
        self.strategy_id_hash = strategy_id_hash

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a CancelAll
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        cancel_tx_event = raw_tx_log_event["event"]["c"]

        return cls(
            cancel_tx_event["strategyKey"]["traderAddress"],
            cancel_tx_event["strategyKey"]["strategyIdHash"],
            raw_tx_log_event["requestIndex"],
        )

    @classmethod
    def decode_event_trigger_into_cls(cls, event_trigger: Dict):
        """
        Decode a event trigger (dict) into a CancelAll
        instance.

        Parameters
        ----------
        event_trigger : Dict
            Event trigger being processed
        """

        return cls(
            event_trigger["traderAddress"],
            event_trigger["strategyIdHash"],
            event_trigger["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a CancelAll transaction. We will need to delete a
        BookOrder leaf with this information.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to Post transactions
        """

        book_order_leaves: List[Tuple[H256, BookOrder]] = kwargs[
            "get_book_orders_for_trader_and_strategy"
        ](self.trader_address, self.strategy_id_hash)

        for book_order_key, book_order_leaf in book_order_leaves:
            book_order_identifier = BookOrderIdentifier.decode_key_into_cls(
                h256_to_bytes_array(book_order_key), book_order_leaf
            )
            cancel = Cancel(
                book_order_identifier.symbol,
                book_order_identifier.order_hash,
                book_order_leaf.amount,
                to_camel_case(EventType.CANCEL_ALL.name),
                self.request_index,
            )
            cancel.process_tx(
                smt, trader_auditor_queue, suppress_trader_queue, **kwargs
            )

    def repr_json(self):
        return {
            "eventType": EventType.CANCEL_ALL,
            "requestIndex": self.request_index,
            "traderAddress": self.trader_address,
            "strategyIdHash": self.strategy_id_hash,
        }

    def __repr__(self):
        return f"CancelAll (event): request_index = {self.request_index}; trader_address: {self.trader_address}; strategy_id_hash: {self.strategy_id_hash}"

    @property
    def metrics_repr(self):
        return {
            "eventType": EventType.CANCEL_ALL,
            "requestIndex": self.request_index,
            "traderAddress": self.trader_address,
            "strategyIdHash": self.strategy_id_hash,
        }
