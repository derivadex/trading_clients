"""
PriceCheckpoint module
"""
import asyncio
from typing import Dict
from ddx_python.decimal import Decimal

from ddx_client.auditor.smt.smt import SparseMerkleTree
from ddx_client.auditor.state.identifiers.price_identifier import PriceIdentifier
from ddx_client.auditor.state.item_message import ItemMessage
from ddx_client.auditor.state.price import Price
from ddx_client.auditor.transactions.event import Event
from ddx_client.auditor.transactions.event_message import EventMessage
from ddx_client.auditor.transactions.event_types import EventType
from ddx_client.auditor.transactions.identifiers.price_checkpoint_identifier import (
    PriceCheckpointIdentifier,
)
from ddx_client.auditor.utils import bytes_array_to_h256
from ddx_client.client.asyncio_utils import place_message_in_queue
from ddx_client.client.websocket_message import WebsocketEventType


class PriceCheckpoint(Event):
    """
    Defines a PriceCheckpoint
    """

    def __init__(
        self,
        symbol: str,
        ema: Decimal,
        index_price_hash: str,
        index_price: Decimal,
        ordinal: int,
        request_index: int,
    ):
        """
        Initialize a PriceCheckpoint transaction. A PriceCheckpoint is
        when a market registers an update to the composite index price
        a perpetual is tracking along with the ema component.

        Parameters
        ----------
        symbol : str
           The symbol for the market this price update is for.
        ema: Decimal
           Contains the EMA information. This captures a smoothed
           average of the spread between the underlying index price and
           the DerivaDEX order book for the market.
        index_price_hash: str
           Index price hash
        index_price: Decimal
           Composite index price (a weighted average across several
           price feed sources)
        ordinal: int
            The numerical sequence-identifying value for a
            PriceCheckpoint
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.symbol = symbol
        self.ema = ema
        self.index_price_hash = index_price_hash
        self.index_price = index_price
        self.ordinal = ordinal

    @property
    def mark_price(self):
        return self.index_price + self.ema

    @classmethod
    def decode_event_trigger_into_cls(cls, raw_event: Dict):
        """
        Decode a raw transaction log event (dict) into a PriceCheckpoint
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        return cls(
            raw_event["symbol"],
            Decimal(raw_event["ema"]),
            raw_event["indexPriceHash"],
            Decimal(raw_event["indexPrice"]),
            raw_event["ordinal"],
            raw_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a PriceCheckpoint transaction. A PriceCheckpoint
        consists of information relating to a new price checkpoint,
        such as the symbol, composite index price, and ema. This will
        update the Price leaf in the SMT.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to Post transactions
        """

        # Place the PriceCheckpoint event message on the queue to
        # consider sending to the Trader
        price_checkpoint_identifier = PriceCheckpointIdentifier.decode_tx_into_cls(self)
        place_message_in_queue(
            trader_auditor_queue,
            suppress_trader_queue,
            EventMessage(
                price_checkpoint_identifier.topic_string,
                WebsocketEventType.UPDATE,
                {
                    "t": price_checkpoint_identifier.topic_string,
                    "c": self,
                },
            ),
        )

        # Construct a PriceIdentifier and corresponding encoded
        # key
        price_identifier = PriceIdentifier(self.symbol, self.index_price_hash)
        price_checkpoint_key = price_identifier.encoded_key
        price_checkpoint_key_h256 = bytes_array_to_h256(price_checkpoint_key)

        # Create a new Price leaf
        price_leaf = Price(
            self.index_price,
            self.ema,
            self.ordinal,
        )

        # Update the SMT with the H256 repr of the key and the
        # Price leaf
        smt.update(price_checkpoint_key_h256, price_leaf)

        # Place the Price leaf update message on the queue to
        # consider sending to the Trader
        place_message_in_queue(
            trader_auditor_queue,
            suppress_trader_queue,
            ItemMessage(
                price_identifier.topic_string,
                WebsocketEventType.UPDATE,
                {"t": price_identifier.topic_string, "c": price_leaf},
                self,
            ),
        )

        # Update latest price leaves abstraction with the new price
        # checkpoint data
        kwargs["latest_price_leaves"][self.symbol] = (
            price_checkpoint_key_h256,
            price_leaf,
        )

    def repr_json(self):
        return {
            "eventType": EventType.PRICE_CHECKPOINT,
            "requestIndex": self.request_index,
            "symbol": self.symbol,
            "ema": str(self.ema),
            "indexPriceHash": self.index_price_hash,
            "indexPrice": str(self.index_price),
            "ordinal": str(self.ordinal),
        }

    def __repr__(self):
        return f"PriceCheckpoint (event): request_index = {self.request_index}; symbol = {self.symbol}; ema = {self.ema}; index_price_hash = {self.index_price_hash}; index_price: {self.index_price}; ordinal: {self.ordinal}"

    @property
    def metrics_repr(self):
        return {
            "eventType": EventType.PRICE_CHECKPOINT,
            "requestIndex": self.request_index,
            "symbol": self.symbol,
            "ema": self.ema,
            "indexPriceHash": self.index_price_hash,
            "indexPrice": self.index_price,
            "ordinal": self.ordinal,
        }
