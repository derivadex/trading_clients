"""
FeeDistribution
"""
import asyncio
from ddx_python.decimal import Decimal
from typing import Dict, List

from ddx_client.auditor.smt.h256 import H256
from ddx_client.auditor.smt.smt import SparseMerkleTree
from ddx_client.auditor.state.empty import Empty
from ddx_client.auditor.state.identifiers.fee_pool_identifier import FeePoolIdentifier
from ddx_client.auditor.state.identifiers.trader_identifier import TraderIdentifier
from ddx_client.auditor.state.trader import Trader
from ddx_client.auditor.transactions.event import Event
from ddx_client.auditor.transactions.event_types import EventType
from ddx_client.auditor.utils import (
    round_to_unit,
    bytes_array_to_h256,
    h256_to_bytes_array,
)


class FeeDistribution(Event):
    """
    Defines a FeeDistribution
    """

    def __init__(
        self,
        custodians: List,
        bonds: List,
        submitter: str,
        epoch_id: int,
        request_index: int,
    ):
        """
        Initialize a FeeDistribution transaction. An
        FeeDistribution is an update to a set of custodians' DDX
        balances.

        Parameters
        ----------
        custodians: List
           Operator custodians
        bonds: List
           Operator bonds
        submitter: str
           Checkpoint submitter address
        epoch_id: int
           Epoch id
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.custodians = custodians
        self.bonds = bonds
        self.submitter = submitter
        self.epoch_id = epoch_id

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a
        FeeDistribution instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        fee_distribution_event = raw_tx_log_event["event"]["c"]

        return cls(
            fee_distribution_event["custodians"],
            [Decimal(bond) for bond in fee_distribution_event["bonds"]],
            fee_distribution_event["submitter"],
            fee_distribution_event["epochId"],
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a FeeDistribution transaction - there shouldn't be any changes to the SMT

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to FeeDistribution transactions
        """

        # update FeePool leaf
        fee_pools = sorted(
            kwargs["get_fee_pool_leaves_up_to_epoch"](self.epoch_id),
            key=lambda x: FeePoolIdentifier.decode_key_into_cls(
                h256_to_bytes_array(x[0])
            ).epoch_id,
        )
        accumulated_ddx = Decimal("0")
        for fee_pool_key_h256, fee_pool in fee_pools:
            accumulated_ddx = accumulated_ddx + fee_pool.pool[kwargs["ddx_address"]]
            smt.update(fee_pool_key_h256, Empty())

        if accumulated_ddx != Decimal("0"):
            total_distributed_fees = Decimal("0")
            distro_per_custodian = accumulated_ddx / Decimal(str(len(self.custodians)))

            for custodian in self.custodians:
                # Construct a TraderIdentifier and corresponding encoded
                # key
                trader_identifier = TraderIdentifier(custodian)
                trader_key = trader_identifier.encoded_key
                trader_key_h256 = bytes_array_to_h256(trader_key)

                # Get the Trader leaf given the key above
                trader_leaf = smt.get(trader_key_h256)
                if trader_leaf == H256.zero():
                    # Initialize a new Trader Leaf
                    trader_leaf = Trader(
                        Decimal("0"),
                        Decimal("0"),
                        "0x0000000000000000000000000000000000000000",
                        False,
                    )

                old_balance = trader_leaf.free_ddx_balance
                new_balance = round_to_unit(
                    trader_leaf.free_ddx_balance + distro_per_custodian
                )

                if new_balance != old_balance:
                    trader_leaf.free_ddx_balance = new_balance

                    # Update the SMT with the H256 repr of the key and the
                    # Trader leaf
                    smt.update(trader_key_h256, trader_leaf)

                    total_distributed_fees += trader_leaf.free_ddx_balance - old_balance

            dust = accumulated_ddx - total_distributed_fees

            # Construct a TraderIdentifier and corresponding encoded
            # key
            trader_identifier = TraderIdentifier(self.submitter)
            trader_key = trader_identifier.encoded_key
            trader_key_h256 = bytes_array_to_h256(trader_key)

            # Get the Trader leaf given the key above
            trader_leaf = smt.get(trader_key_h256)
            if trader_leaf == H256.zero():
                # Initialize a new Trader Leaf
                trader_leaf = Trader(
                    Decimal("0"),
                    Decimal("0"),
                    "0x0000000000000000000000000000000000000000",
                    False,
                )

            old_balance = trader_leaf.free_ddx_balance
            new_balance = round_to_unit(trader_leaf.free_ddx_balance + dust)

            if new_balance != old_balance:
                trader_leaf.free_ddx_balance = new_balance

                # Update the SMT with the H256 repr of the key and the
                # Trader leaf
                smt.update(trader_key_h256, trader_leaf)

    def repr_json(self):
        return {
            "eventType": EventType.FEE_DISTRIBUTION,
            "custodians": self.custodians,
            "bonds": self.bonds,
            "submitter": self.submitter,
            "epochId": self.epoch_id,
        }

    def __repr__(self):
        return f"FeeDistribution (event): custodians = {self.custodians}; bonds = {self.bonds}; submitter = {self.submitter}; epoch_id = {self.epoch_id}"

    @property
    def metrics_repr(self):
        return {
            "eventType": EventType.FEE_DISTRIBUTION,
            "custodians": self.custodians,
            "bonds": self.bonds,
            "submitter": self.submitter,
            "epochId": self.epoch_id,
        }
