"""
InsuranceFundWithdraw module
"""
import asyncio
from ddx_python.decimal import Decimal
from typing import Dict

from ddx_client.auditor.smt.smt import SparseMerkleTree
from ddx_client.auditor.state.identifiers.insurance_fund_contribution_identifier import (
    InsuranceFundContributionIdentifier,
)
from ddx_client.auditor.state.identifiers.organic_insurance_fund_identifier import (
    OrganicInsuranceFundIdentifier,
)
from ddx_client.auditor.state.insurance_fund_contribution import (
    InsuranceFundContribution,
)
from ddx_client.auditor.transactions.event import Event
from ddx_client.auditor.transactions.event_types import EventType
from ddx_client.auditor.utils import bytes_array_to_h256


class InsuranceFundWithdraw(Event):
    """
    Defines an InsuranceFundWithdraw Update
    """

    def __init__(
        self,
        signer_address: str,
        recipient_address: str,
        collateral_address: str,
        amount: Decimal,
        request_index: int,
    ):
        """
        Initialize an InsuranceFundWithdraw transaction. An
        InsuranceFundWithdraw is when a withdrawal of collateral is signaled).

        Parameters
        ----------
        signer_address : str
           Signer's Ethereum address withdrawal is taking place from
        recipient_address: str
           Trader address DDX is being withdrawn to
        amount: Decimal
           The amount of DDX being withdrawn
        strategy_id: str
           Cross-margined strategy ID for which this withdrawal applies
        collateral_address: str
           Collateral ERC-20 token address being withdrawn
        request_index : int
           Sequenced request index of transaction
        """

        self.request_index = request_index
        self.signer_address = signer_address
        self.recipient_address = recipient_address
        self.collateral_address = collateral_address
        self.amount = amount

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a Withdraw
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        insurance_fund_withdraw_tx_event = raw_tx_log_event["event"]["c"]

        return cls(
            insurance_fund_withdraw_tx_event["signerAddress"],
            insurance_fund_withdraw_tx_event["recipientAddress"],
            insurance_fund_withdraw_tx_event["currency"],
            Decimal(insurance_fund_withdraw_tx_event["amount"]),
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a Withdraw transaction. A Withdraw event consists
        of consists of information relating to withdrawal of collateral.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to Post transactions
        """

        # Construct an OrganicInsuranceFundIdentifier and
        # corresponding encoded key
        organic_insurance_fund_identifier = OrganicInsuranceFundIdentifier()
        organic_insurance_fund_key = organic_insurance_fund_identifier.encoded_key
        organic_insurance_fund_key_h256 = bytes_array_to_h256(
            organic_insurance_fund_key
        )
        organic_insurance_fund_leaf = smt.get(organic_insurance_fund_key_h256)

        # Construct an InsuranceFundContributionIdentifier for the
        # withdrawal signer (the address funds are being withdrawn from)
        # and corresponding encoded key
        signer_identifier = InsuranceFundContributionIdentifier(
            self.signer_address,
        )
        signer_key = signer_identifier.encoded_key
        signer_key_h256 = bytes_array_to_h256(signer_key)

        # Get the InsuranceFundContribution leaf given the key above
        signer_leaf: InsuranceFundContribution = smt.get(signer_key_h256)

        # Decrement the free balance by the withdrawn amount
        signer_leaf.free_balance[self.collateral_address] -= self.amount
        if signer_leaf.free_balance[self.collateral_address] == Decimal("0"):
            del signer_leaf.free_balance[self.collateral_address]

        # Update the SMT with the H256 repr of the key and
        # the InsuranceFundContribution leaf for the signer
        smt.update(signer_key_h256, signer_leaf)

        # Construct a InsuranceFundContributionIdentifier for the
        # withdrawal address (the address funds are being withdrawn to)
        # and corresponding encoded key
        trader_identifier = InsuranceFundContributionIdentifier(
            self.recipient_address,
        )
        trader_key = trader_identifier.encoded_key
        trader_key_h256 = bytes_array_to_h256(trader_key)

        # Get the InsuranceFundContribution leaf given the key above
        trader_leaf: InsuranceFundContribution = smt.get(trader_key_h256)

        # Increment the frozen balance by the withdrawn amount
        if self.collateral_address not in trader_leaf.frozen_balance:
            trader_leaf.frozen_balance[self.collateral_address] = self.amount
        else:
            trader_leaf.frozen_balance[self.collateral_address] += self.amount

        # Update the SMT with the H256 repr of the key and
        # the Strategy leaf for the trader
        smt.update(trader_key_h256, trader_leaf)

        organic_insurance_fund_leaf.capitalization[
            self.collateral_address
        ] -= self.amount
        if organic_insurance_fund_leaf.capitalization[
            self.collateral_address
        ] == Decimal("0"):
            del organic_insurance_fund_leaf.capitalization[self.collateral_address]

        # Update the SMT with the H256 repr of the key and the
        # Strategy leaf
        smt.update(organic_insurance_fund_key_h256, organic_insurance_fund_leaf)

    def repr_json(self):
        return {
            "eventType": EventType.WITHDRAW,
            "requestIndex": self.request_index,
            "signerAddress": self.signer_address,
            "recipientAddress": self.recipient_address,
            "collateralAddress": self.collateral_address,
            "amount": str(self.amount),
        }

    def __repr__(self):
        return (
            f"Withdraw(event): "
            f"request_index = {self.request_index}; "
            f"signer_address = {self.signer_address}; "
            f"recipient_address = {self.recipient_address}; "
            f"collateral_address = {self.collateral_address}; "
            f"amount: {self.amount};"
        )
