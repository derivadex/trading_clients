"""
MarketSpecsUpdate module
"""
from ddx_python.decimal import Decimal
from typing import Dict

from ddx_client.auditor.state.market_specs import MarketSpecs
from ddx_client.auditor.state.weighted_price_source import WeightedPriceSource
from ddx_client.auditor.transactions.event import Event


class MarketSpecsUpdate(Event):
    """
    Defines an MarketSpecsUpdate
    """

    def __init__(
        self,
        symbol: str,
        market_specs: MarketSpecs,
        update_type: str,
        tx_hash: str,
        request_index: int,
    ):
        """
        Initialize a MarketSpecsUpdate transaction.

        Parameters
        ----------
        symbol : str
           Symbol for market specs update
        market_specs : MarketSpecs
           Updated market specs
        update_type : str
           Market specs update type (Add | Update | Remove)
        tx_hash : str
           Transaction hash of on-chain action
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.symbol = symbol
        self.market_specs = market_specs
        self.update_type = update_type
        self.tx_hash = tx_hash

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a
        MarketSpecsUpdate instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        market_specs_update_tx_event = raw_tx_log_event["event"]["c"]

        return cls(
            market_specs_update_tx_event["symbol"],
            MarketSpecs(
                Decimal(market_specs_update_tx_event["marketSpecs"]["tickSize"]),
                Decimal(
                    market_specs_update_tx_event["marketSpecs"]["maxOrderNotional"]
                ),
                Decimal(
                    market_specs_update_tx_event["marketSpecs"][
                        "maxTakerPriceDeviation"
                    ]
                ),
                Decimal(market_specs_update_tx_event["marketSpecs"]["minOrderSize"]),
                [
                    WeightedPriceSource(source["sourceName"], Decimal(source["weight"]))
                    for source in market_specs_update_tx_event["marketSpecs"]["sources"]
                ],
            ),
            market_specs_update_tx_event["symbol"],
            cancel_tx_event["orderHash"],
            Decimal(cancel_tx_event["amount"]),
            raw_tx_log_event["requestIndex"],
        )
