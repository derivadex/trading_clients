"""
Post module
"""
import asyncio
from typing import Dict
from ddx_python.decimal import Decimal

from ddx_client.auditor.smt.smt import SparseMerkleTree
from ddx_client.auditor.state.book_order import BookOrder
from ddx_client.auditor.state.identifiers.book_order_identifier import (
    BookOrderIdentifier,
)
from ddx_client.auditor.state.identifiers.strategy_identifier import (
    StrategyIdentifier,
)
from ddx_client.auditor.state.item_message import ItemMessage
from ddx_client.auditor.transactions.event import Event
from ddx_client.auditor.transactions.event_message import EventMessage
from ddx_client.auditor.transactions.event_types import EventType
from ddx_client.auditor.transactions.identifiers.post_identifier import (
    PostIdentifier,
)
from ddx_client.auditor.shared.order_book import OrderBook
from ddx_client.auditor.utils import bytes_array_to_h256
from ddx_client.client.asyncio_utils import place_message_in_queue
from ddx_client.client.websocket_message import WebsocketEventType


class Post(Event):
    """
    Defines a Post
    """

    def __init__(
        self,
        symbol: str,
        order_hash: str,
        side: str,
        amount: Decimal,
        price: Decimal,
        trader_address: str,
        strategy_id: str,
        book_ordinal: int,
        time_value: int,
        reason: str,
        request_index: int,
    ):
        """
        Initialize a Post transaction. A Post is an order that enters
        the order book.

        Parameters
        ----------
        symbol : str
           The symbol for the market this order is for.
        order_hash: str
           Hexstr representation of the EIP-712 hash of the order
        side: str
           Side of order ('Bid', 'Ask')
        amount: Decimal
           Amount/size of order
        price: Decimal
           Price the order has been placed at
        trader_address: str
           The order creator's Ethereum address
        strategy_id: str
           The cross-margined strategy ID for which this order belongs
        book_ordinal: int
            The numerical sequence-identifying value for an order's
            insertion into the book
        time_value : int
            Time value
        reason : str
            Parent transaction reason for order post
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.reason = reason
        self.time_value = time_value
        self.symbol = symbol
        self.order_hash = order_hash
        self.side = side
        self.amount = amount
        self.price = price
        self.trader_address = trader_address
        self.strategy_id = strategy_id
        self.book_ordinal = book_ordinal

    @property
    def is_bid(self):
        return self.side == "Bid"

    @classmethod
    def decode_event_trigger_into_cls(cls, event_trigger: Dict):
        """
        Decode a event trigger (dict) into a Post
        instance.

        Parameters
        ----------
        event_trigger : Dict
            Event trigger being processed
        """

        return cls(
            event_trigger["symbol"],
            event_trigger["orderHash"],
            event_trigger["side"],
            Decimal(event_trigger["amount"]),
            Decimal(event_trigger["price"]),
            event_trigger["traderAddress"],
            event_trigger["strategyId"],
            event_trigger["bookOrdinal"],
            event_trigger["timeValue"],
            event_trigger["reason"],
            event_trigger["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a Post transaction. We will need to create a new
        BookOrder leaf with this information.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to Post transactions
        """

        # Place the Post event message on the queue to
        # consider sending to the Trader
        post_identifier = PostIdentifier.decode_tx_into_cls(self)
        place_message_in_queue(
            trader_auditor_queue,
            suppress_trader_queue,
            EventMessage(
                post_identifier.topic_string,
                WebsocketEventType.UPDATE,
                {"t": post_identifier.topic_string, "c": self},
            ),
        )

        # Construct a BookOrderIdentifier and corresponding encoded key
        book_order_identifier = BookOrderIdentifier(
            self.symbol,
            self.order_hash,
            self.trader_address,
            f"{StrategyIdentifier.generate_strategy_id_hash(self.strategy_id).hex()}",
        )
        book_order_key = book_order_identifier.encoded_key
        book_order_key_h256 = bytes_array_to_h256(book_order_key)

        # Apply the post transaction to the local order book abstraction
        if self.symbol not in kwargs["order_books"]:
            kwargs["order_books"][self.symbol] = OrderBook(smt)
        kwargs["order_books"][self.symbol].apply_post_tx_to_book(
            book_order_key_h256, self
        )

        # Initialize a new BookOrder leaf
        book_order_leaf = BookOrder(
            self.side,
            self.amount,
            self.price,
            book_order_identifier.trader_address,
            book_order_identifier.abbrev_strategy_id_hash,
            self.book_ordinal,
            self.time_value,
        )

        # Update the SMT with the H256 repr of the key and the BookOrder
        # leaf
        smt.update(book_order_key_h256, book_order_leaf)

        # Place the BookOrder leaf update message on the queue to
        # consider sending to the Trader
        place_message_in_queue(
            trader_auditor_queue,
            suppress_trader_queue,
            ItemMessage(
                BookOrderIdentifier.decode_key_into_cls(
                    book_order_key, book_order_leaf
                ).topic_string,
                WebsocketEventType.UPDATE,
                {
                    "t": BookOrderIdentifier.decode_key_into_cls(
                        book_order_key, book_order_leaf
                    ).topic_string,
                    "c": book_order_leaf,
                },
                self,
            ),
        )

    def repr_json(self):
        return {
            "eventType": EventType.POST,
            "reason": self.reason,
            "requestIndex": self.request_index,
            "symbol": self.symbol,
            "orderHash": self.order_hash,
            "side": self.side,
            "amount": str(self.amount),
            "price": str(self.price),
            "traderAddress": self.trader_address,
            "strategyId": self.strategy_id,
            "bookOrdinal": self.book_ordinal,
            "timeValue": self.time_value,
        }

    def __repr__(self):
        return f"Post (event): request_index = {self.request_index}; reason = {self.reason}; symbol = {self.symbol}; order_hash = {self.order_hash}; side: {self.side}; amount: {self.amount}; price: {self.price}; book_ordinal: {self.book_ordinal}, time_value: {self.time_value}"

    @property
    def metrics_repr(self):
        return {
            "eventType": EventType.POST,
            "reason": self.reason,
            "requestIndex": self.request_index,
            "symbol": self.symbol,
            "orderHash": self.order_hash,
            "side": self.side,
            "amount": self.amount,
            "price": self.price,
            "traderAddress": self.trader_address,
            "strategyId": self.strategy_id,
            "bookOrdinal": self.book_ordinal,
            "timeValue": self.time_value,
        }
