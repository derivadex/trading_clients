"""
Signer Registered module
"""
import asyncio
from ddx_python.decimal import Decimal
from typing import Dict

from ddx_client.auditor.smt.smt import SparseMerkleTree
from ddx_client.auditor.state.identifiers.signer_identifier import SignerIdentifier
from ddx_client.auditor.state.signer import Signer
from ddx_client.auditor.state.item_message import ItemMessage
from ddx_client.auditor.transactions.event import Event
from ddx_client.auditor.transactions.event_types import EventType
from ddx_client.auditor.utils import bytes_array_to_h256
from ddx_client.client.asyncio_utils import place_message_in_queue
from ddx_client.client.websocket_message import WebsocketEventType


class SignerRegistered(Event):
    """
    Defines a SignerRegistered
    """

    def __init__(
        self,
        signer_address: str,
        release_hash: str,
    ):
        """
        Initialize a SignerRegistered tx, when a new enclave signer
        registers with the smart contract

        Parameters
        ----------
        signer_address : str
           address used by the enclave to sign receipts and checkpoints
        releaseh_hash: str
           release hash associated with the enclave hash(release measurement + isvsvn)
        """

        self.signer_address = signer_address
        self.release_hash = release_hash

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a SignerRegistered
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        withdraw_tx_event = raw_tx_log_event["event"]["c"]

        return cls(
            withdraw_tx_event["signerAddress"],
            withdraw_tx_event["releaseHash"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a SignerRegistered, inserting the signer into the tree

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to Post transactions
        """

        # Construct a StrategyIdentifier for the withdrawal signer (the
        # address funds are being withdrawn from) and corresponding
        # encoded key
        signer_identifier = SignerIdentifier(self.signer_address)
        signer_key = signer_identifier.encoded_key
        signer_key_h256 = bytes_array_to_h256(signer_key)

        signer_leaf = Signer(self.release_hash)

        # Update the SMT with the H256
        smt.update(signer_key_h256, signer_leaf)

    def repr_json(self):
        return {
            "eventType": EventType.SIGNER_REGISTERED,
            "signerAddress": self.signer_address,
            "releaseHash": self.release_hash,
        }

    def __repr__(self):
        return (
            f"SignerRegistered(event): "
            f"signer_address = {self.signer_address}; "
            f"release_hash= {self.release_hash}; "
        )
