"""
CompleteFill module
"""
import asyncio
from ddx_python.decimal import Decimal
from typing import Dict, List, Union

from ddx_client.auditor.smt.smt import SparseMerkleTree
from ddx_client.auditor.transactions.cancel import Cancel
from ddx_client.auditor.transactions.event import Event
from ddx_client.auditor.transactions.event_types import EventType
from ddx_client.auditor.transactions.outcome import Outcome
from ddx_client.auditor.transactions.price_checkpoint import PriceCheckpoint
from ddx_client.auditor.transactions.trade_fill import TradeFill
from ddx_client.auditor.utils import to_camel_case


class CompleteFill(Event):
    """
    Defines an CompleteFill
    """

    def __init__(
        self,
        price_checkpoints: List[PriceCheckpoint],
        trade_outcomes: List[Union[TradeFill, Cancel]],
        request_index: int,
    ):
        """
        Initialize a CompleteFill transaction. A CompleteFill is a scenario
        where the taker order has been completely filled across 1 or more
        maker orders along with any canceled maker orders.

        Parameters
        ----------
        price_checkpoints : PriceCheckpoint
           The price checkpoints that make up this transaction
        trade_outcomes : List[Union[TradeFill, Cancel]]
            A list of trade outcome objects
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.price_checkpoints = price_checkpoints
        self.trade_outcomes = trade_outcomes

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a CompleteFill
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        all_price_checkpoint_tx_event = raw_tx_log_event["event"]["c"][0]
        trade_outcomes_tx_event = raw_tx_log_event["event"]["c"][1]

        return cls(
            [
                PriceCheckpoint(
                    all_price_checkpoints_key,
                    Decimal(all_price_checkpoints_val["ema"]),
                    all_price_checkpoints_val["indexPriceHash"],
                    Decimal(all_price_checkpoints_val["indexPrice"]),
                    all_price_checkpoints_val["ordinal"],
                    raw_tx_log_event["requestIndex"],
                )
                for all_price_checkpoints_key, all_price_checkpoints_val in all_price_checkpoint_tx_event.items()
            ],
            [
                TradeFill(
                    trade_outcome["Fill"]["reason"],
                    trade_outcome["Fill"]["symbol"],
                    trade_outcome["Fill"]["takerOrderHash"],
                    trade_outcome["Fill"]["makerOrderHash"],
                    Decimal(trade_outcome["Fill"]["makerOrderRemainingAmount"]),
                    Decimal(trade_outcome["Fill"]["amount"]),
                    Decimal(trade_outcome["Fill"]["price"]),
                    trade_outcome["Fill"]["takerSide"],
                    Outcome(
                        trade_outcome["Fill"]["makerOutcome"]["trader"],
                        trade_outcome["Fill"]["makerOutcome"]["strategy"],
                        Decimal(trade_outcome["Fill"]["makerOutcome"]["fee"]),
                        trade_outcome["Fill"]["makerOutcome"]["ddxFeeElection"],
                    ),
                    Outcome(
                        trade_outcome["Fill"]["takerOutcome"]["trader"],
                        trade_outcome["Fill"]["takerOutcome"]["strategy"],
                        Decimal(trade_outcome["Fill"]["takerOutcome"]["fee"]),
                        trade_outcome["Fill"]["takerOutcome"]["ddxFeeElection"],
                    ),
                    raw_tx_log_event["timeValue"],
                    raw_tx_log_event["requestIndex"],
                )
                if "Fill" in trade_outcome
                else Cancel(
                    trade_outcome["Cancel"]["symbol"],
                    trade_outcome["Cancel"]["orderHash"],
                    Decimal(trade_outcome["Cancel"]["amount"]),
                    to_camel_case(EventType.COMPLETE_FILL.name),
                    raw_tx_log_event["requestIndex"],
                )
                for trade_outcome in trade_outcomes_tx_event
            ],
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a CompleteFill transaction. A CompleteFill consists of
        Fill objects, which will adjust the maker BookOrder leaf in the
        SMT, while also adjusting the Strategy, Position, and Trader
        leaves corresponding to both the maker and the taker.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to CompleteFill transactions
        """

        # Loop through each price checkpoint event and process them
        # individually
        for price_checkpoint in self.price_checkpoints:
            price_checkpoint.process_tx(
                smt, trader_auditor_queue, suppress_trader_queue, **kwargs
            )

        # Loop through each trade outcome event and process them individually
        for trade_outcome in self.trade_outcomes:
            trade_outcome.process_tx(
                smt, trader_auditor_queue, suppress_trader_queue, **kwargs
            )

    def __repr__(self):
        return f"CompleteFill (event): trade_outcomes = {self.trade_outcomes}"
