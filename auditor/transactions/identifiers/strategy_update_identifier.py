from ddx_client.auditor.shared.identifier import Identifier
from ddx_client.auditor.shared.identifier_types import IdentifierTypes
from ddx_client.auditor.state.identifiers.strategy_identifier import (
    StrategyIdentifier,
)
from ddx_client.auditor.state.strategy import Strategy
from ddx_client.auditor.utils import is_valid_none_list


class StrategyUpdateIdentifier(Identifier):
    def __init__(self, trader_address: str, abbrev_strategy_id_hash: str):
        if not is_valid_none_list([trader_address, abbrev_strategy_id_hash]):
            raise Exception("Invalid StrategyUpdateIdentifier generation")

        self.trader_address = trader_address
        self.abbrev_strategy_id_hash = abbrev_strategy_id_hash

    def encompasses_identifier(self, identifier):
        if type(identifier).__name__ != "StrategyUpdateIdentifier":
            return False
        if self.abbrev_strategy_id_hash is not None:
            return (
                self.trader_address == identifier.trader_address
                and self.abbrev_strategy_id_hash == identifier.abbrev_strategy_id_hash
            )
        elif self.trader_address is not None:
            return self.trader_address == identifier.trader_address
        return True

    @property
    def topic_string(self):
        return f"{'/'.join(filter(None, ['TX_LOG', IdentifierTypes.STRATEGY_UPDATE, self.trader_address, self.abbrev_strategy_id_hash]))}/"

    @classmethod
    def decode_topic_string_into_cls(cls, topic: str):
        """
        Decode a topic string into a StrategyUpdateIdentifier.

        Parameters
        ----------
        topic : str
            Topic string
        """

        topic_parts = topic.split("/")[:-1]
        if topic_parts[1] != IdentifierTypes.STRATEGY_UPDATE:
            raise Exception(
                f"Invalid topic: must be of {IdentifierTypes.STRATEGY_UPDATE} type"
            )

        def safe_list_get(l, idx, default):
            try:
                return l[idx]
            except IndexError:
                return default

        return cls(
            safe_list_get(topic_parts, 2, None),
            safe_list_get(topic_parts, 3, None),
        )

    @classmethod
    def decode_tx_into_cls(cls, tx):
        """
        Decode a StrategyUpdate transaction into a StrategyUpdateIdentifier topic.

        Parameters
        ----------
        tx : StrategyUpdate
            StrategyUpdate transaction
        """

        return cls(
            tx.trader_address,
            f"{StrategyIdentifier.generate_strategy_id_hash(tx.strategy_id).hex()}",
        )

    @property
    def is_max_granular(self):
        return (
            self.trader_address is not None and self.abbrev_strategy_id_hash is not None
        )
