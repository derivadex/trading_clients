from ddx_client.auditor.shared.identifier import Identifier
from ddx_client.auditor.shared.identifier_types import IdentifierTypes
from ddx_client.auditor.state.identifiers.strategy_identifier import (
    StrategyIdentifier,
)
from ddx_client.auditor.state.strategy import Strategy
from ddx_client.auditor.utils import is_valid_none_list


class TraderUpdateIdentifier(Identifier):
    def __init__(self, trader_address: str):
        if not is_valid_none_list([trader_address]):
            raise Exception("Invalid TraderUpdateIdentifier generation")

        self.trader_address = trader_address

    def encompasses_identifier(self, identifier):
        if type(identifier).__name__ != "TraderUpdateIdentifier":
            return False
        if self.trader_address is not None:
            return self.trader_address == identifier.trader_address
        return True

    @property
    def topic_string(self):
        return f"{'/'.join(filter(None, ['TX_LOG', IdentifierTypes.TRADER_UPDATE, self.trader_address]))}/"

    @classmethod
    def decode_topic_string_into_cls(cls, topic: str):
        """
        Decode a topic string into a TraderUpdateIdentifier.

        Parameters
        ----------
        topic : str
            Topic string
        """

        topic_parts = topic.split("/")[:-1]
        if topic_parts[1] != IdentifierTypes.TRADER_UPDATE:
            raise Exception(
                f"Invalid topic: must be of {IdentifierTypes.TRADER_UPDATE} type"
            )

        def safe_list_get(l, idx, default):
            try:
                return l[idx]
            except IndexError:
                return default

        return cls(safe_list_get(topic_parts, 2, None))

    @classmethod
    def decode_tx_into_cls(cls, tx):
        """
        Decode a TraderUpdate transaction into a TraderUpdateIdentifier topic.

        Parameters
        ----------
        tx : TraderUpdate
            TraderUpdate transaction
        """

        return cls(tx.trader_address)

    @property
    def is_max_granular(self):
        return self.trader_address is not None
