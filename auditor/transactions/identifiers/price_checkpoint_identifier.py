from ddx_client.auditor.shared.identifier import Identifier
from ddx_client.auditor.shared.identifier_types import IdentifierTypes
from ddx_client.auditor.utils import is_valid_none_list


class PriceCheckpointIdentifier(Identifier):
    def __init__(
        self,
        symbol: str,
    ):
        if not is_valid_none_list([symbol]):
            raise Exception("Invalid PriceCheckpointIdentifier generation")

        self.symbol = symbol

    def encompasses_identifier(self, identifier):
        if type(identifier).__name__ != "PriceCheckpointIdentifier":
            return False
        if self.symbol is not None:
            return self.symbol == identifier.symbol
        return True

    @property
    def topic_string(self):
        return f"{'/'.join(filter(None, ['TX_LOG', IdentifierTypes.PRICE_CHECKPOINT, self.symbol]))}/"

    @classmethod
    def decode_topic_string_into_cls(cls, topic: str):
        """
        Decode a topic string into a PriceCheckpointIdentifier.

        Parameters
        ----------
        topic : str
            Topic string
        """

        topic_parts = topic.split("/")[:-1]
        if topic_parts[1] != IdentifierTypes.PRICE_CHECKPOINT:
            raise Exception(
                f"Invalid topic: must be of {IdentifierTypes.PRICE_CHECKPOINT} type"
            )

        def safe_list_get(l, idx, default):
            try:
                return l[idx]
            except IndexError:
                return default

        return cls(safe_list_get(topic_parts, 2, None))

    @classmethod
    def decode_tx_into_cls(cls, tx):
        """
        Decode a PriceCheckpoint transaction into a
        PriceCheckpointIdentifier.

        Parameters
        ----------
        tx : PriceCheckpoint
            Price checkpoint transaction
        """

        return cls(tx.symbol)

    @property
    def is_max_granular(self):
        return self.symbol is not None
