"""
Liquidation module
"""
import asyncio
from ddx_python.decimal import Decimal
from typing import Dict, List, Tuple

from ddx_client.auditor.smt.h256 import H256
from ddx_client.auditor.smt.smt import SparseMerkleTree
from ddx_client.auditor.state.empty import Empty
from ddx_client.auditor.state.identifiers.organic_insurance_fund_identifier import (
    OrganicInsuranceFundIdentifier,
)
from ddx_client.auditor.state.identifiers.position_identifier import (
    PositionIdentifier,
)
from ddx_client.auditor.state.identifiers.price_identifier import PriceIdentifier
from ddx_client.auditor.state.identifiers.strategy_identifier import (
    StrategyIdentifier,
)
from ddx_client.auditor.state.item_message import ItemMessage
from ddx_client.auditor.state.organic_insurance_fund import OrganicInsuranceFund
from ddx_client.auditor.state.position import Position
from ddx_client.auditor.state.price import Price
from ddx_client.auditor.state.strategy import Strategy
from ddx_client.auditor.transactions.adl_outcome import AdlOutcome
from ddx_client.auditor.transactions.cancel import Cancel
from ddx_client.auditor.transactions.event import Event
from ddx_client.auditor.transactions.event_types import EventType
from ddx_client.auditor.transactions.liquidated_position import LiquidatedPosition
from ddx_client.auditor.transactions.liquidation_entry import LiquidationEntry
from ddx_client.auditor.transactions.liquidation_fill import LiquidationFill
from ddx_client.auditor.transactions.outcome import Outcome
from ddx_client.auditor.transactions.price_checkpoint import PriceCheckpoint
from ddx_client.auditor.utils import (
    bytes_array_to_h256,
    to_camel_case,
    round_to_unit,
    compute_avg_pnl,
)
from ddx_client.client.asyncio_utils import place_message_in_queue
from ddx_client.client.websocket_message import WebsocketEventType


class Liquidation(Event):
    """
    Defines an Liquidation
    """

    def __init__(
        self,
        price_checkpoints: List[PriceCheckpoint],
        liquidation_entries: List[LiquidationEntry],
        request_index: int,
    ):
        """
        Initialize a Liquidation transaction. A Liquidation contains
        a list of LiquidationEntry objects.

        Parameters
        ----------
        price_checkpoints : PriceCheckpoint
           The price checkpoints that make up this transaction
        liquidation_entries : List[LiquidationEntry]
            A list of LiquidationEntry objects.
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.price_checkpoints = price_checkpoints
        self.liquidation_entries = liquidation_entries

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a Liquidation
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        all_price_checkpoint_tx_event = raw_tx_log_event["event"]["c"][0]
        liquidation_tx_event = raw_tx_log_event["event"]["c"][1]

        return cls(
            [
                PriceCheckpoint(
                    all_price_checkpoints_key,
                    Decimal(all_price_checkpoints_val["ema"]),
                    all_price_checkpoints_val["indexPriceHash"],
                    Decimal(all_price_checkpoints_val["indexPrice"]),
                    all_price_checkpoints_val["ordinal"],
                    raw_tx_log_event["requestIndex"],
                )
                for all_price_checkpoints_key, all_price_checkpoints_val in all_price_checkpoint_tx_event.items()
            ],
            [
                LiquidationEntry(
                    liquidation_entry["traderAddress"],
                    liquidation_entry["strategyId"],
                    [
                        Cancel(
                            canceled_order["symbol"],
                            canceled_order["orderHash"],
                            Decimal(canceled_order["amount"]),
                            to_camel_case(EventType.LIQUIDATION.name),
                            raw_tx_log_event["requestIndex"],
                        )
                        for canceled_order in liquidation_entry["canceledOrders"]
                    ],
                    [
                        (
                            liquidated_position_key,
                            LiquidatedPosition(
                                Decimal(liquidated_position_val["amount"]),
                                [
                                    LiquidationFill(
                                        trade_outcome["Fill"]["reason"],
                                        trade_outcome["Fill"]["symbol"],
                                        trade_outcome["Fill"]["indexPriceHash"],
                                        trade_outcome["Fill"]["makerOrderHash"],
                                        Decimal(
                                            trade_outcome["Fill"][
                                                "makerOrderRemainingAmount"
                                            ]
                                        ),
                                        Decimal(trade_outcome["Fill"]["amount"]),
                                        Decimal(trade_outcome["Fill"]["price"]),
                                        trade_outcome["Fill"]["takerSide"],
                                        Outcome(
                                            trade_outcome["Fill"]["makerOutcome"][
                                                "trader"
                                            ],
                                            trade_outcome["Fill"]["makerOutcome"][
                                                "strategy"
                                            ],
                                            Decimal(
                                                trade_outcome["Fill"]["makerOutcome"][
                                                    "fee"
                                                ]
                                            ),
                                            trade_outcome["Fill"]["makerOutcome"][
                                                "ddxFeeElection"
                                            ],
                                        ),
                                        raw_tx_log_event["timeValue"],
                                        raw_tx_log_event["requestIndex"],
                                    )
                                    if "Fill" in trade_outcome
                                    else Cancel(
                                        trade_outcome["Cancel"]["symbol"],
                                        trade_outcome["Cancel"]["orderHash"],
                                        Decimal(trade_outcome["Cancel"]["amount"]),
                                        to_camel_case(EventType.LIQUIDATION.name),
                                        raw_tx_log_event["requestIndex"],
                                    )
                                    for trade_outcome in liquidated_position_val[
                                        "tradeOutcomes"
                                    ]
                                ],
                                [
                                    AdlOutcome(
                                        adl_outcome["traderAddress"],
                                        adl_outcome["strategyId"],
                                        raw_tx_log_event["requestIndex"],
                                    )
                                    for adl_outcome in liquidated_position_val[
                                        "adlOutcomes"
                                    ]
                                ],
                                Decimal(liquidated_position_val["newInsuranceFundCap"]),
                                raw_tx_log_event["requestIndex"],
                            ),
                        )
                        for (
                            liquidated_position_key,
                            liquidated_position_val,
                        ) in liquidation_entry["positions"]
                    ],
                    raw_tx_log_event["requestIndex"],
                )
                for liquidation_entry in liquidation_tx_event
            ],
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a Liquidation transaction. A Liquidation is when a
        trader's account is under-collateralized and forcibly closed.
        Their collateral is removed and positions are closed, either
        against the order book with other market participants, or if
        the insurance fund is insufficiently-capitalized, ADL'd vs
        winning traders. A liquidated trader's open orders are canceled
        and the insurance fund is adjusted, along with the relevant
        Stats leaves for the maker traders taking on the liquidated
        position.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to Liquidation transactions
        """

        def compute_strategy_total_value(
            strategy_leaf: Strategy,
            position_leaves: Dict[str, Position],
            prices: Dict[str, Tuple[H256, Price]],
            usdc_address: str,
        ):
            # Compute Strategy total value prior to liquidation
            strategy_total_value = strategy_leaf.free_collateral[usdc_address]

            for symbol in position_leaves:
                # Obtain latest mark price for the symbol
                mark_price = prices[symbol][1].mark_price

                # Compute unrealized PNL for Position
                unrealized_pnl = position_leaves[symbol].balance * compute_avg_pnl(
                    position_leaves[symbol], mark_price
                )

                # Adjust the Strategy's total value to account for
                # the Position's unrealized pnl
                strategy_total_value += unrealized_pnl

            return strategy_total_value

        # Loop through each price checkpoint event and process them
        # individually
        for price_checkpoint in self.price_checkpoints:
            price_checkpoint.process_tx(
                smt, trader_auditor_queue, suppress_trader_queue, **kwargs
            )

        # Loop through each liquidation entry and process the cancels
        for liquidation_entry in self.liquidation_entries:
            # Loop through the canceled orders to remove them from the
            # SMT
            for cancel_tx in liquidation_entry.canceled_orders:
                cancel_tx.process_tx(
                    smt, trader_auditor_queue, suppress_trader_queue, **kwargs
                )

        # Loop through each liquidation entry and process them individually
        for liquidation_entry in self.liquidation_entries:
            # Construct Strategy identifier for the one getting
            # liquidated
            liquidated_strategy_identifier = StrategyIdentifier(
                liquidation_entry.trader_address,
                f"{StrategyIdentifier.generate_strategy_id_hash(liquidation_entry.strategy_id).hex()}",
            )

            # Derive the encoded Strategy key and H256 repr
            liquidated_strategy_key = liquidated_strategy_identifier.encoded_key
            liquidated_strategy_key_h256 = bytes_array_to_h256(liquidated_strategy_key)

            # Get the Strategy leaf
            liquidated_strategy: Strategy = smt.get(liquidated_strategy_key_h256)

            position_leaves_by_symbol = {}
            for symbol, liquidated_position in liquidation_entry.positions:
                # Construct Position identifier for the liquidated
                # position
                liquidated_position_identifier = PositionIdentifier(
                    symbol,
                    liquidation_entry.trader_address,
                    f"{StrategyIdentifier.generate_strategy_id_hash(liquidation_entry.strategy_id).hex()}",
                )

                # Derive the encoded Position key and H256 repr
                liquidated_position_key = liquidated_position_identifier.encoded_key
                liquidated_position_key_h256 = bytes_array_to_h256(
                    liquidated_position_key
                )

                # Get liquidated Position leaf
                liquidated_position_leaf: Position = smt.get(
                    liquidated_position_key_h256
                )

                # Store position in dict
                position_leaves_by_symbol[symbol] = liquidated_position_leaf

            # Place the Strategy leaf update message on the queue to
            # consider sending to the Trader
            trader_auditor_queue.put_nowait(
                ItemMessage(
                    liquidated_strategy_identifier.topic_string,
                    WebsocketEventType.UPDATE,
                    {
                        "t": liquidated_strategy_identifier.topic_string,
                        "c": liquidated_strategy,
                    },
                    liquidation_entry,
                )
            )

            # Loop through the positions of the liquidated Strategy
            for symbol, liquidated_position in liquidation_entry.positions:
                # Get collateral for liquidated strategy
                collateral = liquidated_strategy.free_collateral[kwargs["usdc_address"]]

                # Construct Position identifier for the liquidated
                # position
                liquidated_position_identifier = PositionIdentifier(
                    symbol,
                    liquidation_entry.trader_address,
                    f"{StrategyIdentifier.generate_strategy_id_hash(liquidation_entry.strategy_id).hex()}",
                )

                # Derive the encoded Position key and H256 repr
                liquidated_position_key = liquidated_position_identifier.encoded_key
                liquidated_position_key_h256 = bytes_array_to_h256(
                    liquidated_position_key
                )

                # Get liquidated Position leaf
                liquidated_position_leaf: Position = smt.get(
                    liquidated_position_key_h256
                )

                # Obtain latest mark price
                mark_price = kwargs["latest_price_leaves"][symbol][1].mark_price

                liquidated_strategy_total_value = compute_strategy_total_value(
                    liquidated_strategy,
                    position_leaves_by_symbol,
                    kwargs["latest_price_leaves"],
                    kwargs["usdc_address"],
                )

                # Compute the bankruptcy price for the liquidated Position
                bankruptcy_price = mark_price - (
                    Decimal("1")
                    if liquidated_position_leaf.side == "Long"
                    else Decimal("-1")
                ) * (liquidated_strategy_total_value / liquidated_position_leaf.balance)

                # Loop through each trade outcome event and process them individually
                for trade_outcome in liquidated_position.trade_outcomes:
                    trade_outcome.process_tx(
                        smt, trader_auditor_queue, suppress_trader_queue, **kwargs
                    )

                    # If we're dealing with a Liquidation fill vs.
                    # a cancel...
                    if isinstance(trade_outcome, LiquidationFill):
                        # Compute liquidated Strategy's realized pnl
                        liquidated_realized_pnl = (
                            trade_outcome.amount
                            * compute_avg_pnl(
                                liquidated_position_leaf, bankruptcy_price
                            )
                        )

                        # Update the collateral's intermediate value
                        collateral += liquidated_realized_pnl

                        # Update liquidated Position's balance by the
                        # liquidated amount
                        liquidated_position_leaf.balance -= trade_outcome.amount

                # Loop through each ADL outcome and process them individually
                for adl_outcome in liquidated_position.adl_outcomes:
                    # Construct Strategy identifier for the ADL'd
                    # Strategy
                    adl_strategy_identifier = StrategyIdentifier(
                        adl_outcome.trader_address,
                        f"{StrategyIdentifier.generate_strategy_id_hash(adl_outcome.strategy_id).hex()}",
                    )

                    # Derive the encoded Strategy key and H256 repr
                    adl_strategy_key = adl_strategy_identifier.encoded_key
                    adl_strategy_key_h256 = bytes_array_to_h256(adl_strategy_key)

                    # Get the ADL'd Strategy leaf
                    adl_strategy_leaf: Strategy = smt.get(adl_strategy_key_h256)

                    # Place the Strategy leaf update message on the queue to
                    # consider sending to the Trader
                    place_message_in_queue(
                        trader_auditor_queue,
                        suppress_trader_queue,
                        ItemMessage(
                            adl_strategy_identifier.topic_string,
                            WebsocketEventType.UPDATE,
                            {
                                "t": adl_strategy_identifier.topic_string,
                                "c": adl_strategy_leaf,
                            },
                            adl_outcome,
                        ),
                    )

                    # Construct Position identifier for the ADL'd
                    # Position
                    adl_position_identifier = PositionIdentifier(
                        symbol,
                        adl_outcome.trader_address,
                        f"{StrategyIdentifier.generate_strategy_id_hash(adl_outcome.strategy_id).hex()}",
                    )

                    # Derive the encoded Position key and H256 repr
                    adl_position_key = adl_position_identifier.encoded_key
                    adl_position_key_h256 = bytes_array_to_h256(adl_position_key)

                    # Get ADL'd Position leaf
                    adl_position_leaf: Position = smt.get(adl_position_key_h256)

                    # Compute ADL amount
                    adl_amount = min(
                        liquidated_position_leaf.balance, adl_position_leaf.balance
                    )

                    # Compute ADL'd realized PNL
                    adl_realized_pnl = adl_amount * compute_avg_pnl(
                        adl_position_leaf, bankruptcy_price
                    )

                    # Adjust ADL'd Strategy's free collateral
                    adl_strategy_leaf.free_collateral[
                        kwargs["usdc_address"]
                    ] = round_to_unit(
                        adl_strategy_leaf.free_collateral[kwargs["usdc_address"]]
                        + adl_realized_pnl
                    )
                    # Adjust ADL'd Strategy's free collateral in the SMT
                    if adl_strategy_leaf.free_collateral[
                        kwargs["usdc_address"]
                    ] == Decimal("0"):
                        adl_strategy_leaf.free_collateral = {}

                    # Store ADL'd Strategy in the SMT
                    smt.update(adl_strategy_key_h256, adl_strategy_leaf)

                    # Adjust and store ADL'd Position in the SMT
                    if adl_amount == adl_position_leaf.balance:
                        smt.update(adl_position_key_h256, Empty())
                    else:
                        adl_position_leaf.balance -= adl_amount
                        smt.update(adl_position_key_h256, adl_position_leaf)

                    # Place the Position leaf update message on the queue to
                    # consider sending to the Trader
                    place_message_in_queue(
                        trader_auditor_queue,
                        suppress_trader_queue,
                        ItemMessage(
                            adl_position_identifier.topic_string,
                            WebsocketEventType.UPDATE,
                            {
                                "t": adl_position_identifier.topic_string,
                                "c": adl_position_leaf,
                            },
                            adl_outcome,
                        ),
                    )

                    # Compute liquidated Strategy's realized pnl
                    liquidated_realized_pnl = adl_amount * compute_avg_pnl(
                        liquidated_position_leaf, bankruptcy_price
                    )

                    # Update the collateral's intermediate value
                    collateral += liquidated_realized_pnl

                    # Adjust liquidated Position's balance by the
                    # ADL'd amount
                    liquidated_position_leaf.balance -= adl_amount

                # Update liquidated Strategy's free collateral
                # with the realized PNL from liquidation fills and ADL's
                liquidated_strategy.free_collateral[
                    kwargs["usdc_address"]
                ] = round_to_unit(collateral)

                # Remove Position from the SMT
                smt.update(liquidated_position_key_h256, Empty())

                # Place the Position leaf update message on the
                # queue to consider sending to the Trader
                place_message_in_queue(
                    trader_auditor_queue,
                    suppress_trader_queue,
                    ItemMessage(
                        liquidated_position_identifier.topic_string,
                        WebsocketEventType.UPDATE,
                        {
                            "t": liquidated_position_identifier.topic_string,
                            "c": Empty(),
                        },
                        liquidated_position,
                    ),
                )

                # Construct OrganicInsuranceFund identifier
                organic_insurance_fund_identifier = OrganicInsuranceFundIdentifier()

                # Derive the OrganicInsuranceFund encoded key and H256
                # repr
                organic_insurance_fund_identifier_key = (
                    organic_insurance_fund_identifier.encoded_key
                )
                organic_insurance_fund_identifier_h256 = bytes_array_to_h256(
                    organic_insurance_fund_identifier_key
                )
                organic_insurance_fund_leaf: OrganicInsuranceFund = smt.get(
                    organic_insurance_fund_identifier_h256
                )

                # Overwrite the insurance fund with the new insurance fund
                # capitalization.
                organic_insurance_fund_leaf.capitalization[
                    kwargs["usdc_address"]
                ] = liquidated_position.new_insurance_fund_cap

                smt.update(
                    organic_insurance_fund_identifier_h256, organic_insurance_fund_leaf
                )

                # Place the OrganicInsuranceFund leaf update message on the queue to
                # consider sending to the Trader
                place_message_in_queue(
                    trader_auditor_queue,
                    suppress_trader_queue,
                    ItemMessage(
                        organic_insurance_fund_identifier.topic_string,
                        WebsocketEventType.UPDATE,
                        {
                            "t": organic_insurance_fund_identifier.topic_string,
                            "c": organic_insurance_fund_leaf,
                        },
                        liquidated_position,
                    ),
                )

                # Delete symbol/Position from dict
                del position_leaves_by_symbol[symbol]

            # Clear out the liquidated Strategy's free collateral and
            # store in the SMT
            liquidated_strategy.free_collateral = {}
            smt.update(liquidated_strategy_key_h256, liquidated_strategy)

    def repr_json(self):
        return {
            "eventType": EventType.LIQUIDATION,
            "requestIndex": self.request_index,
            "priceCheckpoints": self.price_checkpoints,
            "liquidation_entries": self.liquidation_entries,
        }

    def __repr__(self):
        return f"Liquidation (event)"
