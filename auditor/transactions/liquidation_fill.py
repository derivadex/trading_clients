"""
LiquidationFill module
"""
import asyncio
from ddx_python.decimal import Decimal
from typing import Dict

from ddx_client.auditor.smt.smt import SparseMerkleTree
from ddx_client.auditor.state.empty import Empty
from ddx_client.auditor.state.identifiers.book_order_identifier import (
    BookOrderIdentifier,
)
from ddx_client.auditor.state.identifiers.strategy_identifier import (
    StrategyIdentifier,
)
from ddx_client.auditor.state.item_message import ItemMessage
from ddx_client.auditor.transactions.event_message import EventMessage
from ddx_client.auditor.transactions.event_types import EventType
from ddx_client.auditor.transactions.fill import Fill
from ddx_client.auditor.transactions.identifiers.fill_identifier import (
    FillIdentifier,
)
from ddx_client.auditor.transactions.outcome import Outcome
from ddx_client.auditor.utils import bytes_array_to_h256
from ddx_client.client.asyncio_utils import place_message_in_queue
from ddx_client.client.websocket_message import WebsocketEventType


class LiquidationFill(Fill):
    """
    Defines a LiquidationFill
    """

    def __init__(
        self,
        reason: str,
        symbol: str,
        index_price_hash: str,
        maker_order_hash: str,
        maker_order_remaining_amount: Decimal,
        amount: Decimal,
        price: Decimal,
        taker_side: str,
        maker_outcome: Outcome,
        time_value: int,
        request_index: int,
    ):
        super().__init__(
            reason,
            symbol,
            maker_order_hash,
            maker_order_remaining_amount,
            amount,
            price,
            taker_side,
            maker_outcome,
            request_index,
        )
        self.index_price_hash = index_price_hash
        self.time_value = time_value

    @classmethod
    def decode_event_trigger_into_cls(cls, event_trigger: Dict):
        """
        Decode a event trigger (dict) into a LiquidationFill
        instance.

        Parameters
        ----------
        event_trigger : Dict
            Event trigger being processed
        """

        return cls(
            event_trigger["reason"],
            event_trigger["symbol"],
            event_trigger["indexPriceHash"],
            event_trigger["makerOrderHash"],
            Decimal(event_trigger["makerOrderRemainingAmount"]),
            Decimal(event_trigger["amount"]),
            Decimal(event_trigger["price"]),
            event_trigger["takerSide"],
            Outcome(
                event_trigger["makerOutcome"]["traderAddress"],
                event_trigger["makerOutcome"]["strategyId"],
                Decimal(event_trigger["makerOutcome"]["fee"]),
                event_trigger["makerOutcome"]["ddxFeeElection"],
            ),
            event_trigger["time_value"],
            event_trigger["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a LiquidationFill transaction. These are Fill
        transactions that have risen from either a Liquidation.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to Post transactions
        """

        maker_trade_identifier = FillIdentifier.decode_tx_into_cls(self)

        # Place the TradeFill event message (maker topic) on the queue to
        # consider sending to the Trader
        place_message_in_queue(
            trader_auditor_queue,
            suppress_trader_queue,
            EventMessage(
                maker_trade_identifier.topic_string,
                WebsocketEventType.UPDATE,
                {"t": maker_trade_identifier.topic_string, "c": self},
            ),
        )

        # Construct a BookOrderIdentifier and corresponding
        # encoded key for the maker order aspect of this
        # LiquidationFill. The maker order is taking on the
        # liquidated position.
        maker_book_order_identifier = BookOrderIdentifier(
            self.symbol,
            self.maker_order_hash,
            self.maker_outcome.trader_address,
            f"{StrategyIdentifier.generate_strategy_id_hash(self.maker_outcome.strategy_id).hex()}",
        )

        # Derive the encoded BookOrders key and H256 repr
        maker_book_order_key = maker_book_order_identifier.encoded_key
        maker_book_order_key_h256 = bytes_array_to_h256(maker_book_order_key)

        # Apply the LiquidationFill transaction to the local order book abstraction
        kwargs["order_books"][self.symbol].apply_fill_to_book(
            maker_book_order_key_h256, self
        )

        maker_book_order_leaf = smt.get(maker_book_order_key_h256)
        maker_book_order_time_value = maker_book_order_leaf.time_value

        if self.maker_order_remaining_amount > 0:
            # If there is still a remaining amount, we modify the
            # existing leaf with the updated amount
            maker_book_order_leaf.amount = self.maker_order_remaining_amount

            # Update the SMT with the H256 repr of the key and the
            # BookOrder leaf
            smt.update(maker_book_order_key_h256, maker_book_order_leaf)

            # Place the BookOrder leaf update message on the queue to
            # consider sending to the Trader
            place_message_in_queue(
                trader_auditor_queue,
                suppress_trader_queue,
                ItemMessage(
                    BookOrderIdentifier.decode_key_into_cls(
                        maker_book_order_key, maker_book_order_leaf
                    ).topic_string,
                    WebsocketEventType.UPDATE,
                    {
                        "t": BookOrderIdentifier.decode_key_into_cls(
                            maker_book_order_key, maker_book_order_leaf
                        ).topic_string,
                        "c": maker_book_order_leaf,
                    },
                    self,
                ),
            )
        else:
            # No remaining amount left for the maker order

            # If there is no remaining amount, we update the SMT with
            # the H256 repr of the key and the Empty leaf, effectively
            # removing it from the SMT
            smt.update(maker_book_order_key_h256, Empty())

            # Place the BookOrder leaf update message on the queue to
            # consider sending to the Trader
            place_message_in_queue(
                trader_auditor_queue,
                suppress_trader_queue,
                ItemMessage(
                    BookOrderIdentifier.decode_key_into_cls(
                        maker_book_order_key, maker_book_order_leaf
                    ).topic_string,
                    WebsocketEventType.UPDATE,
                    {
                        "t": BookOrderIdentifier.decode_key_into_cls(
                            maker_book_order_key, maker_book_order_leaf
                        ).topic_string,
                        "c": Empty(),
                    },
                    self,
                ),
            )

        # Adjust the maker-related position. Take note that
        # in a liquidation, there is no taker component, so
        # unlike its counterpart (TradeFill), a
        # LiquidationFill only considers the maker
        self.adjust_for_maker_taker(
            smt,
            trader_auditor_queue,
            suppress_trader_queue,
            maker_book_order_time_value,
            True,
            kwargs["trade_mining_active"],
            kwargs["usdc_address"],
            kwargs["ddx_address"],
        )

    def repr_json(self):
        return {
            "eventType": EventType.FILL,
            "requestIndex": self.request_index,
            "time_value": self.time_value,
            "reason": self.reason,
            "symbol": self.symbol,
            "indexPriceHash": self.index_price_hash,
            "makerOrderHash": self.maker_order_hash,
            "makerOrderRemainingAmount": str(self.maker_order_remaining_amount),
            "amount": str(self.amount),
            "price": str(self.price),
            "takerSide": self.taker_side,
            "makerOutcome": self.maker_outcome,
        }

    def __repr__(self):
        return f"LiquidationFill (event): request_index = {self.request_index}; time_value = {self.time_value}; index_price_hash = {self.index_price_hash}; maker_order_hash = {self.maker_order_hash}; maker_order_remaining_amount: {self.maker_order_remaining_amount}; amount: {self.amount}; price: {self.price}; maker_outcome: {self.maker_outcome}"

    @property
    def metrics_repr(self):
        return {
            "eventType": EventType.FILL,
            "requestIndex": self.request_index,
            "time_value": self.time_value,
            "reason": self.reason,
            "symbol": self.symbol,
            "indexPriceHash": self.index_price_hash,
            "makerOrderHash": self.maker_order_hash,
            "makerOrderRemainingAmount": self.maker_order_remaining_amount,
            "amount": self.amount,
            "price": self.price,
            "takerSide": self.taker_side,
            "makerOutcome": self.maker_outcome,
        }
