"""
EpochMarker module
"""

from typing import Dict

from ddx_client.auditor.transactions.event import Event


class EpochMarker(Event):
    """
    Defines an EpochMarker
    """

    def __init__(
        self,
        kind: str,
        request_index: int,
    ):
        """
        Initialize an EpochMarker non-transitioning transaction.

        Parameters
        ----------
        kind : str
           Type of EpochMarker (Genesis | AdvanceEpoch)
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.kind = kind

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        raise NotImplementedError
