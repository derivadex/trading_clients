"""
Strategy Update module
"""
import asyncio
from ddx_python.decimal import Decimal
from typing import Dict

from ddx_client.auditor.smt.h256 import H256
from ddx_client.auditor.smt.smt import SparseMerkleTree
from ddx_client.auditor.state.empty import Empty
from ddx_client.auditor.state.identifiers.strategy_identifier import (
    StrategyIdentifier,
)
from ddx_client.auditor.state.identifiers.trader_identifier import TraderIdentifier
from ddx_client.auditor.state.item_message import ItemMessage
from ddx_client.auditor.state.strategy import Strategy
from ddx_client.auditor.state.trader import Trader
from ddx_client.auditor.transactions.event import Event
from ddx_client.auditor.transactions.event_message import EventMessage
from ddx_client.auditor.transactions.event_types import EventType
from ddx_client.auditor.transactions.identifiers.strategy_update_identifier import (
    StrategyUpdateIdentifier,
)
from ddx_client.auditor.utils import (
    bytes_array_to_h256,
    strategy_update_kind_to_int,
    strategy_update_int_to_kind,
    calculate_max_collateral,
)
from ddx_client.client.asyncio_utils import place_message_in_queue
from ddx_client.client.websocket_message import WebsocketEventType


class StrategyUpdate(Event):
    """
    Defines a Strategy Update
    """

    def __init__(
        self,
        trader_address: str,
        collateral_address: str,
        strategy_id: str,
        amount: Decimal,
        update_kind: int,
        tx_hash: str,
        request_index: int,
    ):
        """
        Initialize a StrategyUpdate transaction. A StrategyUpdate is an
        update to a trader's strategy (such as depositing or withdrawing
        collateral).

        Parameters
        ----------
        trader_address : str
           Trader's Ethereum address this strategy belongs to
        collateral_address: str
           Collateral's Ethereum address a deposit/withdrawal has been
           made with
        strategy_id: str
           Strategy ID for the given trader this event belongs to
        amount: Decimal
           The amount of collateral deposited or withdrawn
        update_kind: int
           Update kind (Deposit=0, Withdraw=1)
        tx_hash: str
           The Ethereum transaction's hash
        request_index : int
            Sequenced request index of transaction
        """

        self.request_index = request_index
        self.trader_address = trader_address
        self.collateral_address = collateral_address
        self.strategy_id = strategy_id
        self.amount = amount
        self.update_kind = update_kind
        self.tx_hash = tx_hash

    @classmethod
    def decode_value_into_cls(cls, raw_tx_log_event: Dict):
        """
        Decode a raw transaction log event (dict) into a StrategyUpdate
        instance.

        Parameters
        ----------
        raw_tx_log_event : Dict
            Raw transaction log event being processed
        """

        strategy_update_tx_event = raw_tx_log_event["event"]["c"]

        return cls(
            strategy_update_tx_event["trader"],
            strategy_update_tx_event["collateralAddress"],
            strategy_update_tx_event["strategyId"],
            Decimal(strategy_update_tx_event["amount"]),
            strategy_update_kind_to_int(strategy_update_tx_event["updateKind"]),
            strategy_update_tx_event["txHash"],
            raw_tx_log_event["requestIndex"],
        )

    def process_tx(
        self,
        smt: SparseMerkleTree,
        trader_auditor_queue: asyncio.Queue,
        suppress_trader_queue: bool,
        **kwargs,
    ):
        """
        Process a StrategyUpdate transaction. A StrategyUpdate consists
        of information relating to updates for a trader's strategy, such
        as when their free or frozen collateral has changed due to a
        deposit or withdrawal. This will update the Strategy leaf in the
        SMT.

        Parameters
        ----------
        smt: SparseMerkleTree
            DerivaDEX Sparse Merkle Tree
        trader_auditor_queue : asyncio.Queue
            Queue for sending events from the Auditor to the Trader
        suppress_trader_queue: bool
            Suppress trader queue messages
        **kwargs
            Additional args specific to Post transactions
        """

        # Place the StrategyUpdate event message on the queue to
        # consider sending to the Trader
        strategy_update_identifier = StrategyUpdateIdentifier.decode_tx_into_cls(self)
        place_message_in_queue(
            trader_auditor_queue,
            suppress_trader_queue,
            EventMessage(
                strategy_update_identifier.topic_string,
                WebsocketEventType.UPDATE,
                {
                    "t": strategy_update_identifier.topic_string,
                    "c": self,
                },
            ),
        )

        # Construct a StrategyIdentifier and corresponding encoded
        # key
        strategy_identifier = StrategyIdentifier(
            self.trader_address,
            f"{StrategyIdentifier.generate_strategy_id_hash(self.strategy_id).hex()}",
        )
        strategy_key = strategy_identifier.encoded_key
        strategy_key_h256 = bytes_array_to_h256(strategy_key)

        # Get the Strategy leaf given the key above
        strategy_leaf = smt.get(strategy_key_h256)
        if self.update_kind == 0:
            # If StrategyUpdate is of deposit type

            # Construct a TraderIdentifier and corresponding encoded
            # key
            trader_identifier = TraderIdentifier(self.trader_address)
            trader_key = trader_identifier.encoded_key
            trader_key_h256 = bytes_array_to_h256(trader_key)

            # Get the Trader leaf given the key above
            trader_leaf = smt.get(trader_key_h256)

            if strategy_leaf == H256.zero():
                # If we haven't yet seen the Strategy leaf, create a new
                # one
                strategy_leaf = Strategy(
                    self.strategy_id,
                    {},
                    {},
                    3,
                    False,
                )

                if trader_leaf == H256.zero():
                    # Initialize a new Trader Leaf
                    trader_leaf = Trader(
                        Decimal("0"),
                        Decimal("0"),
                        "0x0000000000000000000000000000000000000000",
                        False,
                    )

                    # Update the SMT with the H256 repr of the key and the
                    # Trader leaf
                    smt.update(trader_key_h256, trader_leaf)

                    # Place the Trader leaf update message on the queue to
                    # consider sending to the Trader
                    place_message_in_queue(
                        trader_auditor_queue,
                        suppress_trader_queue,
                        ItemMessage(
                            trader_identifier.topic_string,
                            WebsocketEventType.UPDATE,
                            {"t": trader_identifier.topic_string, "c": trader_leaf},
                            self,
                        ),
                    )

            # Adjust the existing strategy leaf by incrementing the
            # free collateral by the amount in the deposit event
            if self.collateral_address not in strategy_leaf.free_collateral:
                strategy_leaf.free_collateral[self.collateral_address] = Decimal("0")

            # Compute max allowable deposit given the collateral guard
            # for the trader's DDX balance
            max_allowable_deposit = max(
                (
                    calculate_max_collateral(
                        kwargs["collateral_tranches"], trader_leaf.free_ddx_balance
                    )
                    - strategy_leaf.free_collateral[self.collateral_address]
                ),
                Decimal("0"),
            )

            # Compute how much of the attempted deposit will be added
            # to the Strategy's free collateral
            net = min(max_allowable_deposit, self.amount)

            # Increment the Strategy's free collateral
            strategy_leaf.free_collateral[self.collateral_address] += net

            # Compute how much of the attempted deposit will be added
            # to the Strategy's frozen collateral
            kickback = self.amount - net
            if kickback != Decimal("0"):
                if self.collateral_address not in strategy_leaf.frozen_collateral:
                    strategy_leaf.frozen_collateral[self.collateral_address] = Decimal(
                        "0"
                    )

                # Increment the Strategy's frozen collateral
                strategy_leaf.frozen_collateral[self.collateral_address] += kickback

            # Update the SMT with the H256 repr of the key and the
            # Strategy leaf
            smt.update(strategy_key_h256, strategy_leaf)

            # Place the Strategy leaf update message on the queue to
            # consider sending to the Trader
            place_message_in_queue(
                trader_auditor_queue,
                suppress_trader_queue,
                ItemMessage(
                    strategy_identifier.topic_string,
                    WebsocketEventType.UPDATE,
                    {
                        "t": strategy_identifier.topic_string,
                        "c": strategy_leaf,
                    },
                    self,
                ),
            )
        else:
            # If StrategyUpdate is of withdrawal type
            if (
                strategy_leaf == H256.zero()
                or isinstance(strategy_leaf, Empty)
                or strategy_leaf.frozen_collateral[self.collateral_address]
                < self.amount
            ):
                raise Exception(
                    "Strategy leaf either non-existent or insufficiently capitalized to facilitate withdrawal"
                )

            # Adjust the existing strategy leaf by decrementing the
            # free collateral by the amount in the withdrawal event
            strategy_leaf.frozen_collateral[self.collateral_address] -= self.amount
            if strategy_leaf.frozen_collateral[self.collateral_address] == Decimal("0"):
                del strategy_leaf.frozen_collateral[self.collateral_address]

            # Update the SMT with the H256 repr of the key and the
            # Strategy leaf
            smt.update(strategy_key_h256, strategy_leaf)

            # Place the Strategy leaf update message on the queue to
            # consider sending to the Trader
            place_message_in_queue(
                trader_auditor_queue,
                suppress_trader_queue,
                ItemMessage(
                    strategy_identifier.topic_string,
                    WebsocketEventType.UPDATE,
                    {"t": strategy_identifier.topic_string, "c": strategy_leaf},
                    self,
                ),
            )

    def repr_json(self):
        return {
            "eventType": EventType.STRATEGY_UPDATE,
            "requestIndex": self.request_index,
            "traderAddress": self.trader_address,
            "collateralAddress": self.collateral_address,
            "strategyId": self.strategy_id,
            "amount": str(self.amount),
            "updateKind": self.update_kind,
            "txHash": self.tx_hash,
        }

    def __repr__(self):
        return (
            f"Strategy Update (event): "
            f"request_index = {self.request_index}; "
            f"trader_address = {self.trader_address}; "
            f"collateral_address = {self.collateral_address}; "
            f"strategy_id: {self.strategy_id}; "
            f"amount: {self.amount}; "
            f"update_kind: {self.update_kind}; "
            f"tx_hash: {self.tx_hash}"
        )

    @property
    def metrics_repr(self):
        return {
            "eventType": EventType.STRATEGY_UPDATE,
            "requestIndex": self.request_index,
            "traderAddress": self.trader_address,
            "collateralAddress": self.collateral_address,
            "strategyId": self.strategy_id,
            "amount": self.amount,
            "updateKind": strategy_update_int_to_kind(self.update_kind),
            "txHash": self.tx_hash,
        }
